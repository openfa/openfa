{ pkgs ? import <nixpkgs>
  {
    overlays = [];
  }
}:
let
in
  pkgs.mkShell {
    nativeBuildInputs = [
      pkgs.assimp
      pkgs.atk
      pkgs.awscli2
      pkgs.fontconfig
      pkgs.gdk-pixbuf
      pkgs.glxinfo
      pkgs.gnumake
      pkgs.gtest
      pkgs.gtk3
      pkgs.libxkbcommon
      pkgs.mold-wrapped
      pkgs.ninja
      pkgs.openssl
      pkgs.pango
      pkgs.pkg-config
      pkgs.sccache
      pkgs.simple-http-server
      pkgs.udev
      pkgs.vulkan-tools
      pkgs.wasm-pack
      pkgs.wayland
      pkgs.xorg.libX11
      pkgs.xorg.libXrandr
      pkgs.xz
      pkgs.zola
    ];
    LD_LIBRARY_PATH = with pkgs.xorg; "${pkgs.vulkan-loader}/lib:${pkgs.mesa}/lib:${libX11}/lib:${libXcursor}/lib:${libXxf86vm}/lib:${libXi}/lib:${libXrandr}/lib:${pkgs.libxkbcommon}/lib:${pkgs.wayland}/lib";
    LIBCLANG_PATH = pkgs.lib.makeLibraryPath [ pkgs.llvmPackages_latest.libclang.lib ];
    DISPLAY = ":0";
    RUSTC_WRAPPER = "${pkgs.sccache}/bin/sccache";
    SCCACHE_CACHE_SIZE = "120G";
  }
