New in Version 0.2.15
---

* Found something that may be the correct scaling factor in Sh.
* Find and link all items associated with an airport
* Flatten terrain in the area of airports
* Fix scroll wheel support in Wayland
* Make stars respect the hour angle again

### TODO

* Smoothly animate the camera between scenes
* Fix boresight function using new ray-collider intersection tests

New in Version 0.2.14
---

* Import and export T2 to and from yaml and png planes with ofa-tools
* Prevent filenames in the editor from going over LIB's 13 char limit
* Enable rename of GAS, ECM, and SEE files

New in Version 0.2.13
---

* Fix a bug making selection fiddly and unpredictable
* Fix a bug preventing locals from showing up in the console

New in Version 0.2.12
---

* Fix a bug that was preventing opening a LIB to edit from the LIBs list

New in Version 0.2.11
---

* Remove legacy SH decoder and legacy CSV and text outputs that used it
* Add SEE, ECM, and GAS to ofa-tools and the openfa editor
* Show a messagebox with the error when ofa-tools fails to process
* Support yaml export and import for all brents relocatable format files
    * Drag-n-drop OT, NT, JT, PT, SEE, ECM, and GAS files onto ofa-tools to get .OT.yaml, etc and vice-versa.
    * Yaml files have the name of the field inline and include units (where known) so are much easier to read and edit.

New in Version 0.2.10
---

* Bug: fix a crash in the editor when selecting a game to load assets from
* Add a tool to rescale a shape wholesale
* Re-add CoVertex position editing
* When a single CoVert is selected, allow editing individual vertex positions in the CoVert
* Show the controlling Xform on xformed faces

New in Version 0.2.9
---

* Show a dialog with the error if we fail for any reason
* Fix undo/redo fast-forward so it works in some more cases
* Don't fail to start if we find broken libs
* Rewrite the shape editor around flexible selections
* Draw selections in screen space so that shape editing works at all scales
* Push F1/? to toggle help screen in shape editor

New in Version 0.2.8
---

* Initial texture painting proof-of-concept
* [Issue 8] don't interact with the world when clicking on the panel
* Fix texcoord editing in the face view
* [Issue 10] handle FCF Unk5 correctly

New in Version 0.2.7
---

* Fix mouse button ordering on Windows
* Fix centroid calculation
* Use the correct axis color for z in the vertex mover

New in Version 0.2.6
---

* Update winit, wgpu, and egui. This should solve keyboard problems on linux
* Implement undo/redo support in the editor
* Implement Movement and Controls pages of the PT editor
* Add backend for serializing MM files
* Replace hacky bulk vertex move text entry with a draggable screen element
* [Issue 1] Add a draggable value entry to the palette for manual entry
* [Issue 1] Open palette in a separate window so it doesn't destroy the layout
* [Issue 1] Highlight the currently selected palette value
* [Issue 1] Make the palette bigger
* [Issue 1] Show hovered and current colors as hex
* [Issue 3] Add checkboxes to vertex edit to recompute face center and normal on move

New in Version 0.2.5
---

* Remove show_sh code and references
* Point drag-n-drop for SH to the editor
* Make escape work more correctly in Lib editor
* Add informational code and file offsets to SH yaml to make cross-referencing easier
* Show code offset and vxbuf offset on co-vertices in the editor
* Enable editing the SH name

New in Version 0.2.4
---

* Previously OpenFA was overly aggressive about parallelizing certain drawing operations leading to inconsistent
  behavior.
* Discreet world position and time controls in the View-World subcommand (moved from show-shape command).
* Improved new text output of SH instructions; should be superior to 0.1 versions of ofa-tools now
* Reworked libs around a shared, layered interface. This should give more consistent loading behavior in the editor.
* Switch to indexed drawing for shape rendering
* Move ShowSh functionality into EditLib
* Enable shape editing for parts we know the function of and can edit safely, given the limits of our knowledge

New in Version 0.2.3
---

* Rewrite PIC handling from the ground up with a focus on quality rather than discovery.
* Fix support for specifying extra lib directories in ofa-tools
* Update to latest version of wgpu, bevy, and egui.

New in Version 0.2.2
---

* Unified all of the dump- and pack- tools into a single binary: ofa-tools.
* Support for packing pics. Drag a png onto ofa-tools.exe in order to get a PIC suitable for use as a texture.
  Run `ofa-tools.exe --help` to see additional features.
* Drag and drop pack and unpack of LIB and SH files is still supported by dragging them onto ofa-tools.exe.

New in Version 0.1.4
---

* Add a processed column to CSV import / export to make shape editing easier

New in Version 0.1.3
---

* CSV import and export for shapes

New in Version 0.1.2
---

* Rewrite all GUI elements on top of egui and ripped out my homegrown solution
* Experimental SH <-> CSV and glTF code
