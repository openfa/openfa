+++
title = "OpenFA Alpha Release 0.2.11"
description = "Add support for SEE, ECM, and GAS and yaml/export import for BRF"
date = 2024-06-24
draft = false
slug = "openfa-release-2-11"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download from the [Releases Page](https://gitlab.com/openfa/openfa/-/releases),
or via the [OpenFA website](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.11-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.11-win-x86_64.zip)

New in Version 0.2.11
---

* Add SEE, ECM, and GAS to ofa-tools and the openfa editor
* Support yaml export and import for all brents relocatable format files
    * Drag-n-drop OT, NT, JT, PT, SEE, ECM, and GAS files onto ofa-tools to get .OT.yaml, etc and vice-versa.
    * Yaml files have the name of the field inline and include units (where known) so are much easier to read and edit.
* Show a messagebox with the error when ofa-tools fails to process
* Remove legacy SH decoder and legacy CSV and text outputs that used it
