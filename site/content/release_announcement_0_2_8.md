+++
title = "OpenFA Alpha Release 0.2.8"
description = "Demo for texture paint capabilities"
date = 2024-05-05
draft = false
slug = "openfa-release-2-8"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download at gitlab.com/openfa/openfa, or via
the OpenFA website: [https://openfa.org](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.8-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.8-win-x86_64.zip)

New in Version 0.2.8
---

* Initial texture painting proof-of-concept
* [Issue 8] don't interact with the world when clicking on the panel
* Fix texcoord editing in the face view
* [Issue 10] handle FCF Unk5 correctly
