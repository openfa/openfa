+++
title = "OpenFA Alpha Release 0.2.13"
description = "Chemspill Release"
date = 2024-06-30
draft = false
slug = "openfa-release-0-2-13"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download from the [Releases Page](https://gitlab.com/openfa/openfa/-/releases),
or via the [OpenFA website](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.13-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.13-win-x86_64.zip)

New in Version 0.2.13
---

* Fix a bug making selection fiddly and unpredictable
* Fix a bug preventing locals from showing up in the console
