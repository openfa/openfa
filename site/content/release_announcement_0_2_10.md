+++
title = "OpenFA Alpha Release 0.2.10"
description = "Add a scale tool and vertex move tools"
date = 2024-06-08
draft = false
slug = "openfa-release-2-10"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download from the [Releases Page](https://gitlab.com/openfa/openfa/-/releases),
or via the [OpenFA website](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.10-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.10-win-x86_64.zip)

New in Version 0.2.10
---

* Bug: fix a crash in the editor when selecting a game to load assets from
* Add a tool to rescale a shape wholesale
* Re-add CoVertex position editing
* When a single CoVert is selected, allow editing individual vertex positions in the CoVert
* Show the controlling Xform on xformed faces
