+++
title = "OpenFA Alpha Release 0.2.7"
description = "Fast follow to fix mouse issues from the winit upgrade"
date = 2024-04-28
draft = false
slug = "openfa-release-2-7"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing", "tutorial"]

[extra]
comments = false
+++

The latest release is now available to download at gitlab.com/openfa/openfa, or via
the OpenFA website: [https://openfa.org](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.7-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.7-win-x86_64.zip)

New in Version 0.2.7
---

* Fix mouse button ordering on Windows
* Fix centroid calculation
* Use the correct axis color for z in the vertex mover
