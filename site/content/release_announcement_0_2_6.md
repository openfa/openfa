+++
title = "OpenFA Alpha Release 0.2.6"
description = "Enhancements to the second alpha release of OpenFA"
date = 2024-04-27
draft = false
slug = "openfa-release-2-6"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing", "tutorial"]

[extra]
comments = false
+++

The latest release is now available to download at gitlab.com/openfa/openfa, or via
the OpenFA website: [https://openfa.org](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.6-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.6-win-x86_64.zip)

New in Version 0.2.6
---

* Update winit, wgpu, and egui. This should solve keyboard problems on linux
* Implement undo/redo support in the editor
* Implement Movement and Controls pages of the PT editor
* Add backend for serializing MM files
* Replace hacky bulk vertex move text entry with a draggable screen element
* [Issue 1] Add a draggable value entry to the palette for manual entry
* [Issue 1] Open palette in a separate window so it doesn't destroy the layout
* [Issue 1] Highlight the currently selected palette value
* [Issue 1] Make the palette bigger
* [Issue 1] Show hovered and current colors as hex
* [Issue 3] Add checkboxes to vertex edit to recompute face center and normal on move

And since there was no blog announcement of the other releases after 0.2, here is what else has changed since then.

New in Version 0.2.5
---

* Remove show_sh code and references
* Point drag-n-drop for SH to the editor
* Make escape work more correctly in Lib editor
* Add informational code and file offsets to SH yaml to make cross-referencing easier
* Show code offset and vxbuf offset on co-vertices in the editor
* Enable editing the SH name

New in Version 0.2.4
---

* Previously OpenFA was overly aggressive about parallelizing certain drawing operations leading to inconsistent
  behavior.
* Discreet world position and time controls in the View-World subcommand (moved from show-shape command).
* Improved new text output of SH instructions; should be superior to 0.1 versions of ofa-tools now
* Reworked libs around a shared, layered interface. This should give more consistent loading behavior in the editor.
* Switch to indexed drawing for shape rendering
* Move ShowSh functionality into EditLib
* Enable shape editing for parts we know the function of and can edit safely, given the limits of our knowledge

New in Version 0.2.3
---

* Rewrite PIC handling from the ground up with a focus on quality rather than discovery.
* Fix support for specifying extra lib directories in ofa-tools
* Update to latest version of wgpu, bevy, and egui.

New in Version 0.2.2
---

* Unified all of the dump- and pack- tools into a single binary: ofa-tools.
* Support for packing pics. Drag a png onto ofa-tools.exe in order to get a PIC suitable for use as a texture.
  Run `ofa-tools.exe --help` to see additional features.
* Drag and drop pack and unpack of LIB and SH files is still supported by dragging them onto ofa-tools.exe.
