+++
title = "Features"
description = ""

date = 2024-04-22
draft = false
+++

# Features

## ofa-tools.exe

A drag-n-drop swiss army knife for converting into and out of FA file formats.
Also works as a CLI for deeper inspection and advanced scenarios.

### Drag-n-Drop Conversions

* .LIB -> directory
* directory -> directory.LIB
* .PIC -> .png
* .png .jpg .bmp .gif .ico .tiff -> .PIC
* .SH -> .SH.yaml
* .SH.yaml -> .SH
* .PAL -> .PAL.png

Run `ofa-tools.exe --help` to see current options.

## OpenFA

OpenFA contains an FA LIB editor and an extremely rudimentary mission player.

### LIB Editor

* Create new LIBs from scratch.
* Import and edit PT, HUD, and SH files.

### Mission Player

* Earth: flyable from ground level to space over every square inch, without limitation
* Physically accurate atmospheric scattering for realtime sunrise and sunset
* FA maps overlaid over real terrain in approximately the right positions, but keeping the
  high-detail normal maps from the underlying SRTM data set
* All mission assets loaded and in approximately the right spots
* Basic flight model