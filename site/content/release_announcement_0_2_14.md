+++
title = "OpenFA Alpha Release 0.2.14"
description = "T2 Editing Support"
date = 2024-07-05
draft = false
slug = "openfa-release-0-2-14"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download from the [Releases Page](https://gitlab.com/openfa/openfa/-/releases),
or via the [OpenFA website](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.14-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.14-win-x86_64.zip)

New in Version 0.2.14
---

* Import and export T2 to and from yaml and png planes with ofa-tools
* Prevent filenames in the editor from going over LIB's 13 char limit
* Enable rename of GAS, ECM, and SEE files
