+++
title = "OpenFA Alpha Release 0.2.9"
description = "Rewrite SH editor with everything we've learned"
date = 2024-06-01
draft = false
slug = "openfa-release-2-9"

[taxonomies]
categories = ["releases"]
tags = ["release", "sh-editing"]

[extra]
comments = false
+++

The latest release is now available to download from the [Releases Page](https://gitlab.com/openfa/openfa/-/releases),
or via the [OpenFA website](https://openfa.org).

* [OFA Tools](https://openfa.s3.us-west-1.amazonaws.com/ofa-tools-alpha-0.2.9-win-x86_64.zip)
* [OpenFA](https://openfa.s3.us-west-1.amazonaws.com/openfa-alpha-0.2.9-win-x86_64.zip)

New in Version 0.2.9
---

* Show a dialog with the error if we fail for any reason
* Fix undo/redo fast-forward so it works in some more cases
* Don't fail to start if we find broken libs
* Rewrite the shape editor around flexible selections
* Draw selections in screen space so that shape editing works at all scales
* Push F1/? to toggle help screen in shape editor
