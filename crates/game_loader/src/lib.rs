// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod shape_loader;

use crate::shape_loader::{load_shape_for_mmm_object, load_shape_for_xt};
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use catalog::{AssetCatalog, FileSystem};
use fa_vehicle::Turbojet;
use flight_dynamics::ClassicFlightModel;
use geodb::{Flatten, Foundation, GeoDbStep};
use geodesy::{Bearing, GeodeBB, Geodetic, GeodeticBB};
use geometry::{Aabb3, Ray};
use installations::{from_dos_string, Installations};
use mantle::Gpu;
use mmm::{AirportSegment, Mission, MissionMap, ObjectInfo, ObjectProvider};
use nitrous::{
    inject_nitrous_component, inject_nitrous_resource, make_symbol, method, EntityName, HeapMut,
    HeapRef, NitrousComponent, NitrousResource, Value,
};
use orrery::Orrery;
use phase::{BodyMotion, CollisionBuilder, CollisionImpostor, Frame};
use runtime::{report_errors, Extension, PlayerMarker, Runtime};
use shape::{Shape, ShapeId, ShapeLoadPackage, ShapeStep};
use std::{collections::HashMap, time::Duration};
use t2_terrain::{T2TerrainBuffer, T2TileSet};
use terrain::Terrain;
use uuid::Uuid;
use vehicle::{
    AirbrakeControl, AirbrakeEffector, Airframe, BayControl, BayEffector, FlapsControl,
    FlapsEffector, FuelSystem, FuelTank, FuelTankKind, GearControl, GearEffector, HookControl,
    HookEffector, PitchInceptor, PowerSystem, RollInceptor, ThrottleInceptor,
    ThrustVectorPitchControl, ThrustVectorPitchEffector, ThrustVectorYawControl,
    ThrustVectorYawEffector, VtolAngleControl, YawInceptor,
};
use xt::TypeManager;

#[derive(NitrousComponent, Debug, Default)]
pub struct MissionMarker;

#[inject_nitrous_component]
impl MissionMarker {}

// Added to the flatten entity for the airport as a whole so that we can fix up
// the positions of everything attached to the primary after flattening changes
// the position.
#[derive(NitrousComponent, Clone, Debug)]
pub struct AirportEnvirons {
    previous_position: Pt3<Meters>,
    primary: Entity,
    secondary: Option<Entity>,
    displacement: V3<Length<Feet>>,
    environs: Vec<Entity>,
    // TODO: see if we need to update the position of all objects on the secondary
    //       to follow when we move the secondary, or if everything is good enough already.
}

#[inject_nitrous_component]
impl AirportEnvirons {
    fn new(id: Entity, segment: AirportSegment) -> Self {
        let mut obj = Self {
            previous_position: Pt3::zero(),
            primary: Entity::PLACEHOLDER,
            secondary: None,
            displacement: V3::zero(),
            environs: Vec::new(),
        };
        obj.insert(id, segment);
        obj
    }

    fn insert(&mut self, id: Entity, segment: AirportSegment) {
        match segment {
            AirportSegment::Primary => self.primary = id,
            AirportSegment::Secondary(_) => self.secondary = Some(id),
            AirportSegment::Object(_) => self.environs.push(id),
            AirportSegment::None => {}
        }
    }

    fn set_displacement(&mut self, displacement: V3<Length<Feet>>) {
        self.displacement = displacement;
    }
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum GameStep {
    LinkAirportHalves,
}

#[derive(Debug, Default, NitrousResource)]
#[resource(name = "game")]
pub struct GameLoader;

impl Extension for GameLoader {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        // Attach a foundation to the primary airport half and update the position
        // of the secondary after the primary follows the terrain.
        runtime.add_frame_system(
            Self::sys_link_airport_halves
                .pipe(report_errors)
                .in_set(GameStep::LinkAirportHalves)
                .after(GeoDbStep::CheckDownloads)
                .before(ShapeStep::ApplyTransforms)
                // AirportEnviron and Foundation are orthogonal to ScreenCamera
                .ambiguous_with(CameraStep::MoveCameraToFrame),
        );

        runtime.inject_resource(Self)?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl GameLoader {
    fn sys_link_airport_halves(
        mut all_airports: Query<&mut AirportEnvirons>,
        mut all_ot: Query<(&ShapeId, &mut Frame), With<Foundation>>,
    ) -> Result<()> {
        for mut airport_environ in all_airports.iter_mut() {
            // Note: copy to avoid borrow on all_ot so we can mutate secondary
            let (prime_pos, prime_facing) = {
                let (_, prime_frame) = all_ot.get(airport_environ.primary)?;
                (prime_frame.position(), prime_frame.facing())
            };

            // If we have not moved, skip the update. Note that we can use bitwise
            // comparison here because we want to flag any assignment to the frame.
            if airport_environ.previous_position == prime_pos {
                continue;
            }

            if let Some(secondary_id) = airport_environ.secondary {
                let (_, mut secondary_frame) = all_ot.get_mut(secondary_id)?;
                secondary_frame
                    .set_position(prime_pos + prime_facing * airport_environ.displacement);
            }

            airport_environ.previous_position = prime_pos;
        }
        Ok(())
    }

    #[method]
    fn boresight(&self, mut heap: HeapMut) {
        let position = heap.resource::<ScreenCamera>().position::<Meters>();
        let forward = *heap.resource::<ScreenCamera>().forward();
        let view_ray = Ray::new(position, forward);

        for (name, frame, collision) in heap
            .query::<(&EntityName, &Frame, &CollisionImpostor)>()
            .iter(heap.world())
        {
            let intersect = collision.intersect_ray(frame, &view_ray);
            if intersect.is_some() {
                println!("COLLIDE: {name:?}");
            }
        }
    }

    /// Despawn whatever camera is current and re-spawn as a generic arcball controlled
    /// camera, wherever the prior camera was located, if it existed.
    // #[method]
    // pub fn detach_camera(&self, mut heap: HeapMut) -> Result<()> {
    //     let mut loc = None;
    //     if let Some(id) = heap.maybe_entity("camera") {
    //         loc = heap.maybe_get_by_id::<Frame>(id).map(|v| v.to_owned());
    //         heap.despawn_by_id(id);
    //     }
    //     let frame = loc.unwrap_or_default();
    //
    //     heap.spawn("camera")?
    //         .inject(frame)?
    //         .inject(ArcBallController::default())?
    //         .inject(ScreenCameraController)?;
    //
    //     Ok(())
    // }

    /// Despawn whatever camera is current and re-spawn as a player camera.
    /// A PlayerMarker should be added to some entity before the next frame.
    // #[method]
    // pub fn attach_camera(&self, mut heap: HeapMut) -> Result<()> {
    //     let mut loc = None;
    //     if let Some(id) = heap.maybe_entity("camera") {
    //         loc = heap.maybe_get_by_id::<Frame>(id).map(|v| v.to_owned());
    //         heap.despawn_by_id(id);
    //     }
    //     let frame = loc.unwrap_or_default();
    //
    //     heap.spawn("camera")?
    //         .inject(frame)?
    //         .inject(PlayerCameraController::new("external")?)?
    //         .inject(ScreenCameraController)?;
    //     Ok(())
    // }

    // #[method]
    // pub fn take_control(&self, name: &str, mut heap: HeapMut) -> Result<()> {
    //     let player_id = heap.entity("Player");
    //     if let Some(target_id) = heap.maybe_entity(name) {
    //         heap.entity_by_id_mut(player_id)?
    //             .remove::<PlayerMarker>()?
    //             .rename_numbered("Player_prior_");
    //         heap.entity_by_id_mut(target_id)?
    //             .inject(PlayerMarker)?
    //             .rename("Player");
    //     }
    //     Ok(())
    // }

    fn frame_for_interactive(heap: HeapRef) -> Frame {
        let target = if let Some(arcball) = heap.maybe_get::<ArcBallController>("camera") {
            arcball.target()
        } else if let Some(frame) = heap.maybe_get::<Frame>("camera") {
            let mut geo = frame.geodetic();
            *geo.lat_mut() += degrees!(0.001);
            geo
        } else {
            Geodetic::new(degrees!(0f32), degrees!(0f32), meters!(10f32))
        };
        Frame::from_geodetic_and_bearing(target, Bearing::north())
    }

    #[method]
    pub fn spawn(&self, name: &str, filename: &str, mut heap: HeapMut) -> Result<Value> {
        // Please don't force me to type in all-caps all the time.
        let filename = filename.to_uppercase();

        // Make sure we can load the thing, before creating the entity.
        let xt = {
            let libs = heap.resource::<AssetCatalog>();
            let info = libs.lookup(&filename)?;
            heap.resource::<TypeManager>().load(info)?
        };

        // Load the shape component, if present
        let shape_pkg = load_shape_for_xt("spawn", xt.clone(), heap.as_mut())?;

        // FIXME: layer this such that we can load the core without an ObjectInfo
        // Fake up an info.
        let info = ObjectInfo::from_xt(xt.clone(), name);

        let frame = Self::frame_for_interactive(heap.as_ref());

        // Use the spawn routing shared by mission loading
        let id = Self::spawn_entity_common(
            (name, &info),
            (shape_pkg, frame),
            None as Option<&Mission>,
            None,
            heap.as_mut(),
        )?;
        if name == "Player" {
            heap.entity_by_id_mut(id)?.inject(PlayerMarker)?;
        }

        Ok(Value::True())
    }

    /// Spawn with a temp name, then game.take_control of the new entity.
    // #[method]
    // pub fn spawn_in(&self, filename: &str, mut heap: HeapMut) -> Result<Value> {
    //     let name: String = iter::repeat_with(fastrand::alphanumeric).take(6).collect();
    //     self.spawn(&name, filename, heap.as_mut())?;
    //     self.take_control(&name, heap)?;
    //     Ok(Value::True())
    // }

    fn make_instance_name(map_name: &str, install_name: &str, info: &ObjectInfo) -> Result<String> {
        Ok(if let Some(name) = info.name() {
            name.to_owned()
        } else {
            let shape_name = info.xt().ot()?.shape_file().clone().unwrap_or_default();
            format!(
                "{}_{}_{}",
                install_name,
                make_symbol(&map_name[0..map_name.len() - 3]),
                make_symbol(if !shape_name.is_empty() {
                    &shape_name[0..shape_name.len() - 3]
                } else {
                    "none"
                }),
            )
            .to_lowercase()
        })
    }

    fn spawn_entity_common<T: ObjectProvider>(
        (inst_name, obj_info): (&str, &ObjectInfo),
        (shape_pkg, frame): (Option<ShapeLoadPackage>, Frame),
        object_provider: Option<&T>,
        airport_info: Option<(
            &mut HashMap<Uuid, GeodeBB>,
            &mut HashMap<Uuid, AirportEnvirons>,
        )>,
        mut heap: HeapMut,
    ) -> Result<Entity> {
        // Unpack frame for later use.
        let position = frame.position();
        let facing = frame.facing();

        // Create the entity
        let id = heap
            .spawn_numbered(inst_name)?
            .inject(MissionMarker)?
            .inject(frame)?
            .inject(obj_info.xt())?
            .id();

        // Attach any indicated shape
        let mut shape_bounds = Aabb3::empty();
        let mut ground_offset = meters!(0);
        if let Some(shape_pkg) = shape_pkg {
            shape_bounds = shape_pkg.bounding_box(heap.resource::<Shape>())?;
            ground_offset = shape_pkg.ground_offset(heap.resource::<Shape>())?;
            shape_pkg.inject_into(&mut heap.entity_by_id_mut(id)?)?;
        }

        // If we are a mover type, make sure there is a body motion.
        if obj_info.xt().is_jt() || obj_info.xt().is_nt() || obj_info.xt().is_pt() {
            heap.entity_by_id_mut(id)?.inject(BodyMotion::new_forward(
                feet_per_second!(obj_info.speed()),
                facing,
            ))?;
        }

        // If we are a structure, attach it to the ground permanently. Foundation will make sure
        // that the frame position will follow refinements to the ground height as we load more
        // refined data.
        if obj_info.xt().is_ot_exact() && shape_bounds.is_finite() {
            // Use the corners to get a Geo-aligned bounding box
            // FIXME: we _may_ need to align to the primary during load to get the right BB here
            let corners = shape_bounds.corners().map(|p| position + facing * p);
            let gabb = GeodeticBB::from_world_space_points(&corners);
            let geodebb = GeodeBB::from_geodetic_bb(&gabb);

            // If the object is part of an airport, expand the flattened area to enclose it
            // and set up our runtime structures to maintain the airport.
            if let Some(airport_id) = obj_info.for_airport() {
                if let Some((area, environs)) = airport_info {
                    let segment = obj_info.airport_segment();
                    environs
                        .entry(*airport_id)
                        .and_modify(|environ| environ.insert(id, segment))
                        .or_insert_with(|| AirportEnvirons::new(id, segment));
                    area.entry(*airport_id)
                        .and_modify(|bb| bb.expand_to_include(&geodebb))
                        .or_insert_with(|| geodebb.clone());
                    if let Some(provider) = object_provider {
                        if let AirportSegment::Secondary(primary_id) = segment {
                            let primary_info = provider.object(&primary_id)?;
                            let displacement = primary_info.position().to(*obj_info.position());
                            environs
                                .get_mut(airport_id)
                                .expect("we just set environ")
                                .set_displacement(displacement);
                        }
                    }
                }

                // Add additional offset for airports to make sure the bottom is above the ground level.
                // TODO: do we really need projective texturing for this already?
                ground_offset -= meters!(1);
            }

            // Snap all OT to the ground
            heap.entity_by_id_mut(id)?
                .inject(Foundation::new(ground_offset, &geodebb))?;

            // Create collision for all OT.
            // Note the Runway collison for both primary and secondary, but not objects.
            heap.entity_by_id_mut(id)?.inject(
                if obj_info.is_airport_part() {
                    CollisionBuilder::for_runway(&shape_bounds)
                } else {
                    CollisionBuilder::for_building(&shape_bounds)
                }
                .build(),
            )?;
        }

        // TODO: use audio effect time for timing the animation and effector deployment times?
        // heap.entity_by_id_mut(id)
        // TODO: fuel overrides
        // if let Some(fuel_override) = info.fuel_override() {
        //     heap.get_mut::<VehicleState>(id)
        //         .set_internal_fuel_lbs(fuel_override);
        // }
        // TODO: hardpoint overrides

        // If the type is a plane, install a flight model
        if let Some(pt) = obj_info.xt().maybe_pt() {
            // TODO: figure out how ground contact works and do what we need to make it work here.
            //       probably set it at full gear extension and let springs do work after startup.
            //       But we also need it to follow ground changes, like Foundation. We probably
            //       need to add more code to GeoDB to do foundation-like things for anything with
            //       Dynamics that can take it off the ground.
            let on_ground = obj_info.position().y() == feet!(0);

            // TODO: create a prop power plant with appropriate simulation
            let power = PowerSystem::default().with_engine(Turbojet::new_min_power(obj_info.xt())?);
            let fuel = FuelSystem::default().with_internal_tank(FuelTank::new(
                FuelTankKind::Center,
                kilograms!(*pt.internal_fuel()),
            ))?;

            heap.entity_by_id_mut(id)?
                .inject(ClassicFlightModel::default())?
                .inject(Airframe::new(kilograms!(*obj_info
                    .xt()
                    .ot()?
                    .empty_weight())))?
                .inject(fuel)?
                .inject(power)?
                .inject(PitchInceptor::default())?
                .inject(RollInceptor::default())?
                .inject(YawInceptor::default())?
                .inject(ThrustVectorPitchControl::new(
                    degrees!(-30),
                    degrees!(30),
                    degrees!(1),
                ))?
                .inject(ThrustVectorYawControl::new(
                    degrees!(-30),
                    degrees!(30),
                    degrees!(1),
                ))?
                .inject(VtolAngleControl::new(
                    degrees!(*pt.vt_limit_down()),
                    degrees!(*pt.vt_limit_down()),
                    degrees!(10),
                ))?
                .inject(ThrottleInceptor::new_min_power())?
                .inject(AirbrakeControl::default())?
                .inject(AirbrakeEffector::new(0., Duration::from_millis(1)))?
                .inject(BayControl::default())?
                .inject(ThrustVectorPitchEffector::new(0., Duration::from_millis(1)))?
                .inject(ThrustVectorYawEffector::new(0., Duration::from_millis(1)))?
                .inject(BayEffector::new(0., Duration::from_secs(2)))?
                .inject(FlapsControl::default())?
                .inject(FlapsEffector::new(0., Duration::from_millis(1)))?
                .inject(GearControl::new(on_ground))?
                .inject(GearEffector::new(
                    if on_ground { 1. } else { 0. },
                    Duration::from_secs(4),
                ))?
                .inject(HookControl::default())?
                .inject(HookEffector::new(0., Duration::from_millis(1)))?;
        }

        // TODO: dynamics for ground vehicles

        Ok(id)
    }

    // Like frame_for_interactive, but for normal MM/M loads, we alread have a position
    fn frame_for_mmm_object(obj_info: &ObjectInfo, tile_id: Entity, heap: HeapRef) -> Frame {
        // Use the T2 tile info to find the lat/lon/asl and bearing of our position
        // in order to get a frame. We use the frame to look up ground offset height.
        let tile_mapper = heap.get_by_id::<T2TileSet>(tile_id).mapper();
        let position = tile_mapper.fa_world_to_geodetic(obj_info.position());
        let bearing = Bearing::new(-obj_info.angle().yaw());
        Frame::from_geodetic_and_bearing(position, bearing)
    }

    pub fn load_mmm_common<T: ObjectProvider>(
        &self,
        map_name: &str,
        mission_map: &MissionMap,
        object_provider: &T,
        heap: &mut HeapMut,
    ) -> Result<()> {
        let tile_set = heap.resource_scope(|heap, mut t2_terrain: Mut<T2TerrainBuffer>| {
            t2_terrain.add_map(
                heap.resource::<Installations>().primary_palette(),
                mission_map,
                heap.resource::<AssetCatalog>(),
                heap.resource::<Gpu>(),
            )
        })?;
        let tile_id = heap
            .spawn(make_symbol(map_name))?
            .inject(MissionMarker)?
            .inject(tile_set)?
            .id();

        let install_name = heap.resource::<Installations>().primary().name().to_owned();

        let mut airport_areas: HashMap<Uuid, GeodeBB> = HashMap::new();
        let mut airport_environs: HashMap<Uuid, AirportEnvirons> = HashMap::new();
        for obj_info in object_provider.iter() {
            let shape_pkg = load_shape_for_mmm_object(map_name, obj_info, heap.as_mut())?;
            let inst_name = Self::make_instance_name(map_name, &install_name, obj_info)?;
            let frame = Self::frame_for_mmm_object(obj_info, tile_id, heap.as_ref());

            let id = Self::spawn_entity_common(
                (&inst_name, obj_info),
                (shape_pkg, frame),
                Some(object_provider),
                Some((&mut airport_areas, &mut airport_environs)),
                heap.as_mut(),
            )?;

            if inst_name == "Player" {
                heap.entity_by_id_mut(id)?.inject(PlayerMarker)?;
            }
        }

        // Use the expanded airport areas to spawn some flattening entities.
        for (id, area) in airport_areas.iter() {
            let environs = airport_environs.remove(id).expect("airport environs");
            heap.spawn(format!("{}_flatten_{}", install_name, id))?
                .inject(MissionMarker)?
                .inject(Flatten::new(area))?
                .inject(environs)?;
        }

        // This process just installed a big bunch of Flatteners. Some of these
        // may have modified the terrain under them. Thus, we need to re-upload all
        // some of the tiles. For now let's just clear the cache and let it reload
        // next frame. We just loaded the mission so a framerate hitch is not unexpected.
        heap.resource_mut::<Terrain>().reload_tiles();

        Ok(())
    }

    #[method]
    pub fn unload_mission(&self, mut heap: HeapMut) -> Result<()> {
        let mut work = Vec::new();
        for (entity, _) in heap.query::<(Entity, &MissionMarker)>().iter(heap.world()) {
            work.push(entity);
        }
        println!("Unloading {} mission entities", work.len());
        for entity in work.drain(..) {
            heap.despawn_by_id(entity);
        }
        Ok(())
    }

    #[method]
    pub fn load_map(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        let name = name.to_uppercase();
        if name.starts_with('~') || name.starts_with('$') {
            bail!("cannot load {name}; it is a template (note the ~ or $ prefix)");
        }

        let mm = {
            let libs = heap.resource::<AssetCatalog>();
            let mm_cfn = libs.lookup(&name)?;
            let mm_content = from_dos_string(mm_cfn.data()?);
            MissionMap::from_str(mm_content.as_ref(), heap.resource::<TypeManager>(), libs)?
        };

        self.load_mmm_common(&name, &mm, &mm, &mut heap)?;

        Ok(())
    }

    #[method]
    pub fn load_mission(&self, name: &str, mut heap: HeapMut) -> Result<()> {
        // Unload prior mission
        self.unload_mission(heap.as_mut())?;

        let name = name.to_uppercase();
        let install_name = heap.resource::<Installations>().primary().name().to_owned();

        // FIXME: can we print this in a useful way?
        println!("Loading {install_name}:{name}...");

        let mission = {
            let libs = heap.resource::<AssetCatalog>();
            let m_cfn = libs.lookup(&name)?;
            let m_content = from_dos_string(m_cfn.data()?);
            Mission::from_str(m_content.as_ref(), heap.resource::<TypeManager>(), libs)?
        };

        self.load_mmm_common(&name, mission.mission_map(), &mission, &mut heap)?;

        // Set the time of day at the player load point.
        if let Some(position) = heap
            .maybe_get::<Frame>("Player")
            .map(|p| *p.geodetic().geode())
        {
            let zoned = heap.resource_mut::<Orrery>().get_local_civil(position)?;
            let (hour, minute) = mission.time();
            let local = zoned.with_hour(hour as i8)?.with_minute(minute as i8)?;
            heap.resource_scope(|heap, mut orrery: Mut<Orrery>| {
                orrery.set_local_civil(local, seconds!(5), heap)
            })?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use animate::Timeline;
    use atmosphere::Atmosphere;
    use camera::ScreenCamera;
    use catalog::{CatalogOpts, Search};
    use geodb::GeoDb;
    use installations::LibsOpts;
    use mantle::{Core, CpuDetailLevel, GpuDetailLevel};
    use nitrous::make_partial_test;
    use orrery::Orrery;
    use player::PlayerCameraController;
    use runtime::{StdPaths, StdPathsOpts};
    use terrain::{Terrain, TerrainOpts};

    fn can_load_all_missions(section: usize, total_sections: usize) -> Result<()> {
        // Some missions are templates, apparently
        const SKIP: [&str; 18] = [
            // All these contain `type <selected>`
            "~AA7.M",
            "~AA7V.M",
            "~AAC130.M",
            "~AAV8.M",
            "~AF104.M",
            "~AF14.M",
            "~AF18.M",
            "~AF4B.M",
            "~AF4J.M",
            "~AF8J.M",
            "~AMIG17F.M",
            "~AMIG21F.M",
            "~ASEAHAR.M",
            "~ASU33.M",
            "~AYAK141.M",
            "~INFO.M",
            // Explicity commented as a fake mission for the creator
            "~MC_NATO.M",
            "quick.M",
        ];

        let mut runtime = Core::for_test()?;
        runtime
            .load_extension::<GameLoader>()?
            .load_extension_with::<StdPaths>(StdPathsOpts::new("openfa-test"))?
            .load_extension_with::<AssetCatalog>(CatalogOpts::default())?
            .load_extension_with::<Installations>(LibsOpts::for_testing())?
            .load_extension::<TypeManager>()?
            .load_extension::<GeoDb>()?
            .load_extension::<Timeline>()?
            .load_extension::<Orrery>()?
            .load_extension::<Atmosphere>()?
            .load_extension_with::<Terrain>(TerrainOpts::from_detail(
                CpuDetailLevel::Low,
                GpuDetailLevel::Low,
            ))?
            .load_extension::<T2TerrainBuffer>()?
            .load_extension::<Shape>()?
            .load_extension::<ScreenCamera>()?
            .load_extension::<PlayerCameraController>()?;

        let _fallback_camera_ent = runtime
            .spawn("fallback_camera")?
            .inject(Frame::default())?
            .inject(ArcBallController::default())?
            .id();

        let missions = runtime
            .resource::<AssetCatalog>()
            .search(Search::for_extension("M").in_collection("FA").must_match())?
            .into_iter()
            .filter(|info| !SKIP.contains(&info.name()))
            .filter(|info| !info.name().starts_with("~F"))
            .filter(|info| !info.name().starts_with("~Q"))
            .map(|info| info.name().to_owned())
            .collect::<Vec<String>>();

        let missions = nitrous::segment_tests(section, total_sections, &missions);

        for mission in missions {
            runtime.resource_scope(|heap, assets: Mut<GameLoader>| {
                assets.load_mission(mission, heap)
            })?;
        }

        Ok(())
    }
    make_partial_test!(can_load_all_missions, 15);
}
