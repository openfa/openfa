// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use mmm::ObjectInfo;
use nitrous::HeapMut;
use sh::GearFootprintKind;
use shape::ShapeLoadPackage;
use xt::TypeRef;

/// Load an ObjectInfo's shape and build a ShapeBundle that can be used to inject that shape
/// onto an entity, with enough data to swap out the shape for damage models later, as gameplay
/// may or may not dictate.
///
/// This is where all the side analysis above ShAnalysis lives, e.g. for things that need
/// surrounding context like from the XT or ObjectInfo to determine.
pub fn load_shape_for_mmm_object(
    map_name: &str,
    info: &ObjectInfo,
    heap: HeapMut,
) -> Result<Option<ShapeLoadPackage>> {
    load_shape_for_xt(map_name, info.xt(), heap)
}

/// Load the shape for an XT and build a ShapeLoadPackage that can be used to inject that
/// shape into an entity, with enough data to drive the shape later.
pub fn load_shape_for_xt(
    map_name: &str,
    xt: TypeRef,
    heap: HeapMut,
) -> Result<Option<ShapeLoadPackage>> {
    let Some(shape_file_name) = xt.ot()?.shape_file() else {
        return Ok(None);
    };

    // TODO: figure out the right way to do this. OT work with the SH provided scaling,
    //       but the same scaling over-scales PT significantly. We'll probably need to
    //       adjust these heuristics or find something the OT flags.
    let external_scale = scalar!(if xt.is_ot_exact() { 1. } else { 1. / 3. });

    // Infer the gear layout using everything we know about the xt at this point.
    // We'll need this when loading the SH so that our analysis pass knows where to
    // put the gear when analyzing to find the gear footprint. This lets us get the
    // gear touch points from the analysis rather than carrying them around separately.
    let gear_layout = if let Some(pt) = xt.maybe_pt() {
        if pt.pt_flags().is_helicopter() {
            Some(GearFootprintKind::Skid)
        } else {
            // gear_pitch is used for SeaHarrier... does FA have any tail-draggers?
            Some(GearFootprintKind::Nose)
        }
    } else {
        // TODO: detect tracked vehicles? boats?
        xt.maybe_nt().map(|_| GearFootprintKind::Car)
    };

    Ok(Some(ShapeLoadPackage::with_context(
        map_name,
        &shape_file_name,
        external_scale,
        gear_layout,
        heap,
    )?))
}
