// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};
use xt_parse::{
    extract_table_data, make_brf_conversions, make_xt_struct_family, NameTable, Nothing, NothingIo,
    OutOfLineTable, Value,
};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(u8)]
pub enum TypeTag {
    #[default]
    Ecm = 9,
}

impl TryFrom<u8> for TypeTag {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            9 => Self::Ecm,
            _ => bail!("unknown see TypeTag {}", value),
        })
    }
}

make_brf_conversions!(TypeTag, u8);

// We can detect the version by the number of lines.
// Note: unlike other users, this is inverted: USNF is the
//       one with more lines than expected.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum EcmTypeVersion {
    // All games but USNF
    V0 = 20,
    // USNF only
    V1 = 21,
}

impl EcmTypeVersion {
    fn from_len(n: usize) -> Result<Self> {
        Ok(match n {
            20 => Self::V0,
            21 => Self::V1,
            _ => bail!("unknown ECM type version for length: {}", n),
        })
    }
}

make_xt_struct_family![
EcmType(parent: Nothing, version: EcmTypeVersion) {
    (V0, U8,    struct_type,             TypeTag, Dec, "structType"),
    (V0, Tbl,   si_names,              NameTable, Tbl, "si_names"),
    (V0, U16,   weight,         Mass<PoundsMass>, Dec, "weight"),
    (V0, U8,    externally_loadable,          u8, Hex, "flags"),
    (V0, U16,   jammer_flags,                u16, Hex, "flags"),
    (V0, U8,    chaff_count,                  u8, Dec, "chaffLoaded"),
    (V0, U8,    chaff_chance,                 u8, Dec, "chaffChance"),
    (V0, U8,    chaff_horizontal,             u8, Dec, "chaffH"),
    (V0, U8,    chaff_pitch,                  u8, Dec, "chaffP"),
    (V0, U8,    flares_loaded,                u8, Dec, "flaresLoaded"),
    (V0, U8,    flares_chance,                u8, Dec, "flareChance"),
    (V0, U8,    flares_horizontal,            u8, Dec, "flareH"),
    (V0, U8,    flares_pitch,                 u8, Dec, "flareP"),
    (V0, U8,    radar_jammer_chance,          u8, Dec, "rdChance"),
    (V0, U16,   radar_jammer_rcs_increase,   u16, Dec, "rdSigAdd"),
    (V0, U8,    radar_jammer_max_detect_cone, u8, Dec, "rNoiseMaxDist"),
    (V0, U8,    radar_jammer_min_detect_cone, u8, Dec, "rNoiseMinDist"),
    (V1, U8,    unk_usnf,                     u8, Dec, "unkUsnfWeirdness"),
    (V0, U8,    ir_jammer_chance,             u8, Dec, "irdChance"),
    (V0, U16,   ir_jammer_ics_increase,      u16, Dec, "irdSigAdd"),
    (V0, U8,    ir_jammer_lock_loss_time,     u8, Dec, "irdLoseLockTime")
}];

impl EcmType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = extract_table_data(&lines)?;
        let ecm_lines = lines
            .iter()
            .skip(1)
            .take_while(|vs| **vs != ":si_names")
            .map(|&l| l.trim())
            .filter(|&l| !l.is_empty() && !l.starts_with(';'))
            .collect::<Vec<&str>>();
        Self::from_lines(Nothing, &ecm_lines, &pointers)
    }
}

impl EcmTypeIo {
    pub fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let mut raw = self.serialize_to_lines(tbl);

        // Note: we need to strip out the extra field in USNF
        raw.remove(17);

        let mut out = vec!["[brent's_relocatable_format]".to_owned()];
        out.extend_from_slice(&raw);
        out
    }
}

impl Display for EcmTypeIo {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        write!(f, "{}", rows.join("\r\n"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn can_parse_all_ecm() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("ECM").sort(Order::Asc).must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let ecm = EcmType::from_text(&contents)?;

            assert!(ecm.chaff_chance() == &35 || ecm.chaff_chance() == &0);
            assert!(
                ecm.ir_jammer_chance() == &80 // EA6
                || ecm.ir_jammer_chance() == &50 // F22, SU37
                    || ecm.ir_jammer_chance() == &40
                    || ecm.ir_jammer_chance() == &0
            );
            assert!(
                ecm.weight() == &pounds_mass!(0)
                    || info.name() == "ALE40.ECM"
                    || info.name() == "ALQ167.ECM"
                    || info.name() == "ALQ72.ECM",
                "only a handful of external ECM"
            );
            assert_eq!(
                *ecm.externally_loadable() == 0,
                ecm.weight() == &pounds_mass!(0),
                "external ECMs have a weight"
            );
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_obj() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("ECM")
                .in_collection("FA")
                .must_match(),
        )? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let ecm = EcmType::from_text(&contents)?;
            let ecm_io = EcmTypeIo::from(&ecm);
            let contents2 = ecm_io.to_string();

            for (a, b) in contents.lines().zip(contents2.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_yaml() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("ECM")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            println!("At: {info}");
            let raw0 = info.data()?;
            let contents0 = from_dos_string(raw0);
            let ecm0 = EcmType::from_text(&contents0)?;
            let v0: Value = ecm0.into();
            let yaml = v0.to_yaml()?;
            let v1 = Value::from_yaml(&yaml)?;
            let ecm1 = EcmType::try_from(&v1)?;
            let io1 = EcmTypeIo::from(&ecm1);
            let contents1 = io1.to_string();

            for (a, b) in contents0.lines().zip(contents1.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents0.lines().count(), contents1.lines().count());
        }
        Ok(())
    }
}
