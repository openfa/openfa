// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

// Each T2 file has the following format.
//
// Height "pixels" are stored top to bottom in BIT2 and in one of two strange block formats in
// BITE. There is generally some extra random looking data after the row data. I'm not sure if
// this is some arcane internal detail or vital extra global information. The data stored in the
// extra row does appear to be mostly the same as the pixel format, so maybe it's just scratch or
// overflow for the rendering process? Each height pixel contains 3 bytes, each a field of sorts.
//
// Pixel format:
//   color: u8 =>  0xFF (transparent) for water, or 0xDX or 0xCX for land. These are all mapped to
//                 FF00FF in the default palette. Palette data from LAY files need to be overlayed
//                 into the palette before it is used. The limited color range is probably because
//                 the palette is used to simulate time-of-day; selecting a full and realistic
//                 sunset and sunrise ramp for lots of colors would have been hugely difficult.
//   flags: u8 => appears to modify the section of land or water. Seems to correspond to terrain
//                features or buildings. Water is mostly 0 near-shores and 1 when away from land.
//                This is probably meant to control if we draw wave.sh on it or not. There are also
//                3 to 7 for some maps, maybe naval bases? Land has a wider array of options, but
//                still only 0-E. Only Vietnam has 0x10, and these are dots. Maybe AckAck or SAM
//                emplacements?
//    height: u8 => Seems to only go up to 40 or so at tallest. Not sure why more resolution was
//                  not employed here. Seems a waste. Graphed out, whiteness seems to line up with
//                  the taller points, so I'm pretty sure this is just a simple height-map.

/* color byte usage:
Mostly D2. Some maps have D0 -> DA.
Only Viet has C2->C7.

These are palette indexes into a part of the palette that is not filled in, in the default palette.
These parts of the palette need to be loaded in from LAY files. Presumably, this is so that the
game can change teh palette to simulate sunrise, sunset, and nighttime, as well as different fog
levels and probably other things, just by swapping around the palette.

At a guess, the newer maps only have a single color either because they were expecting most or all
users to be able to run with texture mapping by 1998.

Pakistan          D2
Persian Gulf      D2
Panama            D2
North Vietnam     C2, C4, C5, C6, C7
North/South Korea D2
Iraq              D2
Taiwan            D2
Greece            D2
Egypt             D0, D1, D2, D3, D4, D5, D6, D7, D8, DA
France            D0, D1, D2, D3, D4, D5, D6, D7, D8
Cuba              D2
Vladivostok       D0, D1, D2, D3, D4, D5, D6, D7, D8
The Baltics       D2
Falkland Islands  D2
Kuril Islands     D0, D1, D2, D3, D4, D5, D6, D7, D8
Ukraine           D0, D2, D3, D4, D6, D7, D8, D9
*/

/* Flag byte usage on land
// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 || 16
Pakistan           {4, 3, 2, 0}
Persian Gulf       {2, 4, 0, 6, 3}
Panama             {8, 0, 10, 11, 9, 12, 7}
North Vietnam      {12, 0, 9, 7, 10, 16}
North/South Korea  {12, 8, 7, 0, 10, 11}
Iraq               {4, 3, 6, 0, 2}
Taiwan             {13, 7, 8, 14, 6, 12, 9, 0}
Greece             {2, 4, 0, 3}
Egypt              {0, 3, 2, 4, 6}
France             {4, 6, 2, 3, 0}
Cuba               {10, 8, 9, 7, 0}
Vladivostok        {2, 3, 4, 0, 6}
The Baltics        {3, 0, 2, 4}
Falkland Islands   {1, 3, 0, 4}
Kuril Islands      {0, 2}
Ukraine            {2, 4, 6, 3, 0, 5}
*/

/* Flag byte usage on water
// 0, 1 + 2, 3, 4 || 7, 8, 9
Pakistan           {0, 1}
Persian Gulf       {2, 4, 1, 0}
Panama             {1, 0}
North Vietnam      {1, 0}
North/South Korea  {1, 0}
Iraq               {0, 1}
Taiwan             {7, 8, 1, 0, 9}
Greece             {0, 1, 4}
Egypt              {0, 1}
France             {1, 0}
Cuba               {0, 1}
Vladivostok        {0, 1}
The Baltics        {0, 1}
Falkland Islands   {3, 1, 0}
Kuril Islands      {0, 1}
Ukraine            {0, 1}
*/
#![allow(clippy::transmute_ptr_to_ptr)]

use absolute_unit::{degrees, feet, Angle, Degrees, Feet, Length, LengthUnit};
use anyhow::{bail, ensure, Result};
use base64::prelude::*;
use bitbag::{BitBag, BitBaggable};
use geodesy::Geode;
use image::{DynamicImage, GenericImageView, ImageBuffer, ImageReader, Luma, Rgba};
use lay::Layer;
use log::trace;
use nitrous::{Dict, Value};
use once_cell::sync::Lazy;
use packed_struct::packed_struct;
use pal::Palette;
use std::{collections::HashMap, mem, str};
use strum::{EnumIter, IntoEnumIterator};
use zerocopy::AsBytes;

// Lat/Lon of lower left corner of every map that is shipped with FA.
// TODO: 3rd party maps will need a way to specify. For now we will use a default.
static MAP_POSITIONS: Lazy<HashMap<&'static str, [f32; 2]>> = Lazy::new(|| {
    let mut table = HashMap::new();
    table.insert("Cuba", [25.11, -85.63]);
    table.insert("Egypt", [32.96, 30.35]);
    table.insert("Falkland Islands", [-48.96, -64.75]);
    table.insert("France", [51.57, -0.56]);
    table.insert("Greece", [40.58, 21.24]);
    table.insert("Iraq", [32.44, 44.05]);
    table.insert("Kuril Islands", [52.53, 146.82]);
    table.insert("North Vietnam", [22.35, 105.17]);
    table.insert("North/South Korea", [41.21, 123.98]);
    table.insert("Pakistan", [29.74, 66.46]);
    table.insert("Panama", [11.75, -83.31]);
    table.insert("Persian Gulf", [29.41, 52.72]);
    table.insert("Taiwan", [26.83, 116.7]);
    table.insert("The Baltics", [59.60, 20.80]);
    table.insert("Ukraine", [48.30, 26.70]);
    table.insert("Vladivostok", [45.21, 128.83]);
    table
});

#[allow(non_camel_case_types)]
#[rustfmt::skip]
#[repr(u8)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, BitBaggable, EnumIter)]
pub enum TerrainMod {
    UNK1 = 0x01,
    UNK2 = 0x02,
    UNK3 = 0x04,
    UNK4 = 0x08,
    UNK5 = 0x10,
}

impl TerrainMod {
    pub fn name(&self) -> &'static str {
        match self {
            TerrainMod::UNK1 => "unk1",
            TerrainMod::UNK2 => "unk2",
            TerrainMod::UNK3 => "unk3",
            TerrainMod::UNK4 => "unk4",
            TerrainMod::UNK5 => "unk5",
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Sample {
    pub color: u8,
    pub modifiers: BitBag<TerrainMod>,
    pub height: u8,
}

impl Default for Sample {
    fn default() -> Self {
        Self {
            color: 0xD0,
            modifiers: BitBag::default(),
            height: 0,
        }
    }
}

impl Sample {
    fn from_bytes(data: &[u8]) -> Result<Self> {
        let color = data[0];
        assert!(
            color == 0xFF
                || color == 0xD0
                || color == 0xD1
                || color == 0xD2
                || color == 0xD3
                || color == 0xD4
                || color == 0xD5
                || color == 0xD7
                || color == 0xD6
                || color == 0xD8
                || color == 0xD9
                || color == 0xDA
                || color == 0xC2
                || color == 0xC4
                || color == 0xC5
                || color == 0xC6
                || color == 0xC7
        );
        Ok(Self {
            color,
            modifiers: BitBag::new(data[1])?,
            height: data[2],
        })
    }
}

#[derive(Debug)]
pub struct Terrain {
    name: String,
    pic_file: String,

    // Layout width
    phys_width: u32,
    phys_height: u32,

    // The known good square area of the shape
    logical_width: u32,
    logical_height: u32,

    width_ft: Length<Feet>,
    height_ft: Length<Feet>,
    origin_latitude: Angle<Degrees>,
    origin_longitude: Angle<Degrees>,
    samples: Vec<Sample>,

    // Everything we need to capture to round-trip BIT2 headers.
    raw_height: u32,
    unk0_0: u32,
    unk0_2: u32,
    unk1: u16,
    unk_small: u16,
    remainder: Vec<u8>,
}

impl Terrain {
    pub fn from_bytes(data: &[u8]) -> Result<Self> {
        let magic = &data[0..4];
        Ok(match magic {
            MAGIC_BITE => Self::from_bite(data)?,
            MAGIC_BIT2 => Self::from_bit2(data)?,
            _ => bail!(
                "do not know how to parse a T2 with magic header of {:?}",
                magic
            ),
        })
    }

    fn from_bite(data: &[u8]) -> Result<Self> {
        // Between USNF and MF, the format of the header changed without changing the
        // magic BITE, so we need to do a bit of digging to find out which header to use.
        // The newer format adds a description, so if there is a .PIC after the magic
        // then it is the older format.
        let maybe_pic = read_name(&data[4..19])?;
        if maybe_pic.ends_with(".PIC") {
            return Self::from_bite0(data);
        }
        Self::from_bite1(data)
    }
}

fn read_name(n: &[u8]) -> Result<String> {
    let end_offset: usize = n.iter().position(|&c| c == 0).unwrap_or(n.len() - 1);
    Ok(str::from_utf8(&n[..end_offset])?.to_owned())
}

// This loop handles 13x13 block maps (everything except BAL/KURILE, e.g. only UKR)
// Blocked formats were used for the DOS games (probably to make use of segments and
// near pointers for performance). Only a handful of maps use this format:
//   USNF+MF: Ukraine
//   ATF+NATO:  Egypt, France, Vladivostok
fn unpack_blocks_13x13(
    block_count_x: usize,
    block_count_z: usize,
    blk_size: usize,
    stride: usize,
    height: usize,
    entries: &[u8],
) -> Result<Vec<Sample>> {
    ensure!(block_count_x == 13, "can't handle other sizes");
    let mut samples = vec![Default::default(); stride * height];
    let mut off = 12 * 3; // Looks like there's 4 uints?
    for blkz in 0..block_count_z {
        for blkx in 0..block_count_x {
            // For each pixel in the block from bottom to top...
            for j in 0..blk_size {
                for i in 0..blk_size {
                    let data = &entries[off..off + 3];
                    off += 3;
                    let mut x_pos = blkx * blk_size + i;
                    let mut z_pos = blkz * blk_size + (j + 4) % 16;
                    if j >= 12 {
                        x_pos = (x_pos + 16) % 208;
                        if blkx == 12 {
                            z_pos = (z_pos + 16) % 208;
                        }
                    }
                    let index = z_pos * stride + x_pos;
                    samples[index] = Sample::from_bytes(data)?;
                }
            }
        }
    }
    Ok(samples)
}

// While the second DOS game, ATF, used a different format, it kept to 13x13 maps
// and uses the same de-blocking algorithm under the hood. The expansions, however,
// upped the ante with larger 16x16 tile maps:
//    NATO: BAL.T2
//    MF: KURILE.T2
//
// FIXME: There's almost certainly a common algorithm here that differs only by the block
//        size. The difference is that the 13th block seems to wrap, but needs to be block
//        aware to wrap correctly. This is maybe also true here, but dodged because of the
//        power of 2 block size?
fn unpack_blocks_16x16(
    block_count_x: usize,
    block_count_z: usize,
    blk_size: usize,
    stride: usize,
    height: usize,
    entries: &[u8],
) -> Result<Vec<Sample>> {
    // This loop works for 16x16 block maps (BAL/KURILE)
    let mut samples = vec![Default::default(); stride * height];
    let mut off = 0;
    for blkz in 0..block_count_z {
        for blkx in 0..block_count_x {
            // For each pixel in the block from bottom to top...
            for j in 0..blk_size {
                for i in 0..blk_size {
                    let data = &entries[off..off + 3];
                    off += 3;
                    let x_pos = blkx * blk_size + i;
                    let z_pos = blkz * blk_size + j;
                    let index = z_pos * stride + x_pos;
                    samples[index] = Sample::from_bytes(data)?;
                }
            }
        }
    }
    Ok(samples)
}

const MAGIC_BITE: &[u8] = b"BITE";

/*
The earlier BITE format has the pic right after the magic and a big
string of 0 bytes in the middle of the important bits.

After USNF, it looks like the BITE format changed without changing the
magic word. Now there is a description / name after the magic and the pic
is moved lower. There is also a big block of zeros removed from the middle.
*/
#[packed_struct]
struct BITEHeader0 {
    magic: [u8; 4],

    pic_file: [u8; 16],

    pad0: [u8; 38],

    unk0: u32,
    unk1: u32,
    unk2: u32,
    unk3: u32,

    pad1: [u8; 77],

    unk4: u16,

    width_ft: u16,
    height_ft: u16,

    unk5: [u8; 5],

    phys_width: u16,
    phys_height: u16,

    block_size: u16,
    block_count_x: u16,
    block_count_z: u16,
}

impl Terrain {
    // This format is used exclusively by the first game in the series: USNF.
    // It has the same BITE header as in later games, but is missing the name
    // field.
    fn from_bite0(data: &[u8]) -> Result<Self> {
        let header = BITEHeader0::overlay_prefix(data)?;
        ensure!(header.magic() == MAGIC_BITE, "missing magic");
        ensure!(header.pad0() == [0u8; 38], "pad0");
        ensure!(header.pad1() == [0u8; 77], "pad1");

        let pic_file = read_name(&header.pic_file())?;

        trace!(
            "T2:BIT0: {:08X} {:08X} {:08X} {:08X} - {:04X} {:04X} {:04X} - {:?} - {}x{} [{}, {}, {}]",
            header.unk0(),
            header.unk1(),
            header.unk2(),
            header.unk3(),
            header.unk4(),
            header.width_ft(),
            header.height_ft(),
            header.unk5(),
            header.phys_width(),
            header.phys_height(),
            header.block_size(),
            header.block_count_x(),
            header.block_count_z(),
        );

        // Note physical decode size is always 208x208, but we need to display as 200x200.
        // Presumably this allows the software renderer to wrap at edges efficiently while
        // only loading relevant blocks, but it makes the decode process a bit confusing.
        ensure!(header.phys_width() == 208, "expected 208 wide bite0");
        ensure!(header.phys_height() == 208, "expected 208 high bite0");

        // We can now skip the row offsets block to get to the height entries.
        let offsets_start = mem::size_of::<BITEHeader0>();
        let offsets_size = header.phys_height() as usize * mem::size_of::<u32>();
        let stride = header.phys_width() as usize;
        let height = header.phys_height() as usize;
        let data_start = offsets_start + offsets_size;
        let entries = &data[data_start..];

        let blk_size = header.block_size() as usize;
        ensure!(blk_size == 16, "expect block size of 16");
        let block_count_z = header.block_count_z() as usize;
        let block_count_x = header.block_count_x() as usize;
        ensure!(block_count_x == 13, "can't handle other sizes");
        ensure!(block_count_x == block_count_z, "only support square maps");

        let samples = unpack_blocks_13x13(
            block_count_x,
            block_count_z,
            blk_size,
            stride,
            height,
            entries,
        )?;

        let name = "Ukraine".to_owned();
        let [lat_deg, lon_deg] = MAP_POSITIONS.get::<str>(&name).unwrap_or(&[0f32, 0f32]);

        let terrain = Terrain {
            name,
            pic_file,
            width_ft: feet!((header.width_ft() as u32) << 8),
            height_ft: feet!((header.height_ft() as u32) << 8),
            phys_width: header.phys_width() as u32,
            phys_height: header.phys_height() as u32,
            logical_width: 200,
            logical_height: 200,
            origin_latitude: degrees!(*lat_deg),
            origin_longitude: degrees!(*lon_deg),
            samples,

            raw_height: header.phys_height() as u32,
            unk0_0: 0,
            unk0_2: 0,
            unk1: 0,
            unk_small: 0,
            remainder: vec![],
        };
        Ok(terrain)
    }
}

#[packed_struct]
struct BITEHeader1 {
    magic: [u8; 4],

    name: [u8; 80],
    pic_file: [u8; 15],

    unk0: [u32; 5],
    unk_pad0: [u8; 1],
    unk_a: u16,

    width_ft: u16,
    height_ft: u16,

    unk_after: [u8; 5],

    phys_width: u16,
    phys_height: u16,

    block_size: u16,
    block_count_z: u16,
    block_count_x: u16,
}

impl Terrain {
    fn from_bite1(data: &[u8]) -> Result<Self> {
        let header = BITEHeader1::overlay_prefix(data)?;
        ensure!(header.magic() == MAGIC_BITE, "missing magic");

        let name = read_name(&header.name())?;
        let pic_file = read_name(&header.pic_file())?;
        let [lat_deg, lon_deg] = MAP_POSITIONS.get::<str>(&name).unwrap_or(&[0f32, 0f32]);

        trace!(
            "T2:BITE1: {} {:?} {:?} {:04X} {:?}- {}x{} ({:04X}x{:04X}ft) [{}, {}, {}]",
            name,
            header.unk0(),
            header.unk_pad0(),
            header.unk_a(),
            header.unk_after(),
            header.phys_width(),
            header.phys_height(),
            header.width_ft(),
            header.height_ft(),
            header.block_size(),
            header.block_count_z(),
            header.block_count_x(),
        );

        // We can now skip the row offsets block to get to the height entries.
        let offsets_start = mem::size_of::<BITEHeader1>();
        let offsets_size = header.phys_height() as usize * mem::size_of::<u32>();
        let stride = header.phys_width() as usize;
        let height = header.phys_height() as usize;
        let data_start = offsets_start + offsets_size;
        let entries = &data[data_start..];

        let blk_size = header.block_size() as usize;
        ensure!(blk_size == 16, "expect block size of 16");
        let block_count_z = header.block_count_z() as usize;
        let block_count_x = header.block_count_x() as usize;
        ensure!(block_count_x == block_count_z, "only support square maps");

        let samples = if block_count_x == 16 {
            unpack_blocks_16x16(
                block_count_x,
                block_count_z,
                blk_size,
                stride,
                height,
                entries,
            )?
        } else {
            unpack_blocks_13x13(
                block_count_x,
                block_count_z,
                blk_size,
                stride,
                height,
                entries,
            )?
        };

        // Seems to be legacy from bite0. I expect they translated the weird wrapping layout
        // into a more reasonable block layout and forgot to clean up the 8px fringe.
        let mut logical_width = header.phys_width();
        let mut logical_height = header.phys_height();
        if logical_width == 208 {
            logical_width = 200;
            logical_height = 200;
        }

        let terrain = Terrain {
            name,
            pic_file,
            width_ft: feet!((header.width_ft() as u32) << 8),
            height_ft: feet!((header.height_ft() as u32) << 8),
            phys_width: header.phys_width() as u32,
            phys_height: header.phys_height() as u32,
            logical_width: logical_width as u32,
            logical_height: logical_height as u32,
            origin_latitude: degrees!(*lat_deg),
            origin_longitude: degrees!(*lon_deg),
            samples,

            raw_height: header.phys_height() as u32,
            unk0_0: 0,
            unk0_2: 0,
            unk1: 0,
            unk_small: 0,
            remainder: vec![],
        };
        Ok(terrain)
    }
}

const MAGIC_BIT2: &[u8] = b"BIT2";

#[packed_struct]
struct BIT2Header {
    magic: [u8; 4],
    name: [u8; 80],
    pic_file: [u8; 15],

    unk0: [u32; 6],

    width_ft: u32,
    height_ft: u32,

    unk_zero: u16,
    unk1: u16,
    unk_small: u16,

    width: u32,
    height: u32,

    header_length: u32,
}

/*
writer.Write(Encoding.ASCII.GetBytes("BIT2"));  // Magic
writer.Write(Encoding.ASCII.GetBytes(mapName.PadRight(80, (char)0x00))); // In a good implementation we would check that mapName < 80 bytes
writer.Write(Encoding.ASCII.GetBytes(mapPic.PadRight(15, (char)0x00))); // In a good implementation we would check that mapPic < 15 bytes
writer.Write((uint) 8192); // Map picture dimension X
writer.Write((uint) 0);    // No idea.
writer.Write((uint) 8192); // Map picture dimension Y
writer.Write(new byte[10]); // There's a big gap here.
writer.Write((uint) 8); // Flag? I think they got lazy and made integer-width enums for something. Could end up looking like: 00111?
writer.Write((uint) 16);    // Flag?
writer.Write((uint) 32);    // Flag
writer.Write((uint) (x*y*3) + 149); //442368 + 149 = 442517 assuming 384x384.
writer.Write((uint) x); // X tiles
writer.Write((uint) y); // Y tiles
writer.Write((uint) 149); // Header length. If this value is not equal to the actual header length then you goofed badly.
*/

impl Terrain {
    fn from_bit2(data: &[u8]) -> Result<Self> {
        let header = BIT2Header::overlay_prefix(data)?;

        // 4 byte of magic
        ensure!(header.magic() == MAGIC_BIT2, "missing magic");

        // 80 bytes of name / description
        let name = read_name(&header.name())?;
        let [lat_deg, lon_deg] = MAP_POSITIONS.get::<str>(&name).unwrap_or(&[0f32, 0f32]);

        // Followed by 15 bytes containing the pic file.
        let pic_file = read_name(&header.pic_file())?;
        trace!("Loaded T2 with name: {}, pic_file: {}", name, pic_file);

        // Followed by a bunch of ints.
        ensure!(header.unk0()[1] == 0, "expected 0 in unk0[1]");
        ensure!(header.unk0()[3] == 0, "expected 0 in unk0[3]");
        ensure!(header.unk0()[4] == 0, "expected 0 in unk0[4]");
        ensure!(header.unk0()[5] == 524_288, "expected 524288 in unk0[5]");
        if header.unk_small() == 3 {
            ensure!(header.width() == 256, "if 3, expect 256");
            ensure!(header.height() == 256, "if 3, expect 256");
        }
        ensure!(
            header.header_length() == mem::size_of::<BIT2Header>().try_into()?,
            "expected header size field"
        );
        trace!(
            "BITE2: {:?} {:08X} {:?}; {}x{} (0x{:06X}x{:06X}ft)",
            header.unk0(),
            header.unk1(),
            header.unk_small(),
            header.width(),
            header.height(),
            header.width_ft(),
            header.height_ft(),
        );

        // Followed by one 3-byte entry for each sample in width/height.
        let npix = (header.width() * header.height()) as usize;
        let data_start = mem::size_of::<BIT2Header>();
        let data_end = data_start + npix * 3;
        let entries = &data[data_start..data_end];
        let mut samples = Vec::new();
        for i in 0..npix {
            samples.push(Sample::from_bytes(&entries[i * 3..i * 3 + 3])?)
        }

        // Followed by some more rows of samples with seemingly semi-random data in them.
        // The rows may not be complete, so be sure to take only the complete rows for samples.
        let trailer = &data[data_end..];
        ensure!(trailer.len() % 3 == 0, "trailer has samples");
        let extra_rows = trailer.len() as u32 / 3u32 / header.width();
        let extra_rows_end = data_end + (extra_rows * 3 * header.width()) as usize;
        let trailer = &data[data_end..extra_rows_end];
        for i in 0..trailer.len() / 3 {
            samples.push(Sample::from_bytes(&trailer[i * 3..i * 3 + 3])?)
        }
        let remainder = &data[extra_rows_end..];

        let terrain = Terrain {
            name,
            pic_file,
            width_ft: feet!(header.width_ft()),
            height_ft: feet!(header.height_ft()),
            phys_width: header.width(),
            phys_height: header.height() + extra_rows,
            logical_width: header.width().min(header.height()),
            logical_height: header.height().min(header.width()),
            origin_latitude: degrees!(*lat_deg),
            origin_longitude: degrees!(*lon_deg),
            samples,

            raw_height: header.height(),
            unk0_0: header.unk0()[0],
            unk0_2: header.unk0()[2],
            unk1: header.unk1(),
            unk_small: header.unk_small(),
            remainder: remainder.to_owned(),
        };
        Ok(terrain)
    }

    pub fn to_bytes(&self) -> Result<Vec<u8>> {
        let mut header = BIT2Header {
            magic: [0u8; 4],
            name: [0u8; 80],
            pic_file: [0u8; 15],
            unk0: [self.unk0_0, 0, self.unk0_2, 0, 0, 524_288],
            width_ft: self.width_ft.f32() as u32,
            height_ft: self.height_ft.f32() as u32,
            unk_zero: 0,
            unk1: self.unk1,
            unk_small: self.unk_small,
            width: self.phys_width,
            height: self.raw_height,
            header_length: mem::size_of::<BIT2Header>() as u32,
        };
        header.magic.copy_from_slice(MAGIC_BIT2);
        header.name[0..self.name.as_bytes().len()].copy_from_slice(self.name.as_bytes());
        header.pic_file[0..self.pic_file.as_bytes().len()]
            .copy_from_slice(self.pic_file.as_bytes());
        let mut out = header.as_bytes().to_owned();
        for sample in &self.samples {
            out.push(sample.color);
            out.push(sample.modifiers.into_inner());
            out.push(sample.height);
        }
        out.extend(&self.remainder);

        Ok(out)
    }

    /*
    The french map is:
        Total (/208):
           miles ->      290 (1.4)
           meters -> 466,710 (2243)
           feet -> 1,531,000 (7360)

           miles ->      300 (1.44)
           meters -> 482,803 (2321)
           feet -> 1,584,000 (7615)

    Possible scales:
        0x00000008 => 8
        0x00000800 => 2048
        0x00080000 => 524288

        0x00000019 => 25
        0x00001900 => 6400
        0x00190000 => 1638400    <- most likely unit is feet
        0x19000000 => 419,430,400

        0x0000001a => 26
        0x00001a00 => 6656
        0x001a0000 => 1703936

    The cuban map is:
        Size: 256x256
        miles:      343 (1.34)
        meters: 552,005 (2,156)
        feet: 1,811,040 (7,074)

     Possible scales:
        0x0000001a => 26
        0x00001a00 => 6656
        0x001a0000 => 1703936   <- most likely is again feet
    */

    pub fn make_palette(
        layer: &Layer,
        layer_index: usize,
        system_palette: &Palette,
    ) -> Result<Palette> {
        let layer_data = layer.for_index(layer_index)?;
        let r0 = layer_data.slice(0x00, 0x10)?;
        let r1 = layer_data.slice(0x10, 0x20)?;
        let r2 = layer_data.slice(0x20, 0x30)?;
        let r3 = layer_data.slice(0x30, 0x40)?;

        // We need to put rows r0, r1, and r2 into into 0xC0, 0xE0, 0xF0 somehow.
        let mut palette = system_palette.clone();
        palette.overlay_at(&r0, 0xE0 - 1)?;
        palette.overlay_at(&r1, 0xF0 - 1)?;
        palette.overlay_at(&r2, 0xC0)?;
        palette.overlay_at(&r3, 0xD0)?;
        palette.override_one(0xFF, [0; 4]);

        Ok(palette)
    }

    pub fn terrain_color_indices() -> &'static [u8; 17] {
        &[
            0xC2, 0xC4, 0xC5, 0xC6, 0xC7, 0xD0, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8,
            0xD9, 0xDA, 0xFF,
        ]
    }

    // The layer palettes are not invertable in general or in specific.
    // With only a couple exceptions, the colors that _are_ used are invertable,
    // but not only do we not know what palette index was used, different
    // origin terrains would have used different index ranges. Thus, we
    // use a fake palette based on a real one with the terrain's color range
    // explicitly de-duped. We follow the same de-dup algorithm going in and
    // going out, so we'll get the same results each time, as long as the same
    // MM and LAY are used.
    fn make_export_palette(palette: &Palette) -> HashMap<Rgba<u8>, usize> {
        let mut inv_palette = HashMap::new();
        for index in Self::terrain_color_indices() {
            let mut color = palette.rgba(*index as usize);
            while inv_palette.contains_key(&color) {
                color.0[2] += 1;
            }
            let prior = inv_palette.insert(color, *index as usize);
            assert!(prior.is_none());
        }
        inv_palette
    }

    pub fn serialize_layers(&self, palette: &Palette, value: &mut Value) -> Result<()> {
        let color_path = self.color_export_name();
        let height_path = self.height_export_name();
        let mask_paths = TerrainMod::iter()
            .map(|modifier| {
                (
                    format!("{}_map", modifier.name()),
                    self.mask_export_name(modifier),
                )
            })
            .collect::<Vec<_>>();

        // Put the layers on the filesystem
        let (color, height, masks) = self.make_layers(palette)?;
        color.save(&color_path)?;
        height.save(&height_path)?;
        for (i, (_, path)) in mask_paths.iter().enumerate() {
            masks[i].save(path)?;
        }

        // Include a palette for reference to additional colors that may be available.
        // Note: clobber all colors outside of what is currently used for clarity on what's usable.
        let mut option_palette = palette.to_owned();
        let indices = Self::terrain_color_indices();
        for offset in 0..256 {
            if !indices.contains(&(offset as u8)) {
                option_palette.set_rgba(offset, Rgba([255, 0, 255, 255]));
            }
        }
        Palette::dump_partial(&option_palette.as_bytes(), 4, &self.palette_export_base())?;

        // Extend map with keys describing where we put the layers
        let map = value.to_dict_mut()?;
        ensure!(!map.contains_key("base_color_map"));
        map.insert("base_color_map", color_path.into());
        map.insert("height_map", height_path.into());
        for (key, path) in &mask_paths {
            map.insert(key, path.into());
        }

        Ok(())
    }

    fn make_layers(
        &self,
        palette: &Palette,
    ) -> Result<(
        ImageBuffer<Rgba<u8>, Vec<u8>>,
        ImageBuffer<Luma<u8>, Vec<u8>>,
        [ImageBuffer<Luma<u8>, Vec<u8>>; 5],
    )> {
        // Note: invert the inverse palette to get our forward palette
        let inv_palette = Self::make_export_palette(palette);
        let mut fwd_palette = HashMap::new();
        for (clr, index) in inv_palette.iter() {
            fwd_palette.insert(*index, *clr);
        }

        let mut color_buf = ImageBuffer::new(self.phys_width, self.phys_height);
        let mut height_buf = ImageBuffer::new(self.phys_width, self.phys_height);
        let mut mask_bufs = [
            ImageBuffer::new(self.phys_width, self.phys_height),
            ImageBuffer::new(self.phys_width, self.phys_height),
            ImageBuffer::new(self.phys_width, self.phys_height),
            ImageBuffer::new(self.phys_width, self.phys_height),
            ImageBuffer::new(self.phys_width, self.phys_height),
        ];
        for (pos, sample) in self.all_samples().iter().enumerate() {
            let w = (pos % self.phys_width as usize) as u32;
            let h = (self.phys_height as usize - (pos / self.phys_width as usize) - 1) as u32;

            // Note: height is limited to 0..31 (5-bit) for whatever reason.
            //       We emit the height plane at 4x (0..255) so we can edit in grayscale.
            //       Note that precision will be lost on import as we will divide back out.
            let height_out = sample.height.checked_mul(8).expect("height too big");
            height_buf.put_pixel(w, h, Luma([height_out]));

            // Note: each mask layer gets its own image
            for (i, modifier) in TerrainMod::iter().enumerate() {
                if sample.modifiers.is_set(modifier) {
                    mask_bufs[i].put_pixel(w, h, Luma([255]));
                }
            }

            // Serialize the color at that palette, given an overlaid Layer
            // We'll do a closest match search on the palette when coming back in.
            let color = fwd_palette
                .get(&(sample.color as usize))
                .expect("limited color range");
            color_buf.put_pixel(w, h, *color);
        }

        Ok((color_buf, height_buf, mask_bufs))
    }

    pub fn samples_from_layers(
        phys_width: u32,
        phys_height: u32,
        palette: &Palette,
        color_buf: DynamicImage,
        height_buf: DynamicImage,
        mask_bufs: &[DynamicImage],
    ) -> Result<Vec<Sample>> {
        ensure!(phys_width == color_buf.width());
        ensure!(phys_width == height_buf.width());
        ensure!(phys_height == color_buf.height());
        ensure!(phys_height == height_buf.height());
        for mask_buf in mask_bufs {
            ensure!(phys_width == mask_buf.width());
            ensure!(phys_height == mask_buf.height());
        }

        let inv_palette = Self::make_export_palette(palette);

        let mut samples = vec![];
        for pos in 0..(phys_width * phys_height) as usize {
            let x = (pos % phys_width as usize) as u32;
            let y = (phys_height as usize - (pos / phys_width as usize) - 1) as u32;

            // Use the layer to find the closest matching color index
            let color = color_buf.get_pixel(x, y);
            let Some(palette_index) = inv_palette.get(&color).cloned() else {
                bail!("Color plane contains colors not in the palette");
            };

            // Invert the scaling operation we do on export; lose any precision the image gained.
            let height = height_buf.get_pixel(x, y);
            let height_in = height.0[0] / 8;

            // Reconstruct the modifier bit from the masks.
            let mut pixel_modifier = BitBag::<TerrainMod>::default();
            for (i, modifier) in TerrainMod::iter().enumerate() {
                if mask_bufs[i].get_pixel(x, y).0[0] >= 127 {
                    pixel_modifier |= modifier;
                }
            }

            samples.push(Sample::from_bytes(&[
                palette_index.try_into()?,
                pixel_modifier.into_inner(),
                height_in,
            ])?);
        }
        Ok(samples)
    }

    // Reconstruct the terrain from a serialzied value and layers
    pub fn deserialize_layers(palette: &Palette, value: &Value) -> Result<Self> {
        let map = value.to_dict()?;
        let name = map.try_get("name")?.to_str()?.to_owned();
        let pic_file = map.try_get("pic_file")?.to_str()?.to_owned();
        let phys_width: u32 = map.try_get("phys_width")?.to_int()?.try_into()?;
        let phys_height: u32 = map.try_get("phys_height")?.to_int()?.try_into()?;
        let logical_width: u32 = map.try_get("logical_width")?.to_int()?.try_into()?;
        let logical_height: u32 = map.try_get("logical_height")?.to_int()?.try_into()?;
        let width_ft: Length<Feet> = map.try_get("width")?.try_into()?;
        let height_ft: Length<Feet> = map.try_get("height")?.try_into()?;
        let origin_latitude: Angle<Degrees> = map.try_get("origin_latitude")?.try_into()?;
        let origin_longitude: Angle<Degrees> = map.try_get("origin_longitude")?.try_into()?;
        let raw_height = map.try_get("raw_height")?.try_into()?;
        let unk0_0 = map.try_get("unk0_0")?.try_into()?;
        let unk0_2 = map.try_get("unk0_2")?.try_into()?;
        let unk1 = map.try_get("unk1")?.try_into()?;
        let unk_small = map.try_get("unk_small")?.try_into()?;
        let remainder = map.try_get("remainder")?.to_str()?.to_owned();

        let color_map = ImageReader::open(map.try_get("base_color_map")?.to_str()?)?.decode()?;
        let height_map = ImageReader::open(map.try_get("height_map")?.to_str()?)?.decode()?;
        let mut mask_maps = vec![];
        for modifier in TerrainMod::iter() {
            let key = format!("{}_map", modifier.name());
            let filename = map.try_get(key)?.to_str()?;
            mask_maps.push(ImageReader::open(filename)?.decode()?);
        }
        let samples = Terrain::samples_from_layers(
            phys_width,
            phys_height,
            palette,
            color_map,
            height_map,
            &mask_maps,
        )?;

        Ok(Self {
            name,
            pic_file,
            phys_width,
            phys_height,
            logical_width,
            logical_height,
            width_ft,
            height_ft,
            origin_latitude,
            origin_longitude,
            samples,
            raw_height,
            unk0_0,
            unk0_2,
            unk1,
            unk_small,
            remainder: BASE64_STANDARD.decode(remainder)?,
        })
    }

    fn export_base(&self) -> String {
        self.name.replace([' ', '/'], "_").to_ascii_lowercase()
    }

    pub fn color_export_name(&self) -> String {
        self.export_base() + ".color.png"
    }

    pub fn height_export_name(&self) -> String {
        self.export_base() + ".heights.png"
    }

    pub fn mask_export_name(&self, modifier: TerrainMod) -> String {
        self.export_base() + ".mask." + modifier.name() + ".png"
    }

    pub fn palette_export_base(&self) -> String {
        self.export_base() + ".PAL"
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn pic_file(&self) -> &str {
        &self.pic_file
    }

    pub fn extent_east_west<Unit: LengthUnit>(&self) -> Length<Unit> {
        (&self.width_ft).into()
    }

    pub fn extent_north_south<Unit: LengthUnit>(&self) -> Length<Unit> {
        (&self.height_ft).into()
    }

    pub fn origin_latitude(&self) -> Angle<Degrees> {
        self.origin_latitude
    }

    pub fn origin_longitude(&self) -> Angle<Degrees> {
        self.origin_longitude
    }

    pub fn origin(&self) -> Geode {
        Geode::new(self.origin_latitude, self.origin_longitude)
    }

    pub fn width(&self) -> u32 {
        self.logical_width
    }

    pub fn height(&self) -> u32 {
        self.logical_height
    }

    pub fn physical_width(&self) -> u32 {
        self.phys_width
    }

    pub fn physical_height(&self) -> u32 {
        self.phys_height
    }

    pub fn npix(&self) -> usize {
        (self.logical_width * self.logical_height) as usize
    }

    fn samples(&self) -> &[Sample] {
        &self.samples[0..self.npix()]
    }

    fn all_samples(&self) -> &[Sample] {
        &self.samples
    }

    pub fn sample_at(&self, xi: u32, zi: u32) -> Sample {
        let offset = (zi * self.phys_width + xi) as usize;
        if offset < self.samples.len() {
            self.samples[offset]
        } else {
            // Note: clamp-to-edge semantics with a pixel border
            let offset = ((zi - 1) * self.phys_width + xi) as usize;
            if offset < self.samples().len() {
                self.samples[offset]
            } else {
                let offset = ((zi - 1) * self.phys_width + (xi - 1)) as usize;
                self.samples[offset]
            }
        }
    }
}

impl From<Terrain> for Value {
    fn from(terrain: Terrain) -> Self {
        (&terrain).into()
    }
}

impl From<&Terrain> for Value {
    fn from(terrain: &Terrain) -> Self {
        let mut map = Dict::default();
        map.insert("name", terrain.name().into());
        map.insert("pic_file", terrain.pic_file().into());
        map.insert("phys_width", terrain.phys_width.into());
        map.insert("phys_height", terrain.phys_height.into());
        map.insert("logical_width", terrain.logical_width.into());
        map.insert("logical_height", terrain.logical_height.into());
        map.insert("width", terrain.width_ft.into());
        map.insert("height", terrain.height_ft.into());
        map.insert("origin_latitude", terrain.origin_latitude.into());
        map.insert("origin_longitude", terrain.origin_longitude.into());
        map.insert("raw_height", terrain.raw_height.into());
        map.insert("unk0_0", terrain.unk0_0.into());
        map.insert("unk0_2", terrain.unk0_2.into());
        map.insert("unk1", terrain.unk1.into());
        map.insert("unk_small", terrain.unk_small.into());
        map.insert(
            "remainder",
            BASE64_STANDARD.encode(&terrain.remainder).into(),
        );
        Value::Dict(map)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::{from_dos_string, Installations};
    use std::fs;
    use std::path::PathBuf;

    #[test]
    fn it_can_parse_all_t2_files() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("T2").must_match())? {
            println!("At: {info}");
            let _terrain = Terrain::from_bytes(&info.data()?)?;
        }

        Ok(())
    }

    #[test]
    fn it_can_roundtrip() -> Result<()> {
        let (catalog, installations) = Installations::for_testing()?;
        let typeman = xt::TypeManager::default();
        let system_palette = installations.palette("FA")?;

        for info in catalog.search(
            Search::for_extension("T2")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            println!("At: {info}");
            let bytes0 = &info.data()?;
            let terrain0 = Terrain::from_bytes(bytes0)?;

            // Look up the MM to get the LAY name
            let t2_path: PathBuf = info.name().into();
            let mm_path = t2_path.with_extension("MM");
            let mm_fp = catalog.lookup(mm_path.to_str().expect("ascii"))?;
            let mm_content = from_dos_string(mm_fp.data()?);
            let mm = mmm::MissionMap::from_str(&mm_content, &typeman, &catalog)?;
            let lay_index = if mm.layer_index() != 0 {
                mm.layer_index()
            } else {
                2
            };
            let lay_name = mm.layer_name().filename();

            // Load the LAY to get palette indices to export
            let layer_fp = catalog.lookup(lay_name)?;
            let layer = Layer::from_bytes(&layer_fp.data()?, system_palette)?;

            // Build a palette from the layer
            let palette = Terrain::make_palette(&layer, lay_index, system_palette)?;

            // Serialize to value, write layers and extend value with paths
            let mut v0: Value = (&terrain0).into();
            terrain0.serialize_layers(&palette, &mut v0)?;

            // Round trip through yaml
            let yaml = v0.to_yaml()?;
            let v1: Value = Value::from_yaml(&yaml)?;
            assert_eq!(v0, v1);

            // Deserialize layers back into a terrain and check that we round-tripped
            let terrain1 = Terrain::deserialize_layers(&palette, &v1)?;
            assert_eq!(terrain0.name, terrain1.name);
            assert_eq!(terrain0.pic_file, terrain1.pic_file);
            assert_eq!(terrain0.phys_width, terrain1.phys_width);
            assert_eq!(terrain0.phys_height, terrain1.phys_height);
            assert_eq!(terrain0.logical_width, terrain1.logical_width);
            assert_eq!(terrain0.logical_height, terrain1.logical_height);
            assert_eq!(terrain0.width_ft, terrain1.width_ft);
            assert_eq!(terrain0.height_ft, terrain1.height_ft);
            assert_eq!(terrain0.samples.len(), terrain1.samples.len());
            assert_eq!(terrain0.samples, terrain1.samples);

            let bytes1 = terrain1.to_bytes()?;
            assert_eq!(bytes0.len(), bytes1.len());
            for (i, (a, b)) in bytes0.iter().zip(bytes1.iter()).enumerate() {
                assert_eq!(a, b, "{a:02X} != {b:02X} at offset {i:04X}");
            }
            assert_eq!(bytes0.as_ref(), &bytes1);

            // TODO: serialize back into a T2 and check that the bytes match

            // Note: delete the linked images whether we succeeded or not
            fs::remove_file(terrain0.color_export_name())?;
            fs::remove_file(terrain0.height_export_name())?;
            for modifier in TerrainMod::iter() {
                fs::remove_file(terrain0.mask_export_name(modifier))?;
            }
            fs::remove_file(terrain0.palette_export_base() + ".png")?;
        }

        Ok(())
    }
}
