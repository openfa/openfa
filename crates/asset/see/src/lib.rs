// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};
use xt_parse::{
    extract_table_data, make_brf_conversions, make_xt_struct_family, NameTable, Nothing, NothingIo,
    OutOfLineTable, Value,
};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(u8)]
pub enum TypeTag {
    #[default]
    Seeker = 10,
}

impl TryFrom<u8> for TypeTag {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            10 => Self::Seeker,
            _ => bail!("unknown see TypeTag {}", value),
        })
    }
}

make_brf_conversions!(TypeTag, u8);

// We can detect the version by the number of lines.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum SeeTypeVersion {
    V0 = 25,
}

impl SeeTypeVersion {
    fn from_len(n: usize) -> Result<Self> {
        Ok(match n {
            25 => Self::V0,
            _ => bail!("unknown SEE type version for length: {}", n),
        })
    }
}

make_xt_struct_family![
SeeType(parent: Nothing, version: SeeTypeVersion) {
    (V0, U8,    struct_type,             TypeTag, Dec, "structType"),
    (V0, Tbl,   si_names,              NameTable, Tbl, "si_names"),
    (V0, U16,   weight,         Mass<PoundsMass>, Dec, "weight"),
    (V0, U8,    externally_loadable,          u8, Hex, "flags"),
    (V0, U8,    seeker_type,                  u8, Dec, "sig"), // Seeker Type
    (V0, U8,    radar_type,                   u8, Hex, "flags"), // Radar Type

    (V0, U8,    look_down,                    u8, Dec, "lookDown"),
    (V0, U8,    doppler_speed_above,          u8, Dec, "dopplerSpeedAbove"),
    (V0, U8,    doppler_speed_below,          u8, Dec, "dopplerSpeedBelow"),
    (V0, U8,    doppler_min_range,            u8, Dec, "dopplerMinRange"), // Minimum Distance
    (V0, U8,    all_aspect,                   u8, Dec, "allAspect"),

    // RWS
    (V0, I16,   rws_fov_horizontal,          i16, Dec, "h"),
    (V0, I16,   rws_fov_pitch,               i16, Dec, "p"),
    (V0, U32,   rws_min_range,      Length<Feet>, Car, "minRange"),
    (V0, U32,   rws_max_range,      Length<Feet>, Car, "maxRange"),
    (V0, I32,   rws_min_altitude,            i32, Hex, "minAlt"),
    (V0, I32,   rws_max_altitude,            i32, Hex, "maxAlt"),
    // TWS
    (V0, I16,   tws_fov_horizontal,          i16, Dec, "h"),
    (V0, I16,   tws_fov_pitch,               i16, Dec, "p"),
    (V0, U32,   tws_min_range,      Length<Feet>, Car, "minRange"),
    (V0, U32,   tws_max_range,      Length<Feet>, Car, "maxRange"),
    (V0, I32,   tws_min_altitude,            i32, Hex, "minAlt"),
    (V0, I32,   tws_max_altitude,            i32, Hex, "maxAlt"),

    (V0, U8,    chaff_flare_chance,           u8, Dec, "chaffFlareChance"), // chaff/flare miss factor
    (V0, U8,    deception_chance,             u8, Dec, "deceptionChance") // ECM jamming factor
}];

impl SeeType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = extract_table_data(&lines)?;
        let see_lines = lines
            .iter()
            .skip(1)
            .take_while(|vs| **vs != ":si_names")
            .map(|&l| l.trim())
            .filter(|&l| !l.is_empty() && !l.starts_with(';'))
            .collect::<Vec<&str>>();
        Self::from_lines(Nothing, &see_lines, &pointers)
    }
}

impl SeeTypeIo {
    pub fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let raw = self.serialize_to_lines(tbl);
        let mut out = vec!["[brent's_relocatable_format]".to_owned()];
        out.extend_from_slice(&raw);
        out
    }
}

impl Display for SeeTypeIo {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        write!(f, "{}", rows.join("\r\n"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn can_parse_all_see() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("SEE").sort(Order::Asc).must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let see = SeeType::from_text(&contents)?;

            // println!("AT: {info:<30} : {} / {}", see.tws_fov_horizontal(), see.tws_fov_pitch());
            assert!(see.all_aspect == 0 || see.all_aspect == 50);

            assert_eq!(see.chaff_flare_chance, 100);
            assert_eq!(see.deception_chance, 100);

            // Note: mostly even degrees, except for SU35R
            assert!(
                see.tws_fov_horizontal % 182 == 0
                    || see.tws_fov_horizontal == i16::MAX
                    || see.tws_fov_horizontal as u16 == 57346
            );
            assert!(see.tws_fov_pitch % 182 == 0 || see.tws_fov_horizontal == i16::MAX);
            assert!(see.tws_min_altitude == i32::MIN || see.tws_min_altitude == 256);
            assert_eq!(see.tws_max_altitude, i32::MAX);
            assert!(see.rws_min_altitude == i32::MIN || see.rws_min_altitude == 256);
            assert_eq!(see.rws_max_altitude, i32::MAX);

            // Note: mostly even degrees, except for SU35R
            assert!(
                see.rws_fov_horizontal % 182 == 0
                    || see.rws_fov_horizontal == i16::MAX
                    || see.rws_fov_horizontal as u16 == 54616
            );
            assert!(see.rws_fov_pitch % 182 == 0 || see.rws_fov_horizontal == i16::MAX);
            assert_eq!(see.rws_min_range, feet!(0));
            assert_eq!(see.rws_max_range.f64() as u32 % 6076, 0);
            assert_eq!(see.tws_min_range, feet!(0));
            assert_eq!(see.tws_max_range.f64() as u32 % 6076, 0);
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_obj() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("SEE")
                .in_collection("FA")
                .must_match(),
        )? {
            // Not actually used anywhere and probably hand made as the format differs.
            if info.name() == "GCIR.SEE" || info.name() == "REDCR.SEE" {
                continue;
            }

            println!("At: {info}");
            let contents = from_dos_string(info.data()?);

            let see = SeeType::from_text(&contents)?;
            let see_io = SeeTypeIo::from(&see);
            let contents2 = see_io.to_string();

            for (a, b) in contents.lines().zip(contents2.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents.lines().count(), contents2.lines().count());
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_yaml() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("SEE")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            // Not actually used anywhere and probably hand made as the format differs.
            if info.name() == "GCIR.SEE" || info.name() == "REDCR.SEE" {
                continue;
            }

            println!("At: {info}");
            let raw0 = info.data()?;
            let contents0 = from_dos_string(raw0);
            let see0 = SeeType::from_text(&contents0)?;
            let v0: Value = see0.into();
            let yaml = v0.to_yaml()?;
            let v1 = Value::from_yaml(&yaml)?;
            let see1 = SeeType::try_from(&v1)?;
            let io1 = SeeTypeIo::from(&see1);
            let contents1 = io1.to_string();

            for (a, b) in contents0.lines().zip(contents1.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents0.lines().count(), contents1.lines().count());
        }
        Ok(())
    }
}
