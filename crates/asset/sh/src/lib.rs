// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod analysis;
mod coordinate;
mod draw_state;
mod extent;
mod instrs;
mod sh_code;
mod sh_x86;
mod synthetics;
mod usage;
mod xform;

pub use crate::{
    analysis::{ShAnalysis, ShCapabilities, ShXforms},
    coordinate::{
        fa_center_to_pt3, fa_face_norm_to_vec, fa_pos_to_pt3, fa_rot16_to_angle,
        fa_vert_norm_to_vec, matrix_for_xform, pt3_to_fa_center, pt3_to_fa_pos,
        vec_to_fa_face_norm, vec_to_fa_vert_norm,
    },
    draw_state::DrawState,
    extent::{GearFootprintKind, ShExtent},
    instrs::*,
    sh_code::{DrawSelection, InstrDetail, ShCode, ShInstr},
    usage::{
        AfterBurnerState, AileronPosition, AirbrakeState, BayState, EjectState, FaceUsage,
        FlapState, GearState, HookState, PlayerState, RudderPosition, SamCount, SlatsState,
        VertBufUsage,
    },
    xform::{XformId, Xformer},
};

pub const SHAPE_LOAD_BASE: u32 = 0xAA00_0000;
