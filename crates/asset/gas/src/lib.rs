// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use nitrous::Value;
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};
use xt_parse::{
    extract_table_data, make_brf_conversions, make_xt_struct_family, NameTable, Nothing, NothingIo,
    OutOfLineTable,
};

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
#[repr(u8)]
pub enum TypeTag {
    #[default]
    Gas = 8,
}

impl TryFrom<u8> for TypeTag {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            8 => Self::Gas,
            _ => bail!("unknown see TypeTag {}", value),
        })
    }
}

make_brf_conversions!(TypeTag, u8);

// We can detect the version by the number of lines.
// Note: unlike other users, this is inverted: USNF is the
//       one with more lines than expected.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum GasTypeVersion {
    V0 = 5,
}

impl GasTypeVersion {
    fn from_len(n: usize) -> Result<Self> {
        Ok(match n {
            5 => Self::V0,
            _ => bail!("unknown GAS type version for length: {}", n),
        })
    }
}

make_xt_struct_family![
GasType(parent: Nothing, version: GasTypeVersion) {
    (V0, U8,    struct_type,             TypeTag, Dec, "structType"),
    (V0, Tbl,   si_names,              NameTable, Tbl, "si_names"),
    (V0, U16,   weight,         Mass<PoundsMass>, Dec, "weight"),
    (V0, U8,    flags,                        u8, Hex, "flags"),
    (V0, U32,   fuel,                        u32, Dec, "fuel")
}];

impl GasType {
    pub fn from_text(data: &str) -> Result<Self> {
        let lines = data.lines().collect::<Vec<&str>>();
        ensure!(
            lines[0] == "[brent's_relocatable_format]",
            "not a type file"
        );
        let pointers = extract_table_data(&lines)?;
        let gas_lines = lines
            .iter()
            .skip(1)
            .take_while(|vs| **vs != ":si_names")
            .map(|&l| l.trim())
            .filter(|&l| !l.is_empty() && !l.starts_with(';'))
            .collect::<Vec<&str>>();
        Self::from_lines(Nothing, &gas_lines, &pointers)
    }
}

impl GasTypeIo {
    pub fn to_lines(&self, tbl: &mut OutOfLineTable) -> Vec<String> {
        let raw = self.serialize_to_lines(tbl);
        let mut out = vec!["[brent's_relocatable_format]".to_owned()];
        out.extend_from_slice(&raw);
        out
    }
}

impl Display for GasTypeIo {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut tbl = OutOfLineTable::default();
        let mut rows = self.to_lines(&mut tbl);
        rows.extend(tbl.take_strings());
        rows.push("\tend".to_owned());
        rows.push(String::new());
        write!(f, "{}", rows.join("\r\n"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Order, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn can_parse_all_gas() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(Search::for_extension("GAS").sort(Order::Asc).must_match())? {
            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let gas = GasType::from_text(&contents)?;

            assert!(gas.fuel() > &0);
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_obj() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("GAS")
                .in_collection("FA")
                .must_match(),
        )? {
            println!("At: {info}");
            let contents0 = from_dos_string(info.data()?);
            let gas0 = GasType::from_text(&contents0)?;
            let io0 = GasTypeIo::from(&gas0);

            let contents1 = io0.to_string();
            for (a, b) in contents0.lines().zip(contents1.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents0.lines().count(), contents1.lines().count());
        }
        Ok(())
    }

    #[test]
    fn can_roundtrip_yaml() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs.search(
            Search::for_extension("GAS")
                .in_collection("FA")
                .sort(Order::Asc)
                .must_match(),
        )? {
            println!("At: {info}");
            let raw0 = info.data()?;
            let contents0 = from_dos_string(raw0);
            let gas0 = GasType::from_text(&contents0)?;
            let v0: Value = gas0.into();
            let yaml = v0.to_yaml()?;
            let v1 = Value::from_yaml(&yaml)?;
            let gas1 = GasType::try_from(&v1)?;
            let io1 = GasTypeIo::from(&gas1);
            let contents1 = io1.to_string();

            for (a, b) in contents0.lines().zip(contents1.lines()) {
                assert_eq!(a, b);
            }
            assert_eq!(contents0.lines().count(), contents1.lines().count());
        }
        Ok(())
    }
}
