// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    analysis::{Airport, MmmAnalysis},
    map_name::MapName,
    mvalue::MValue,
    obj::ObjectInfo,
    reindex_objects, LayerName, MissionMap, ObjectProvider, ScreenKind,
};
use anyhow::{anyhow, bail, ensure, Result};
use bitbag::BitBag;
use catalog::FileSystem;
use installations::from_dos_string;
use log::debug;
use std::collections::HashMap;
use uuid::Uuid;
use xt::TypeManager;

/// Represents an M file.
#[derive(Debug)]
pub struct Mission {
    mm: MissionMap,
    map_name: MapName,
    layer_name: LayerName,
    layer_index: usize,
    screens: BitBag<ScreenKind>,
    view: (u32, u32, u32),
    wind: (i16, i16),
    time: (u8, u8),
    clouds: u32,
    us_air_skill: u8,
    us_ground_skill: u8,
    them_air_skill: u8,
    them_ground_skill: u8,
    free_flight: bool,
    guns_only: bool,
    allow_rearm_refuel: Option<bool>,
    print_mission_outcome: Option<bool>,
    mission_code_file: Option<String>,
    revive: Option<(u8, u8, u8)>,
    end_scenario: Option<(u32, u32, u32)>,
    sides: Vec<u8>,
    objects: Vec<ObjectInfo>,
    object_name_index: HashMap<String, usize>,
    object_uuid_index: HashMap<Uuid, usize>,
    analysis: MmmAnalysis,
}

impl Mission {
    pub fn from_str<FS: FileSystem>(
        s: &str,
        type_manager: &TypeManager,
        catalog: &FS,
    ) -> Result<Self> {
        let mut tokens = MValue::tokenize(s, type_manager, catalog)?;

        let mut mm = None;
        let mut map_name = None;
        let mut screens = BitBag::default();
        let mut allow_rearm_refuel = None;
        let mut print_mission_outcome = None;
        let mut mission_code_file = None;
        let mut wind = None;
        let mut revive = None;
        let mut end_scenario = None;
        let mut us_air_skill = None;
        let mut us_ground_skill = None;
        let mut them_air_skill = None;
        let mut them_ground_skill = None;
        let mut free_flight = false;
        let mut sides = None;
        let mut layer_name = None;
        let mut layer_index = None;
        let mut view = None;
        let mut time = None;
        let mut clouds = None;
        let mut objects = None;
        let mut guns_only = false;

        ensure!(
            matches!(tokens[0], MValue::TextFormat | MValue::AllowRearmRefuel(_)),
            "missing textFormat node in M"
        );
        for value in tokens.drain(..) {
            match value {
                MValue::TextFormat => {}
                MValue::Brief => screens |= ScreenKind::BRIEFING,
                MValue::BriefMap => screens |= ScreenKind::BRIEFING_MAP,
                MValue::SelectPlane => screens |= ScreenKind::SELECT_PLANE,
                MValue::ArmPlane => screens |= ScreenKind::ARM_PLANE,
                MValue::AllowRearmRefuel(v) => allow_rearm_refuel = Some(v),
                MValue::PrintMissionOutcome(v) => print_mission_outcome = Some(v),
                MValue::Code(v) => mission_code_file = Some(v),
                MValue::Wind(v) => wind = Some(v),
                MValue::Revive(v) => revive = Some(v),
                MValue::EndScenario(v) => end_scenario = Some(v),
                MValue::UsAirSkill(v) => us_air_skill = Some(v),
                MValue::UsGroundSkill(v) => us_ground_skill = Some(v),
                MValue::ThemAirSkill(v) => them_air_skill = Some(v),
                MValue::ThemGroundSkill(v) => them_ground_skill = Some(v),
                MValue::FreeFlight => free_flight = true,
                MValue::MapObjSuccessFlags(_) => {
                    // TODO: we probably need to handle this as part of the next(?) object?
                }
                MValue::Sides((v, _n)) => sides = Some(v),
                MValue::MapName(map) => {
                    debug!("using mission parent map: {}", map.parent());
                    let info = catalog.lookup(&map.parent())?;
                    let mm_raw = info.data()?;
                    let mm_content = from_dos_string(mm_raw);
                    mm = Some(MissionMap::from_str(
                        mm_content.as_ref(),
                        type_manager,
                        catalog,
                    )?);
                    map_name = Some(map);
                }
                MValue::Layer((name, index)) => {
                    layer_name = Some(name);
                    layer_index = Some(index);
                }
                MValue::View(v) => view = Some(v),
                MValue::Time(t) => time = Some(t),
                MValue::Clouds(v) => clouds = Some(v),
                MValue::HistoricalEra(historical_era) => ensure!(historical_era == 4),
                MValue::Objects(objs) => objects = Some(objs),
                MValue::TMaps(_) => bail!("TMaps not allowed in M files"),
                MValue::TDics(_) => bail!("TDics not allowed in M files"),
                MValue::Specials(_) => bail!("Special markers not allowed in M files"),
                MValue::GunsOnly => guns_only = true,
            }
        }

        // Perform analysis and indexing
        let mut objects = objects.ok_or_else(|| anyhow!("mission must have 'object's"))?;
        let (object_name_index, object_uuid_index) = reindex_objects(&objects);
        let analysis = MmmAnalysis::analyze(&objects, &object_uuid_index)?;

        // Note: we need the borrow on objects to end so we can mutate.
        MmmAnalysis::mark_airports(&object_uuid_index, &mut objects, analysis.airports());

        Ok(Mission {
            map_name: map_name.ok_or_else(|| anyhow!("Missions must have a map_name"))?,
            mm: mm.ok_or_else(|| anyhow!("Missions must have a parent MissionMap"))?,
            layer_name: layer_name.ok_or_else(|| anyhow!("Missions must have a layer name"))?,
            layer_index: layer_index.ok_or_else(|| anyhow!("Missions must have a layer index"))?,
            screens,
            view: view.ok_or_else(|| anyhow!("mission must have view"))?,
            wind: wind.ok_or_else(|| anyhow!("mission must have wind"))?,
            time: time.ok_or_else(|| anyhow!("mission must have time"))?,
            clouds: clouds.ok_or_else(|| anyhow!("mission must have clouds"))?,
            us_air_skill: us_air_skill.ok_or_else(|| anyhow!("mission must have usAirSkill"))?,
            us_ground_skill: us_ground_skill
                .ok_or_else(|| anyhow!("mission must have usGroundSkill"))?,
            them_air_skill: them_air_skill
                .ok_or_else(|| anyhow!("mission must have themAirSkill"))?,
            them_ground_skill: them_ground_skill
                .ok_or_else(|| anyhow!("mission must have themGroundSkill"))?,
            free_flight,
            guns_only,
            allow_rearm_refuel,
            print_mission_outcome,
            mission_code_file,
            revive,
            end_scenario,
            sides: sides.ok_or_else(|| anyhow!("missions must have sides defined"))?,
            objects,
            object_name_index,
            object_uuid_index,
            analysis,
        })
    }

    pub fn mission_map(&self) -> &MissionMap {
        &self.mm
    }

    pub fn map_name(&self) -> &MapName {
        &self.map_name
    }

    pub fn layer_name(&self) -> &LayerName {
        &self.layer_name
    }

    pub fn layer_index(&self) -> usize {
        self.layer_index
    }

    pub fn screens(&self) -> &BitBag<ScreenKind> {
        &self.screens
    }

    pub fn view(&self) -> (u32, u32, u32) {
        self.view
    }

    pub fn time(&self) -> (u8, u8) {
        self.time
    }

    pub fn wind(&self) -> (i16, i16) {
        self.wind
    }

    pub fn clouds(&self) -> u32 {
        self.clouds
    }

    pub fn us_air_skill(&self) -> u8 {
        self.us_air_skill
    }

    pub fn us_ground_skill(&self) -> u8 {
        self.us_ground_skill
    }

    pub fn them_air_skill(&self) -> u8 {
        self.them_air_skill
    }

    pub fn them_ground_skill(&self) -> u8 {
        self.them_ground_skill
    }

    pub fn free_flight(&self) -> bool {
        self.free_flight
    }

    pub fn guns_only(&self) -> bool {
        self.guns_only
    }

    pub fn allow_rearm_refuel(&self) -> bool {
        self.allow_rearm_refuel.unwrap_or(false)
    }

    pub fn print_mission_outcome(&self) -> bool {
        self.print_mission_outcome.unwrap_or(false)
    }

    pub fn mission_code_file(&self) -> Option<&str> {
        self.mission_code_file.as_deref()
    }

    pub fn revive(&self) -> Option<(u8, u8, u8)> {
        self.revive
    }

    pub fn end_scenario(&self) -> Option<(u32, u32, u32)> {
        self.end_scenario
    }

    pub fn sides(&self) -> &[u8] {
        &self.sides
    }

    pub fn mission_objects(&self) -> &[ObjectInfo] {
        &self.objects
    }

    pub fn objects_without_airports(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.objects.iter().filter(|info| !info.is_airport_part())
    }

    pub fn all_objects(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.objects.iter().chain(self.mm.objects.iter())
    }

    // pub fn all_objects_without_airports(&self) -> impl Iterator<Item = &ObjectInfo> {
    //     self.mm
    //         .objects_without_airports()
    //         .chain(self.objects_without_airports())
    // }

    pub fn airports(&self) -> &[Airport] {
        self.analysis.airports()
    }

    pub fn all_airports(&self) -> impl Iterator<Item = &Airport> {
        self.analysis.airports().iter().chain(self.mm.airports())
    }

    pub fn object_mut(&mut self, target: &Uuid) -> Result<&mut ObjectInfo> {
        if let Some(offset) = self.object_uuid_index.get(target) {
            Ok(&mut self.objects[*offset])
        } else if let Ok(info) = self.mm.object_mut(target) {
            Ok(info)
        } else {
            bail!("do not know object {target}")
        }
    }

    pub fn analysis(&self) -> &MmmAnalysis {
        &self.analysis
    }
}

impl ObjectProvider for Mission {
    fn object(&self, target: &Uuid) -> Result<&ObjectInfo> {
        if let Some(offset) = self.object_uuid_index.get(target) {
            Ok(&self.objects[*offset])
        } else if let Ok(info) = self.mm.object(target) {
            Ok(info)
        } else {
            bail!("do not know object {target}")
        }
    }

    fn object_by_name(&self, target: &str) -> Result<&ObjectInfo> {
        if let Some(offset) = self.object_name_index.get(target) {
            Ok(&self.objects[*offset])
        } else if let Ok(info) = self.mm.object_by_name(target) {
            Ok(info)
        } else {
            bail!("do not know object {target}")
        }
    }

    fn iter(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.all_objects()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::is_mission_template;
    use catalog::Search;
    use installations::Installations;

    #[test]
    fn it_can_parse_all_m_files() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        let type_manager = TypeManager::default();
        for info in libs.search(Search::for_extension("M").must_match())? {
            if is_mission_template(info.name()) {
                continue;
            }

            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let mission = Mission::from_str(contents.as_ref(), &type_manager, &libs)?;
            assert!(!mission.sides.is_empty());
            assert!(mission.map_name.meta_name().ends_with(".T2"));
        }

        Ok(())
    }
}
