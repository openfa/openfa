// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
#![allow(clippy::cognitive_complexity)]

mod analysis;
mod formation;
mod map_name;
mod mission;
mod mvalue;
mod obj;
mod special;
mod util;
mod waypoint;

pub use crate::{
    formation::{FormationControl, FormationKind, WingFormation},
    map_name::MapName,
    mission::Mission,
    obj::{AirportSegment, ObjectInfo},
    special::SpecialInfo,
    waypoint::Waypoints,
};

use crate::{
    analysis::{Airport, MmmAnalysis},
    mvalue::MValue,
};
use anyhow::{anyhow, bail, ensure, Result};
use bitbag::BitBaggable;
use catalog::FileSystem;
use log::debug;
use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
};
use strum::EnumIter;
use uuid::Uuid;
use xt::TypeManager;

/// This mission is used to show the "vehicle info" screen in the reference.
///
/// INPUTS:
///     <selected> - an XT file for alias -2
pub const VEHICLE_INFO_MISSION: &str = "~INFO.M";

/// ~/$MC\[_NATO\].M claim to be the base for created missions.
/// INPUTS: none
pub const NEW_MISSION_PREFIX: &str = "~MC";

/// Freeflight mission prefix. There are a bunch of missions with a freeflight tag
/// that generally take a <selected> input, but also a bunch that have a fixed input
/// XT that generally matches the name. The intent is probably the same for all of
/// these. There are no ~A missions that do not have the freeflight tag. These are
/// generally able to be parsed independently, unlike quick mission fragments.
///
/// INPUTS:
///     <selected> - but some are hardcoded
pub const FREEFLIGHT_PREFIX: &str = "~A";

/// Quick mission prefix. Quick missions are shipped as pile of fragments that gets
/// catted together based on what's selected in the GUI with a bunch of variables
/// provided. These are generally tokenable, but not individually recognizable as
/// anything mission-like.
///
/// INPUTS:
///     <aaa>
///     <afv>
///     <cargo>
///     <carrier>
///     <cruiser>
///     <destroyer>
///     <hovercraft>
///     <sam>
///     <small>
///     <tank>
pub const QUICK_MISSION_PREFIX: &str = "~Q";

/// There is also quick.M in the base directory, which I think is probably where
/// the built quick mission is saved before being run. We need to mask it too.
pub const QUICK_MISSION_BASE: &str = "QUICK.M";

/// Multiplayer missions? These have Red and Blue bases, but nothing else.
/// Clearly just a framework for something more.
///
/// INPUTS:
///     <aaa>
///     <jstars>
///     <sam>
///     <wateraaa>
///     <watersam>
pub const MULTIPLAYER_MISSION_PREFIX: &str = "~F";

pub fn is_mission_template(name: &str) -> bool {
    name == VEHICLE_INFO_MISSION
        || name == QUICK_MISSION_BASE
        || name.starts_with(NEW_MISSION_PREFIX)
        || name.starts_with(FREEFLIGHT_PREFIX)
        || name.starts_with(MULTIPLAYER_MISSION_PREFIX)
        || name.starts_with(QUICK_MISSION_PREFIX)
        || canonicalize(name) == VEHICLE_INFO_MISSION
        || canonicalize(name) == QUICK_MISSION_BASE
        || canonicalize(name).starts_with(NEW_MISSION_PREFIX)
        || canonicalize(name).starts_with(FREEFLIGHT_PREFIX)
        || canonicalize(name).starts_with(MULTIPLAYER_MISSION_PREFIX)
        || canonicalize(name).starts_with(QUICK_MISSION_PREFIX)
}

// It appears that '$' got changed to '~' in filenames (when moving to
// windows?), but only M/MM references were caught, not PICs. Thus, this
// local routine to normalize.
pub fn canonicalize(name: &str) -> String {
    name.to_ascii_uppercase().replace('$', "~")
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum TLoc {
    Index(usize),
    Name(String),
}

impl TLoc {
    pub fn pic_file(&self, base: &str) -> String {
        match self {
            TLoc::Index(ref i) => format!("{base}{i}.PIC"),
            TLoc::Name(ref s) => s.to_uppercase() + ".PIC",
        }
    }

    pub fn is_named(&self) -> bool {
        match self {
            Self::Index(_) => false,
            Self::Name(_) => true,
        }
    }
}

impl Display for TLoc {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Index(i) => write!(f, "{i}"),
            Self::Name(n) => write!(f, "{n}"),
        }?;
        Ok(())
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum MapOrientation {
    Unk0,
    Unk1,
    FlipS,
    RotateCcw,
}

impl MapOrientation {
    pub fn from_byte(n: u8) -> Result<Self> {
        Ok(match n {
            0 => MapOrientation::Unk0,
            1 => MapOrientation::Unk1,
            2 => MapOrientation::FlipS,
            3 => MapOrientation::RotateCcw,
            _ => bail!("invalid orientation"),
        })
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            MapOrientation::Unk0 => 0,
            MapOrientation::Unk1 => 1,
            MapOrientation::FlipS => 2,
            MapOrientation::RotateCcw => 3,
        }
    }
}

impl Display for MapOrientation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_byte())
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TMap {
    pub orientation: MapOrientation,
    pub loc: TLoc,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TDic {
    n: usize,
    map: [[u8; 4]; 8],
}

#[derive(Debug)]
pub struct LayerName {
    raw: String,
    filename: String,
}

impl LayerName {
    // This is yet a different lookup routine than for T2 or PICs. It is usually the `layer` value,
    // except when it is a modified version with the first (non-tilde) character of the MM name
    // appended to the end of the LAY name, before the dot.
    fn new<FS: FileSystem>(layer_token: char, raw_layer_name: &str, catalog: &FS) -> Result<Self> {
        debug!("find_layer token:{}, layer:{}", layer_token, raw_layer_name);

        // the map name is probably
        let canonical = raw_layer_name.to_uppercase();
        let (layer_prefix, layer_ext) = canonical
            .rsplit_once('.')
            .ok_or_else(|| anyhow!("layer must have extension"))?;

        let alt_layer_name = format!("{layer_prefix}{layer_token}.{layer_ext}");
        let filename = if catalog.lookup(&alt_layer_name).is_ok() {
            debug!("B: using lay: {}", alt_layer_name);
            alt_layer_name
        } else {
            canonical
        };

        debug!("A: using lay: {filename}");
        Ok(Self {
            raw: raw_layer_name.to_string(),
            filename,
        })
    }

    pub fn mm_name(&self) -> &str {
        &self.raw
    }

    pub fn filename(&self) -> &str {
        &self.filename
    }
}

#[allow(non_camel_case_types)]
#[repr(u8)]
#[derive(BitBaggable, Clone, Copy, Debug, Eq, PartialEq, EnumIter)]
pub enum ScreenKind {
    BRIEFING = 0x01,
    BRIEFING_MAP = 0x02,
    SELECT_PLANE = 0x04,
    ARM_PLANE = 0x08,
}

pub trait ObjectProvider {
    fn object(&self, target: &Uuid) -> Result<&ObjectInfo>;
    fn object_by_name(&self, target: &str) -> Result<&ObjectInfo>;
    fn iter(&self) -> impl Iterator<Item = &ObjectInfo>;
}

pub(crate) fn reindex_objects(
    objects: &[ObjectInfo],
) -> (HashMap<String, usize>, HashMap<Uuid, usize>) {
    let mut names = HashMap::new();
    let mut uuids = HashMap::new();
    for (i, info) in objects.iter().enumerate() {
        if let Some(name) = info.name() {
            names.insert(name.to_owned(), i);
        }
        uuids.insert(*info.id(), i);
    }
    (names, uuids)
}

#[derive(Debug)]
pub struct MissionMap {
    map_name: MapName,
    layer_name: LayerName,
    layer_index: usize,
    tmaps: HashMap<(i32, i32), TMap>,
    tmap_order: Vec<(i32, i32)>,
    tdics: Vec<TDic>,
    wind: Option<(i16, i16)>,
    view: (u32, u32, u32),
    time: (u8, u8),
    sides: Vec<u8>,
    sides_number: &'static str,
    historical_era: Option<usize>,
    objects: Vec<ObjectInfo>,
    object_name_index: HashMap<String, usize>,
    object_uuid_index: HashMap<Uuid, usize>,
    specials: Vec<SpecialInfo>,
    trailer: String,
    analysis: MmmAnalysis,
}

impl MissionMap {
    pub fn from_str<FS: FileSystem>(
        s: &str,
        type_manager: &TypeManager,
        catalog: &FS,
    ) -> Result<Self> {
        let mut tokens = MValue::tokenize(s, type_manager, catalog)?;

        let mut map_name = None;
        let mut layer_name = None;
        let mut layer_index = None;
        let mut wind = None;
        let mut view = None;
        let mut time = None;
        let mut sides = None;
        let mut sides_number = None;
        let mut historical_era = None;
        let mut specials = None;
        let mut tmaps = None;
        let mut tmap_order = None;
        let mut tdics = None;
        let mut objects = None;

        ensure!(
            matches!(tokens[0], MValue::TextFormat),
            "missing textFormat node in MM"
        );
        for value in tokens.drain(..) {
            match value {
                MValue::TextFormat => {}
                MValue::Brief => bail!("Brief in MM"),
                MValue::BriefMap => bail!("BriefMap in MM"),
                MValue::SelectPlane => bail!("SelectPlane in MM"),
                MValue::ArmPlane => bail!("ArmPlane in MM"),
                MValue::AllowRearmRefuel(_) => bail!("AllowRearmRefuel in MM"),
                MValue::PrintMissionOutcome(_) => bail!("PrintMissionOutcome in MM"),
                MValue::Code(_) => bail!("Code in MM"),
                MValue::Wind(v) => wind = Some(v),
                MValue::Revive(_) => bail!("Revive in MM"),
                MValue::EndScenario(_) => bail!("EndScenario in MM"),
                MValue::UsAirSkill(_) => bail!("UsAirSkill in MM"),
                MValue::UsGroundSkill(_) => bail!("UsGroundSkill in MM"),
                MValue::ThemAirSkill(_) => bail!("ThemAirSkill in MM"),
                MValue::ThemGroundSkill(_) => bail!("ThemGroundSkill in MM"),
                MValue::FreeFlight => bail!("FreeFlight in MM"),
                MValue::MapObjSuccessFlags(_) => bail!("MapObjSuccessFlags in MM"),
                MValue::GunsOnly => bail!("GunsOnly in MM"),
                MValue::Sides((v, n)) => {
                    sides = Some(v);
                    sides_number = Some(n);
                }
                MValue::MapName(map) => {
                    // ensure!(map_name.parent(name.chars().next().unwrap()) == name);
                    map_name = Some(map);
                }
                MValue::Layer((name, index)) => {
                    layer_name = Some(name);
                    layer_index = Some(index);
                }
                MValue::View(v) => view = Some(v),
                MValue::Time(t) => time = Some(t),
                MValue::Clouds(clouds) => ensure!(clouds == 0),
                MValue::HistoricalEra(era) => {
                    ensure!(era == 4);
                    historical_era = Some(4usize);
                }
                MValue::TMaps((tm, tmo)) => {
                    tmaps = Some(tm);
                    tmap_order = Some(tmo);
                }
                MValue::TDics(td) => tdics = Some(td),
                MValue::Objects(objs) => objects = Some(objs),
                MValue::Specials(sps) => specials = Some(sps),
            }
        }

        let (_, trailer) = s
            .rsplit_once(|c| !(c == ' ' || c == '\r' || c == '\n' || c == '\0' || c == '\u{1A}'))
            .unwrap_or(("", ""));
        assert!(trailer.starts_with("\r\n"));
        let trailer = trailer
            .chars()
            .skip(2)
            .collect::<String>()
            .replace('\r', "");

        // Perform indexing and analysis
        let mut objects = objects.ok_or_else(|| anyhow!("mm must have 'object' keys"))?;
        let (object_name_index, object_uuid_index) = reindex_objects(&objects);
        let analysis = MmmAnalysis::analyze(&objects, &object_uuid_index)?;

        // Note: we need the borrow on objects to end so we can mutate.
        MmmAnalysis::mark_airports(&object_uuid_index, &mut objects, analysis.airports());

        Ok(MissionMap {
            map_name: map_name.ok_or_else(|| anyhow!("mm must have a 'map' key"))?,
            layer_name: layer_name.ok_or_else(|| anyhow!("mm must have a 'layer' key"))?,
            layer_index: layer_index.ok_or_else(|| anyhow!("mm must have a 'layer' key"))?,
            wind,
            view: view.ok_or_else(|| anyhow!("mm must have a 'view' key"))?,
            time: time.ok_or_else(|| anyhow!("mm must have a 'time' key"))?,
            sides: sides.ok_or_else(|| anyhow!("mm must have 'sides' key"))?,
            sides_number: sides_number.ok_or_else(|| anyhow!("mm must have 'sides' key"))?,
            historical_era,
            tmaps: tmaps.ok_or_else(|| anyhow!("mm must have 'tmaps' keys"))?,
            tmap_order: tmap_order.ok_or_else(|| anyhow!("mm must have 'tmaps' keys"))?,
            tdics: tdics.ok_or_else(|| anyhow!("mm must have 'tdics' keys"))?,
            objects,
            object_name_index,
            object_uuid_index,
            specials: specials.ok_or_else(|| anyhow!("mm must have 'special' keys"))?,
            trailer,
            analysis,
        })
    }

    pub fn map_name(&self) -> &MapName {
        &self.map_name
    }

    pub fn layer_name(&self) -> &LayerName {
        &self.layer_name
    }

    pub fn layer_index(&self) -> usize {
        self.layer_index
    }

    pub fn texture_dictionary(&self) -> &[TDic] {
        &self.tdics
    }

    pub fn texture_maps(&self) -> std::collections::hash_map::Values<'_, (i32, i32), TMap> {
        self.tmaps.values()
    }

    pub fn texture_map(&self, xi: i32, zi: i32) -> Option<&TMap> {
        self.tmaps.get(&(xi, zi))
    }

    pub fn object_mut(&mut self, target: &Uuid) -> Result<&mut ObjectInfo> {
        let offset = self
            .object_uuid_index
            .get(target)
            .ok_or_else(|| anyhow!("do not know object {target}"))?;
        Ok(&mut self.objects[*offset])
    }

    pub fn objects(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.objects.iter()
    }

    pub fn objects_without_airports(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.objects.iter().filter(|info| !info.is_airport_part())
    }

    pub fn airports(&self) -> &[Airport] {
        self.analysis.airports()
    }

    pub fn analysis(&self) -> &MmmAnalysis {
        &self.analysis
    }
}

impl ObjectProvider for MissionMap {
    fn object(&self, target: &Uuid) -> Result<&ObjectInfo> {
        let offset = self
            .object_uuid_index
            .get(target)
            .ok_or_else(|| anyhow!("do not know object {target}"))?;
        Ok(&self.objects[*offset])
    }

    fn object_by_name(&self, target: &str) -> Result<&ObjectInfo> {
        let offset = self
            .object_name_index
            .get(target)
            .ok_or_else(|| anyhow!("do not know object {target}"))?;
        Ok(&self.objects[*offset])
    }

    fn iter(&self) -> impl Iterator<Item = &ObjectInfo> {
        self.objects()
    }
}

impl Display for MissionMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "textFormat")?;
        writeln!(f, "map {}", self.map_name.raw_name())?;
        writeln!(
            f,
            "layer {} {}",
            self.layer_name.mm_name(),
            self.layer_index
        )?;
        writeln!(f, "clouds 0")?;
        if let Some((n, w)) = &self.wind {
            writeln!(f, "wind {} {}", *n, *w)?;
        }
        writeln!(f, "view {} {} {}", self.view.0, self.view.1, self.view.2)?;
        writeln!(f, "sides{}", self.sides_number)?;
        for side in &self.sides {
            if self.sides_number.is_empty() {
                writeln!(f, "\t{side}")?;
            } else {
                writeln!(f, "\t${side:X}")?;
            }
        }
        writeln!(f, "time {} {}", self.time.0, self.time.1)?;
        if let Some(era) = self.historical_era {
            writeln!(f, "historicalera {era}")?;
        }
        for obj in &self.objects {
            writeln!(f, "{obj}")?;
        }
        for special in &self.specials {
            writeln!(f, "{special}")?;
        }
        for (x, y) in &self.tmap_order {
            let tmap = &self.tmaps[&(*x, *y)];
            match &tmap.loc {
                TLoc::Index(n) => {
                    writeln!(f, "tmap {x} {y} {n} {}", tmap.orientation)?;
                }
                TLoc::Name(s) => {
                    writeln!(f, "tmap_named {s} {x} {y}")?;
                }
            }
        }
        for tdic in &self.tdics {
            writeln!(f, "tdic {}", tdic.n)?;
            for row in &tdic.map {
                writeln!(f, "\t{} {} {} {}", row[0], row[1], row[2], row[3])?;
            }
        }
        write!(f, "{}", self.trailer)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::{from_dos_string, Installations};

    #[test]
    fn it_can_parse_all_mm_files() -> Result<()> {
        let (libs, installs) = Installations::for_testing()?;
        let type_manager = TypeManager::default();
        for info in libs.search(Search::for_extension("MM").must_match())? {
            // For some reason, the ATF Gold disks contain USNF missions, but
            // do not contain the USNF assets. Not sure how that works.
            if installs.game_info(info.collection_name())?.test_dir == "ATFGOLD"
                && (info.name().contains("UKR")
                    || info.name() == "KURILE.MM"
                    || info.name() == "VIET.MM")
            {
                println!("skipping broken asset: {info}");
                continue;
            }

            // This looks like a fragment of an MM used for... something?
            if info.name() == "$VARF.MM" {
                continue;
            }

            println!("At: {info}");
            let contents = from_dos_string(info.data()?);
            let mm = MissionMap::from_str(contents.as_ref(), &type_manager, &libs)?;
            assert_eq!(mm.map_name().base_texture_name().len(), 3);
            assert!(mm.map_name().t2_name().ends_with(".T2"));
        }

        Ok(())
    }

    #[test]
    fn it_can_roundtrip_all_mm_files() -> Result<()> {
        let (libs, installs) = Installations::for_testing()?;
        let type_manager = TypeManager::default();
        for info in libs.search(Search::for_extension("MM").must_match())? {
            // For some reason, the ATF Gold disks contain USNF missions, but
            // do not contain the USNF assets. Not sure how that works.
            if installs.game_info(info.collection_name())?.test_dir == "ATFGOLD"
                && (info.name().contains("UKR")
                    || info.name() == "KURILE.MM"
                    || info.name() == "VIET.MM")
            {
                println!("skipping broken asset: {info}");
                continue;
            }

            // This looks like a fragment of an MM used for... something?
            if info.name() == "$VARF.MM" {
                continue;
            }

            // Ukr was hand coded and is chock-full of comments, interleaved special
            // and a bunch of other hand-crafted weirdness that I don't want to handle.
            if info.name() == "UKR.MM"
                || info.name().starts_with("~UKR")
                || info.name().starts_with("$UKR")
            {
                continue;
            }

            println!("At: {info}");
            let data0 = info.data()?;
            let contents0 = from_dos_string(data0.clone());
            let mm = MissionMap::from_str(contents0.as_ref(), &type_manager, &libs)?;
            let contents1 = mm.to_string();
            let contents1 = contents1.replace('\n', "\r\n");

            let lines0 = contents0.lines().collect::<Vec<_>>();
            let lines1 = contents1.lines().collect::<Vec<_>>();
            let mut comment_bytes = 0;
            for (&a, &b) in lines0.iter().zip(&lines1) {
                // println!("{info}: {a:?} vs {b:?}");
                assert_eq!(a.trim_end(), b.trim_end());
                comment_bytes += a.len() - b.len(); // handle trailing whitespace
            }
            // println!("lines: {} == {}", lines0.len(), lines1.len());
            assert_eq!(lines0.len(), lines1.len());
            // println!(
            //     "len: {} == {}",
            //     contents0.len(),
            //     contents1.len() + comment_bytes
            // );
            assert_eq!(contents0.len(), contents1.len() + comment_bytes);
        }
        Ok(())
    }
}
