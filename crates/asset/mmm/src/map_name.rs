use anyhow::{anyhow, ensure};

// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Debug, Eq, PartialEq)]
pub struct MapName {
    raw: String,
    canonical: String,
    prefix: Option<char>,
    name: String,
    number: Option<u32>,
    ext: String,
}

impl MapName {
    // These are all of the terrains and map references in the base games.
    // FA:
    //     FA_2.LIB:
    //         EGY.T2, FRA.T2, VLA.T2, BAL.T2, UKR.T2, KURILE.T2, TVIET.T2
    //         APA.T2, CUB.T2, GRE.T2, IRA.T2, LFA.T2, NSK.T2, PGU.T2, SPA.T2, WTA.T2
    //     MM refs:
    //         // Campaign missions?
    //         $bal[0-7].T2
    //         $egy[1-9].T2
    //         $fra[0-9].T2
    //         $vla[1-8].T2
    //         ~ukr[1-8].T2
    //         // Freeform missions and ???; map editor layouts maybe?
    //         ~apaf.T2, apa.T2
    //         ~balf.T2, bal.T2
    //         ~cubf.T2, cub.T2
    //         ~egyf.T2, egy.T2
    //         ~fraf.T2, fra.T2
    //         ~gref.T2, gre.T2
    //         ~iraf.T2, ira.T2
    //         ~kurile.T2, kurile.T2
    //         ~lfaf.T2, lfa.T2
    //         ~nskf.T2, nsk.T2
    //         ~pguf.T2, pgu.T2
    //         ~spaf.T2, spa.T2
    //         ~tviet.T2, tviet.T2
    //         ~ukrf.T2, ukr.T2
    //         ~vlaf.T2, vla.T2
    //         ~wtaf.T2, wta.T2
    //    M refs:
    //         $bal[0-7].T2
    //         $egy[1-8].T2
    //         $fra[0-3,6-9].T2
    //         $vla[1-8].T2
    //         ~bal[0,2,3,6,7].T2
    //         ~egy[1,2,4,7].T2
    //         ~fra[3,9].T2
    //         ~ukr[1-8].T2
    //         ~vla[1,2,5].T2
    //         bal.T2, cub.T2, egy.T2, fra.T2, kurile.T2, tviet.T2, ukr.T2, vla.T2
    // USNF97:
    //     USNF_2.LIB: UKR.T2, ~UKR[1-8].T2, KURILE.T2, VIET.T2
    //     MM refs: ukr.T2, ~ukr[1-8].T2, kurile.T2, viet.T2
    //     M  refs: ukr.T2, ~ukr[1-8].T2, kurile.T2, viet.T2
    // ATFGOLD:
    //     ATF_2.LIB: EGY.T2, FRA.T2, VLA.T2, BAL.T2
    //     MM refs: egy.T2, fra.T2, vla.T2, bal.T2
    //              $egy[1-9].T2, $fra[0-9].T2, $vla[1-8].T2, $bal[0-7].T2
    //     INVALID: kurile.T2, ~ukr[1-8].T2, ukr.T2, viet.T2
    //     M  refs: $egy[1-8].T2, $fra[0-3,6-9].T2, $vla[1-8].T2, $bal[0-7].T2,
    //              ~bal[2,6].T2, bal.T2, ~egy4.T2, egy.T2, fra.T2, vla.T2
    //     INVALID: ukr.T2
    // ATFNATO:
    //     installdir: EGY.T2, FRA.T2, VLA.T2, BAL.T2
    //     MM refs: egy.T2, fra.T2, vla.T2, bal.T2,
    //              $egy[1-9].T2, $fra[0-9].T2, $vla[1-8].T2, $bal[0-7].T2
    //     M  refs: egy.T2, fra.T2, vla.T2, bal.T2,
    //              $egy[1-8].T2, $fra[0-3,6-9].T2, $vla[1-8].T2, $bal[0-7].T2
    // ATF:
    //     installdir: EGY.T2, FRA.T2, VLA.T2
    //     MM refs: egy.T2, fra.T2, vla.T2,
    //              $egy[1-8].T2, $fra[0-9].T2, $vla[1-8].T2
    //     M  refs: $egy[1-8].T2, $fra[0-3,6-9].T2, $vla[1-8].T2, egy.T2
    // MF:
    //     installdir: UKR.T2, $UKR[1-8].T2, KURILE.T2
    //     MM+M refs: ukr.T2, $ukr[1-8].T2, kurile.T2
    // USNF:
    //     installdir: UKR.T2, $UKR[1-8].T2
    //     MM+M refs: ukr.T2, $ukr[1-8].T2
    //
    // There are only unadorned T2, but many of the T2 references in M/MM are adorned with a sigil
    // and a number. It turns out when these appear in M files, those map directly to MM instead of
    // T2 (with some squinting at the sigil). So I think how this goes is: loading an M, findes the
    // MM (instead of T2) referenced in the map_name, then loading the MM strips any sigil and
    // number to find the T2, since the MM are the ones with tmap and tdict entries needed to
    // actually understand what's in the T2 file.
    pub(crate) fn parse(map_name: &str) -> anyhow::Result<Self> {
        let canonical = map_name.to_uppercase();

        let (name, ext) = canonical
            .rsplit_once('.')
            .ok_or_else(|| anyhow!("invalid map_name; must be a t2 metafile"))?;

        let mut name = name.to_owned();
        ensure!(!name.is_empty());
        let maybe_prefix = name.chars().next().unwrap();
        let maybe_number = name.chars().last().unwrap();

        let mut prefix = None;
        if ['~', '$'].contains(&maybe_prefix) {
            prefix = Some(maybe_prefix);
            name = name[1..].to_string();
        }

        let mut number = None;
        if let Some(digit) = maybe_number.to_digit(10) {
            number = Some(digit);
            name = name[..name.len() - 1].to_string();
        } else if maybe_number == 'F' {
            // Note that ~KURILE and ~TVIET also exist; the E in kurile would be parsed as hex,
            // so we can't just interpret as hex here. I don't know the significance of F.
            number = Some(15);
            name = name[..name.len() - 1].to_string();
        }

        Ok(Self {
            raw: map_name.to_owned(),
            ext: ext.to_owned(),
            canonical,
            prefix,
            name,
            number,
        })
    }

    pub fn meta_name(&self) -> &str {
        &self.canonical
    }

    pub fn base_name(&self) -> &str {
        &self.name
    }

    pub fn base_texture_name(&self) -> &str {
        &self.name[..3]
    }

    pub fn t2_name(&self) -> String {
        format!("{}.T2", self.name)
    }

    pub fn raw_name(&self) -> &str {
        &self.raw
    }

    /// The first character of the name section of the metaname. The LAY data may have a
    /// map-specific version of a layer that should be preferred over the base LAY. This
    /// verison has this token appended to the non-extention part of the name.
    pub fn layer_token(&self) -> char {
        self.name.chars().next().expect("non-empty map name")
    }

    pub fn parent(&self) -> String {
        format!("{}.MM", self.name)
    }
}
