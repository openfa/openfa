// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::util::maybe_hex;
use crate::{
    LayerName, MapName, MapOrientation, ObjectInfo, SpecialInfo, TDic, TLoc, TMap, Waypoints,
};
use anyhow::{anyhow, bail, ensure};
use catalog::FileSystem;
use std::{collections::HashMap, str::FromStr};
use xt::TypeManager;

#[derive(Debug)]
pub(crate) enum MValue {
    TextFormat,
    Brief,
    BriefMap,
    SelectPlane,
    ArmPlane,
    AllowRearmRefuel(bool),
    PrintMissionOutcome(bool),
    Code(String),
    MapName(MapName),
    Layer((LayerName, usize)),
    Clouds(u32),
    Wind((i16, i16)),
    View((u32, u32, u32)),
    Time((u8, u8)),
    Revive((u8, u8, u8)),
    EndScenario((u32, u32, u32)),
    UsAirSkill(u8),
    UsGroundSkill(u8),
    ThemAirSkill(u8),
    ThemGroundSkill(u8),
    FreeFlight,
    #[allow(unused)]
    MapObjSuccessFlags((i32, u8)),
    Sides((Vec<u8>, &'static str)),
    HistoricalEra(u8),
    TMaps((HashMap<(i32, i32), TMap>, Vec<(i32, i32)>)),
    TDics(Vec<TDic>),
    Objects(Vec<ObjectInfo>),
    Specials(Vec<SpecialInfo>),
    GunsOnly,
}

impl MValue {
    pub(crate) fn tokenize<FS: FileSystem>(
        s: &str,
        type_manager: &TypeManager,
        catalog: &FS,
    ) -> anyhow::Result<Vec<MValue>> {
        let mut mm = Vec::new();

        // Do a fast pre-pass to get array pre-sizing for allocations and check if we need a
        // lexical pass to remove comments.
        let mut obj_cnt = 0;
        let mut special_cnt = 0;
        let mut tmap_cnt = 0;
        let mut tdic_cnt = 0;
        let mut need_lexical_pass = false;
        let init_tokens = s.split_ascii_whitespace();
        let prepass_tokens = init_tokens.clone();
        for token in prepass_tokens {
            match token {
                "obj" => obj_cnt += 1,
                "special" => special_cnt += 1,
                "tmap" => tmap_cnt += 1,
                "tmap_named" => tmap_cnt += 1,
                "tdic" => tdic_cnt += 1,
                v => {
                    if v.starts_with(';') {
                        need_lexical_pass = true;
                    }
                }
            }
        }
        let owned;
        let mut tokens = if need_lexical_pass {
            owned = s
                .lines()
                .filter(|l| !l.starts_with(';'))
                .collect::<Vec<_>>()
                .join("\n");
            owned.split_ascii_whitespace()
        } else {
            init_tokens
        };

        let mut layer_token = None;
        let mut sides: Vec<u8> = Vec::with_capacity(64);
        let mut sides_number = None;
        let mut objects_by_alias = HashMap::with_capacity(obj_cnt);
        let mut objects = Vec::with_capacity(obj_cnt);
        let mut specials: Vec<SpecialInfo> = Vec::with_capacity(special_cnt);
        let mut tmaps = HashMap::with_capacity(tmap_cnt);
        let mut tmap_order = Vec::with_capacity(tmap_cnt);
        let mut tdics = Vec::with_capacity(tdic_cnt);

        while let Some(token) = tokens.next() {
            assert!(!token.starts_with(';'));
            match token {
                "allowrearmrefuel" => {
                    let v = str::parse::<u8>(tokens.next().expect("allow rearm value"))?;
                    ensure!(v == 0);
                    mm.push(MValue::AllowRearmRefuel(false));
                }
                "textFormat" => mm.push(MValue::TextFormat),
                "brief" => mm.push(MValue::Brief),
                "briefmap" => mm.push(MValue::BriefMap),
                "selectplane" => mm.push(MValue::SelectPlane),
                "armplane" => mm.push(MValue::ArmPlane),
                "printmissionoutcome" | "printMissionOutcome" => {
                    let v = str::parse::<u8>(tokens.next().expect("allow rearm value"))?;
                    ensure!(v == 0);
                    mm.push(MValue::PrintMissionOutcome(false));
                }
                "map" => {
                    let raw_map_name = tokens.next().ok_or_else(|| anyhow!("map name expected"))?;
                    let map_name = MapName::parse(raw_map_name)?;
                    layer_token = Some(map_name.layer_token().to_owned());
                    mm.push(MValue::MapName(map_name));
                }
                "layer" => {
                    let raw_layer_name = tokens.next().expect("layer name");
                    let layer_index = tokens.next().expect("layer index").parse::<usize>()?;
                    let layer_name = LayerName::new(
                        layer_token.expect("map name must come before layer"),
                        raw_layer_name,
                        catalog,
                    )?;
                    mm.push(MValue::Layer((layer_name, layer_index)));
                }
                "clouds" => {
                    mm.push(MValue::Clouds(
                        tokens.next().expect("clouds").parse::<u32>()?,
                    ));
                }
                "wind" => {
                    let x = str::parse::<i16>(tokens.next().expect("wind x"))?;
                    let z = str::parse::<i16>(tokens.next().expect("wind z"))?;
                    mm.push(MValue::Wind((x, z)));
                }
                "view" => {
                    let x = str::parse::<u32>(tokens.next().expect("view x"))?;
                    let y = str::parse::<u32>(tokens.next().expect("view y"))?;
                    let z = str::parse::<u32>(tokens.next().expect("view z"))?;
                    mm.push(MValue::View((x, y, z)));
                }
                "code" => {
                    let mc_name =
                        format!("{}.MC", tokens.next().expect("code name")).to_uppercase();
                    ensure!(
                        catalog.lookup(&mc_name).is_ok(),
                        "mission references non-existing code file {}",
                        mc_name
                    );
                    mm.push(MValue::Code(mc_name));
                }
                "time" => {
                    let h = str::parse::<u8>(tokens.next().expect("time h"))?;
                    let m = str::parse::<u8>(tokens.next().expect("time m"))?;
                    mm.push(MValue::Time((h, m)));
                }
                "revive" => {
                    let a = str::parse::<u8>(tokens.next().expect("revive lives"))?;
                    let b = str::parse::<u8>(tokens.next().expect("revive wait"))?;
                    let c = str::parse::<u8>(tokens.next().expect("revive unk"))?;
                    ensure!(a <= 4);
                    ensure!(b == 0 || b == 15);
                    ensure!(c == 10);
                    mm.push(MValue::Revive((a, b, c)));
                }
                "endscenario" => {
                    let a = str::parse::<u32>(tokens.next().expect("endscenario timeout"))?;
                    let b = str::parse::<u32>(tokens.next().expect("endscenario unk 1"))?;
                    let c = str::parse::<u32>(tokens.next().expect("endscenario unk 2"))?;
                    ensure!(a == 600 || a == 900 || a == 1200 || a == 1500);
                    ensure!(b == 0x7FFF_FFFF);
                    ensure!(c == 0);
                    mm.push(MValue::EndScenario((a, b, c)));
                }
                "usGroundSkill" => {
                    let skill = str::parse::<u8>(tokens.next().expect("skill"))?;
                    mm.push(MValue::UsGroundSkill(skill));
                }
                "usAirSkill" => {
                    let skill = str::parse::<u8>(tokens.next().expect("skill"))?;
                    mm.push(MValue::UsAirSkill(skill));
                }
                "themGroundSkill" => {
                    let skill = str::parse::<u8>(tokens.next().expect("skill"))?;
                    mm.push(MValue::ThemGroundSkill(skill));
                }
                "themAirSkill" => {
                    let skill = str::parse::<u8>(tokens.next().expect("skill"))?;
                    mm.push(MValue::ThemAirSkill(skill));
                }
                "freeflight" | "freeFlight" => mm.push(MValue::FreeFlight),
                "sides" => {
                    // Only used by Ukraine.
                    assert!(sides.is_empty());
                    for _ in 0..18 {
                        let side = str::parse::<u8>(tokens.next().expect("side"))?;
                        ensure!(side == 0 || side == 128, "mm: unknown side flag");
                        sides.push(side);
                    }
                    sides_number = Some("");
                }
                "sides2" => {
                    // Post USNF: one more nationality, now in hex format, 0 or $80
                    assert!(sides.is_empty());
                    for _ in 0..19 {
                        let side = u8::from_str_radix(&tokens.next().expect("side")[1..], 16)?;
                        ensure!(side == 0 || side == 128, "mm: unknown side flag");
                        sides.push(side);
                    }
                    sides_number = Some("2 ");
                }
                "sides3" => {
                    // Protocol bump for 24 nationalities.
                    assert!(sides.is_empty());
                    for _ in 0..24 {
                        let side = u8::from_str_radix(&tokens.next().expect("side")[1..], 16)?;
                        ensure!(side == 0 || side == 128, "mm: unknown side flag");
                        sides.push(side);
                    }
                    sides_number = Some("3 ");
                }
                "sides4" => {
                    // Protocol bump for 64 nationalities.
                    assert!(sides.is_empty());
                    for _ in 0..64 {
                        let side = u8::from_str_radix(&tokens.next().expect("side")[1..], 16)?;
                        ensure!(side == 0 || side == 128, "mm: unknown side flag");
                        sides.push(side);
                    }
                    sides_number = Some("4 ");
                }
                "historicalera" => {
                    let historical_era = u8::from_str(tokens.next().expect("historical era"))?;
                    mm.push(MValue::HistoricalEra(historical_era));
                }
                "map_obj_success_flags" => {
                    // Only used in a handful of vietnam missions: T02, T08, T10.
                    // Seems like it might be positional, since the first arg doesn't map to an
                    // alias or anything else obvious, even in the MM.
                    let a = str::parse(tokens.next().expect("map_obj_success_flags a"))?;
                    let b = maybe_hex(tokens.next().expect("map_obj_success_flags b"))?;
                    ensure!(a < 0);
                    ensure!(b == 0x80);
                    mm.push(MValue::MapObjSuccessFlags((a, b)));
                }
                "obj" => {
                    let obj = ObjectInfo::from_tokens(&mut tokens, type_manager, catalog)?;
                    let obj_offset = objects.len();
                    objects.push(obj);
                    if let Some(alias) = objects[obj_offset].alias() {
                        ensure!(
                            !objects_by_alias.contains_key(&alias),
                            "duplicate alias detected"
                        );
                        objects_by_alias.insert(alias, obj_offset);
                    }
                }
                "special" => {
                    let special = SpecialInfo::from_tokens(&mut tokens)?;
                    specials.push(special);
                }
                "tmap" => {
                    let x = tokens.next().expect("tmap x").parse::<i16>()? as i32;
                    let y = tokens.next().expect("tmap y").parse::<i16>()? as i32;
                    ensure!(x % 4 == 0, "unaligned tmap x index");
                    ensure!(y % 4 == 0, "unaligned tmap y index");
                    let index = tokens.next().expect("index").parse::<usize>()?;
                    let orientation = tokens.next().expect("orientation").parse::<u8>()?;
                    tmaps.insert(
                        (x, y),
                        TMap {
                            orientation: MapOrientation::from_byte(orientation)?,
                            loc: TLoc::Index(index),
                        },
                    );
                    tmap_order.push((x, y));
                }
                "tmap_named" => {
                    // TODO: maybe push to_uppercase lower?
                    let tmp = tokens.next().expect("name");
                    // let name = (String::with_capacity(tmp.len() + 4) + tmp).to_uppercase() + ".PIC";
                    let name = tmp.to_owned();
                    let x = tokens.next().expect("tmap_named x").parse::<i16>()? as i32;
                    let y = tokens.next().expect("tmap_named y").parse::<i16>()? as i32;
                    ensure!(x % 4 == 0, "unaligned tmap_named x index");
                    ensure!(y % 4 == 0, "unaligned tmap_named y index");
                    tmaps.insert(
                        (x, y),
                        TMap {
                            orientation: MapOrientation::from_byte(0)?,
                            loc: TLoc::Name(name),
                        },
                    );
                    tmap_order.push((x, y));
                }
                "tdic" => {
                    let n = tokens.next().expect("tdic n").parse::<usize>()?;
                    let mut map = [[0u8; 4]; 8];
                    for row in &mut map {
                        for item in row {
                            let t = tokens.next().expect("map");
                            *item = (t == "1") as u8;
                        }
                    }
                    let tdic = TDic { n, map };
                    tdics.push(tdic);
                }
                "waypoint2" => {
                    let cnt = tokens.next().expect("waypoint cnt").parse::<usize>()?;
                    let wp = Waypoints::from_tokens(cnt, &mut tokens)?;
                    let obj_offset = *objects_by_alias
                        .get(&wp.for_alias())
                        .ok_or_else(|| anyhow!("waypoints for unknown object"))?;
                    objects[obj_offset].set_waypoints(wp);
                }
                "\0" | "\x1A" => {
                    // DOS EOF char, but not always at eof.
                }
                "gunsOnly" => {
                    // Used only in training mission 9: EXTRA09.M in USNF+MF and UKR09 in USNF97.
                    // TODO: does FA even support this mission properly?
                    mm.push(MValue::GunsOnly);
                }
                v => {
                    println!("mm parse error near token: {:?} {:?}", v, tokens.next());
                    bail!("unknown mission map key: {}", v);
                }
            }
        }

        for tmap in tmaps.iter() {
            if let TLoc::Index(i) = tmap.1.loc {
                ensure!(i < tdics.len(), "expected a tdict for each tmap index");
            }
        }

        if !sides.is_empty() {
            mm.push(MValue::Sides((sides, sides_number.unwrap())));
        }

        if !specials.is_empty() {
            mm.push(MValue::Specials(specials));
        }
        if !objects.is_empty() {
            mm.push(MValue::Objects(objects));
        }
        if !tmaps.is_empty() {
            mm.push(MValue::TMaps((tmaps, tmap_order)));
        }
        if !tdics.is_empty() {
            mm.push(MValue::TDics(tdics));
        }

        Ok(mm)
    }
}
