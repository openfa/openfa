// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

use crate::{obj::AirportSegment, ObjectInfo};
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use std::collections::{HashMap, HashSet};
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct Airport {
    name: String,
    part1: Uuid,
    part2: Option<Uuid>,
    objects: HashSet<Uuid>,
}

impl Airport {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn part1(&self) -> &Uuid {
        &self.part1
    }

    pub fn part2(&self) -> Option<&Uuid> {
        self.part2.as_ref()
    }
}

#[derive(Debug, Default)]
pub struct MmmAnalysis {
    airports: Vec<Airport>,
}

impl MmmAnalysis {
    pub(crate) fn analyze(objects: &[ObjectInfo], index: &HashMap<Uuid, usize>) -> Result<Self> {
        let mut airports = Vec::new();

        // Find paired rnwy halves
        for obj1 in objects {
            // Not interested in .PT, .NT, etc.
            if !obj1.xt().names().file_name().ends_with(".OT") {
                continue;
            }

            // Airports start with "Strip"
            if obj1.xt().names().file_name().starts_with("STRIP") {
                // Secondary half; already marked by discovering the primary
                if obj1.xt().names().file_name().ends_with("A.OT") {
                    continue;
                }

                // Get the pair name for the OT
                let pair_name = obj1
                    .xt()
                    .ot()?
                    .ot_names()
                    .file_name()
                    .replacen(".OT", "A.OT", 1);

                // Search through objs to see if there is a paired OT and get its id
                let part2 = objects
                    .iter()
                    .find(|obj2| {
                        obj2.xt().names().file_name() == pair_name
                            && obj1.position().to(*obj2.position()).magnitude() < feet!(miles!(8))
                    })
                    .map(|obj2| *obj2.id());

                // Create the airport from one or more parts
                airports.push(Airport {
                    name: obj1
                        .name()
                        .map(|s| s.to_owned())
                        .unwrap_or_else(|| format!("Airport {}", obj1.id())),
                    part1: *obj1.id(),
                    part2,
                    objects: HashSet::new(),
                });
            }
        }

        // Note: Object scale and general extents are stored in the SH, so we
        // just treat anything on the ground within 5 miles of the airport
        // as probably part of the general infrastructure.
        for airport in &mut airports {
            let part1 = &objects[index[&airport.part1]];
            let part2 = airport.part2.as_ref().map(|id| &objects[index[id]]);
            for obj in objects {
                if obj.id() == part1.id() {
                    // Skip the primary airport
                    continue;
                }
                if let Some(part2) = part2 {
                    if obj.id() == part2.id() {
                        // Skip secondary airport half
                        continue;
                    }
                }
                if obj.position().y() > feet!(0) {
                    // Skip non-ground object
                    continue;
                }
                let d1 = part1.position().to(*obj.position()).magnitude();
                if d1 < feet!(5 * 5280) {
                    airport.objects.insert(*obj.id());
                    continue;
                }
                if let Some(part2) = part2 {
                    let d2 = part2.position().to(*obj.position()).magnitude();
                    if d2 < feet!(5 * 5280) {
                        airport.objects.insert(*obj.id());
                        continue;
                    }
                }
            }
        }

        Ok(Self { airports })
    }

    pub fn mark_airports(
        index: &HashMap<Uuid, usize>,
        objects: &mut [ObjectInfo],
        airports: &[Airport],
    ) {
        for airport in airports {
            objects[*index.get(airport.part1()).expect("an object")]
                .set_is_airport(AirportSegment::Primary);
            if let Some(part2) = airport.part2() {
                objects[*index.get(part2).expect("an object")]
                    .set_is_airport(AirportSegment::Secondary(airport.part1));
            }
            for obj_id in &airport.objects {
                objects[*index.get(obj_id).expect("an object")]
                    .set_is_airport(AirportSegment::Object(airport.part1));
            }
        }
    }

    pub fn airports(&self) -> &[Airport] {
        &self.airports
    }

    pub fn airport(&self, name: &str) -> Result<&Airport> {
        for airport in &self.airports {
            if airport.name() == name {
                return Ok(airport);
            }
        }
        bail!("no such airport: {name}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::MissionMap;
    use anyhow::bail;
    use catalog::FileSystem;
    use installations::{from_dos_string, Installations};
    use xt::TypeManager;

    #[test]
    fn it_can_parse_all_mm_files() -> Result<()> {
        let (libs, _installs) = Installations::for_testing()?;
        let type_manager = TypeManager::default();
        let info = libs.collection("FA")?.lookup("UKR.MM")?;
        let data = info.data()?;
        let text = from_dos_string(data);
        let mm = MissionMap::from_str(&text, &type_manager, &libs)?;
        for airports in mm.analysis().airports() {
            if airports.name() == "Kherson" {
                return Ok(());
            }
        }
        bail!("did not find Kherson in UKR.MM");
    }
}
