// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{anyhow, bail, Result};
use bevy_ecs::prelude::*;
use catalog::{AssetCatalog, FileInfo, FileSystem};
use ecm::EcmType;
use gas::GasType;
use installations::from_dos_string;
use jt::ProjectileType;
use log::trace;
use nitrous::{
    inject_nitrous_component, inject_nitrous_resource, method, HeapMut, NitrousComponent,
    NitrousResource, Value,
};
use nt::NpcType;
use ot::ObjectType;
use parking_lot::Mutex;
use pt::PlaneType;
use runtime::{Extension, Runtime};
use see::SeeType;
use std::{collections::HashMap, sync::Arc};
use xt_parse::NameTable;

// The heap storage for one of the core FA types. Note that the
// enum type tags here are duplicative with the struct_type field
// in the first byte of each of the included structs, but making
// this intrusive seems like far too big a headache.
#[derive(Debug)]
pub enum Type {
    Gas(Box<GasType>),
    Ecm(Box<EcmType>),
    See(Box<SeeType>),
    JT(Box<ProjectileType>),
    NT(Box<NpcType>),
    OT(Box<ObjectType>),
    PT(Box<PlaneType>),
}

impl Type {
    pub fn maybe_gas(&self) -> Option<&GasType> {
        match self {
            Type::Gas(ref gas) => Some(gas),
            _ => None,
        }
    }

    pub fn maybe_ecm(&self) -> Option<&EcmType> {
        match self {
            Type::Ecm(ref ecm) => Some(ecm),
            _ => None,
        }
    }

    pub fn maybe_see(&self) -> Option<&SeeType> {
        match self {
            Type::See(ref see) => Some(see),
            _ => None,
        }
    }

    // Return the OT portion of OT, JT, NT, or PT
    pub fn maybe_ot(&self) -> Option<&ObjectType> {
        Some(match self {
            Type::OT(ref ot) => ot,
            Type::JT(ref jt) => jt.ot(),
            Type::NT(ref nt) => nt.ot(),
            Type::PT(ref pt) => pt.nt().ot(),
            _ => return None,
        })
    }

    // Return an OT exactly and None for JT, NT, or PT.
    pub fn maybe_ot_exact(&self) -> Option<&ObjectType> {
        Some(match self {
            Type::OT(ref ot) => ot,
            _ => return None,
        })
    }

    pub fn maybe_jt(&self) -> Option<&ProjectileType> {
        match self {
            Type::JT(ref jt) => Some(jt),
            _ => None,
        }
    }

    // Return the NT portion fo NT or PT
    pub fn maybe_nt(&self) -> Option<&NpcType> {
        match self {
            Type::NT(ref nt) => Some(nt),
            Type::PT(ref pt) => Some(pt.nt()),
            _ => None,
        }
    }

    // Return an NT exactly and None for PT.
    pub fn maybe_nt_exact(&self) -> Option<&NpcType> {
        Some(match self {
            Type::NT(ref nt) => nt,
            _ => return None,
        })
    }

    pub fn maybe_pt(&self) -> Option<&PlaneType> {
        match self {
            Type::PT(ref pt) => Some(pt),
            _ => None,
        }
    }
}

// Any single type is likely used by multiple game objects at once so we cache
// type loads aggressively and hand out a Ref to an immutable, shared global
// copy of the Type.
#[derive(NitrousComponent, Clone, Debug)]
pub struct TypeRef(Arc<Type>);

#[inject_nitrous_component]
impl TypeRef {
    fn new(item: Type) -> Self {
        TypeRef(Arc::new(item))
    }

    pub fn maybe_ot(&self) -> Option<&ObjectType> {
        self.0.maybe_ot()
    }

    pub fn maybe_ot_exact(&self) -> Option<&ObjectType> {
        self.0.maybe_ot_exact()
    }

    pub fn maybe_jt(&self) -> Option<&ProjectileType> {
        self.0.maybe_jt()
    }

    pub fn maybe_nt(&self) -> Option<&NpcType> {
        self.0.maybe_nt()
    }

    pub fn maybe_nt_exact(&self) -> Option<&NpcType> {
        self.0.maybe_nt_exact()
    }

    pub fn maybe_pt(&self) -> Option<&PlaneType> {
        self.0.maybe_pt()
    }

    pub fn gas(&self) -> Result<&GasType> {
        self.0.maybe_gas().ok_or_else(|| anyhow!("not a GAS type"))
    }

    pub fn ecm(&self) -> Result<&EcmType> {
        self.0.maybe_ecm().ok_or_else(|| anyhow!("not an ECM type"))
    }

    pub fn see(&self) -> Result<&SeeType> {
        self.0.maybe_see().ok_or_else(|| anyhow!("not a SEE type"))
    }

    pub fn ot(&self) -> Result<&ObjectType> {
        self.0
            .maybe_ot()
            .ok_or_else(|| anyhow!("not an object type"))
    }

    pub fn pt(&self) -> Result<&PlaneType> {
        self.0.maybe_pt().ok_or_else(|| anyhow!("not a plane type"))
    }

    // True for any of ot, nt, pt, or jt.
    pub fn is_ot(&self) -> bool {
        self.maybe_ot().is_some()
    }

    // True only for literal OT files
    pub fn is_ot_exact(&self) -> bool {
        self.maybe_ot_exact().is_some()
    }

    pub fn is_pt(&self) -> bool {
        self.maybe_pt().is_some()
    }

    // True for nt or pt.
    pub fn is_nt(&self) -> bool {
        self.maybe_nt().is_some()
    }

    // True only for literal NT files
    pub fn is_nt_exact(&self) -> bool {
        self.maybe_nt_exact().is_some()
    }

    pub fn is_jt(&self) -> bool {
        self.maybe_jt().is_some()
    }

    pub fn names(&self) -> &NameTable {
        match self.0.as_ref() {
            Type::Ecm(ecm) => ecm.si_names(),
            Type::Gas(gas) => gas.si_names(),
            Type::See(see) => see.si_names(),
            Type::OT(ot) => ot.ot_names(),
            Type::NT(nt) => nt.ot().ot_names(),
            Type::JT(jt) => jt.ot().ot_names(),
            Type::PT(pt) => pt.nt().ot().ot_names(),
        }
    }
}

// Knows how to load a type from a game library. Keeps a cached copy and hands
// out a pointer to the type, since we frequently need to load the same item
// repeatedly.
#[derive(Debug, Default, NitrousResource)]
#[resource(name = "typeman")]
pub struct TypeManager {
    // Cache immutable resources. Use interior mutability for ease of use.
    cache: Mutex<HashMap<String, TypeRef>>,
}

impl Extension for TypeManager {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(TypeManager::default())?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl TypeManager {
    pub fn unload(&self, info: FileInfo<'_>) -> Result<TypeRef> {
        let cache_key = info.to_string();
        self.cache
            .lock()
            .remove(&cache_key)
            .ok_or_else(|| anyhow!("the file {info} was not cached!"))
    }

    #[method]
    pub fn lookup(&self, name: &str, heap: HeapMut) -> Result<Value> {
        let info = heap.resource::<AssetCatalog>().lookup(name)?;
        let xt = self.load(info)?;
        Ok(xt.names().into())
    }

    pub fn load(&self, info: FileInfo<'_>) -> Result<TypeRef> {
        let cache_key = info.to_string();
        if let Some(item) = self.cache.lock().get(&cache_key) {
            trace!("TypeManager::load({}) -- cached", info);
            return Ok(item.clone());
        };

        trace!("TypeManager::load({})", cache_key);
        let content = from_dos_string(info.data()?);

        // let ext = info.name()[info.name().len() - 3..].to_ascii_uppercase();
        let ext = info
            .name()
            .rsplit('.')
            .next()
            .ok_or_else(|| anyhow!("no extension found on {info}"))?;
        let item = match ext {
            "GAS" => {
                let gas = GasType::from_text(&content)?;
                Type::Gas(Box::new(gas))
            }
            "ECM" => {
                let ecm = EcmType::from_text(&content)?;
                Type::Ecm(Box::new(ecm))
            }
            "SEE" => {
                let see = SeeType::from_text(&content)?;
                Type::See(Box::new(see))
            }
            "OT" => {
                let ot = ObjectType::from_text(&content)?;
                Type::OT(Box::new(ot))
            }
            "JT" => {
                let jt = ProjectileType::from_text(&content)?;
                Type::JT(Box::new(jt))
            }
            "NT" => {
                let nt = NpcType::from_text(&content)?;
                Type::NT(Box::new(nt))
            }
            "PT" => {
                let pt = PlaneType::from_text(&content)?;
                Type::PT(Box::new(pt))
            }
            _ => bail!("resource: unknown type {info}"),
        };
        let xt = TypeRef::new(item);
        self.cache.lock().insert(cache_key, xt.clone());
        Ok(xt)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use catalog::{FileSystem, Search};
    use installations::Installations;

    #[test]
    fn can_parse_all_entity_types() -> Result<()> {
        let (libs, _) = Installations::for_testing()?;
        for info in libs
            .search(Search::for_glob("*.[OJNP]T").must_match())?
            .iter()
            .chain(libs.search(Search::for_glob("*.ECM").must_match())?.iter())
            .chain(libs.search(Search::for_glob("*.GAS").must_match())?.iter())
            .chain(libs.search(Search::for_glob("*.SEE").must_match())?.iter())
        {
            println!("At: {info}");
            let types = TypeManager::default();
            let ty = types.load(*info)?;
            // Only one misspelling in 2500+ files.
            if !ty.names().file_name().is_empty() {
                assert!(ty.names().file_name() == info.name() || info.name() == "SMALLARM.JT");
            }
        }

        Ok(())
    }
}
