// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Search {
    Present(&'static str),
    Absent(&'static str),
}

// FIXME: should this all be in a common crate?
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct GameInfo {
    pub release_year: usize,
    pub release_month: usize,
    pub release_day: usize,
    pub name: &'static str,
    pub long_name: &'static str,
    pub developer: &'static str,
    pub publisher: &'static str,
    pub test_dir: &'static str,
    pub allow_packed_t2: bool,
    pub unique_files: &'static [Search],
    pub cd_libs: &'static [&'static str],
    pub optional_cd_libs: &'static [&'static str],
    pub all_libs: &'static [&'static str],
    pub lib_prefix: &'static str,
}

impl GameInfo {
    pub fn released_at(&self) -> String {
        assert_ne!(self.release_year, 0);
        if self.release_month > 0 {
            format!("{}/{}", self.release_month, self.release_year)
        } else {
            format!("?/{}", self.release_year)
        }
    }
}

const USNF: GameInfo = GameInfo {
    name: "USNF",
    long_name: "U.S. Navy Fighters",
    developer: "Electronic Arts Inc.",
    publisher: "Electronic Arts Inc.",
    release_year: 1994,
    release_month: 11,
    release_day: 1,
    test_dir: "USNF",
    allow_packed_t2: false,
    unique_files: &[
        Search::Present("USNF.EXE"),  // USNF || MF || USNF97
        Search::Present("1.LIB"),     // !USNF97: added usnf_ prefix to libs
        Search::Absent("USNF_1.LIB"), // !USNF97: same as above but opposite direction
        Search::Absent("8.LIB"),      // !MF: added 8.lib over USNF
        Search::Absent("42.2D"),      // !MF: added 42.2D over USNF
    ],
    cd_libs: &["1.LIB", "2.LIB", "3.LIB", "5.LIB", "6.LIB", "7.LIB"],
    optional_cd_libs: &[],
    all_libs: &[
        "1.LIB", "2.LIB", "3.LIB", "4.LIB", "5.LIB", "6.LIB", "7.LIB",
    ],
    lib_prefix: "",
};

const USMF: GameInfo = GameInfo {
    name: "Marine Fighters",
    long_name: "U.S. Navy Fighters Expansion Disk: Marine Fighters",
    developer: "Electronic Arts Inc.",
    publisher: "Electronic Arts Inc.",
    release_year: 1995,
    release_month: 0,
    release_day: 0,
    test_dir: "MF",
    allow_packed_t2: false,
    unique_files: &[
        Search::Present("USNF.EXE"),  // USNF || MF || USNF97
        Search::Present("8.LIB"),     // !USNF && !USNF97: added 8.lib and usnf_ prefix to libs
        Search::Absent("USNF_1.LIB"), // !USNF97: same as above but opposite direction
        Search::Present("42.2D"),     // !USNF: adding 42.2D in MF
        Search::Present("KURILE.T2"), // !USNF: did not have KURILE yet
    ],
    cd_libs: &["8.LIB"],
    optional_cd_libs: &[],
    all_libs: &[
        "1.LIB", "2.LIB", "3.LIB", "4.LIB", "5.LIB", "6.LIB", "8.LIB",
    ],
    lib_prefix: "",
};

const ATF: GameInfo = GameInfo {
    name: "ATF",
    long_name: "Jane's ATF: Advanced Tactical Fighters",
    developer: "Jane's Combat Simulations",
    publisher: "Electronic Arts Inc.",
    test_dir: "ATF",
    release_year: 1996,
    release_month: 3,
    release_day: 31,
    allow_packed_t2: false,
    unique_files: &[
        Search::Present("AF.EXE"), // ATF || NATO
        Search::Absent("10.LIB"),  // !NATO: added 10.lib
    ],
    cd_libs: &["4C.LIB", "9.LIB"],
    optional_cd_libs: &[],
    all_libs: &["1.LIB", "2.LIB", "4B.LIB", "4C.LIB", "9.LIB"],
    lib_prefix: "",
};

const ATF_NATO: GameInfo = GameInfo {
    name: "ATF Nato",
    long_name: "Jane's ATF: Nato Fighters",
    developer: "Jane's Combat Simulations",
    publisher: "Electronic Arts Inc.",
    release_year: 1996,
    release_month: 9,
    release_day: 30,
    test_dir: "ATFNATO",
    allow_packed_t2: false,
    unique_files: &[
        Search::Present("AF.EXE"), // ATF || NATO
        Search::Present("10.LIB"), // Added 10.lib
    ],
    cd_libs: &["4C.LIB", "10.LIB"],
    optional_cd_libs: &[],
    all_libs: &["1.LIB", "2.LIB", "4B.LIB", "4C.LIB", "10.LIB"],
    lib_prefix: "",
};

const USNF97: GameInfo = GameInfo {
    name: "USNF '97",
    long_name: "Jane's US Navy Fighters '97",
    developer: "Jane's Combat Simulations",
    publisher: "Electronic Arts Inc.",
    release_year: 1996,
    release_month: 11,
    release_day: 0,
    test_dir: "USNF97",
    allow_packed_t2: true,
    unique_files: &[
        Search::Present("USNF.EXE"),   // USNF || MF || USNF97
        Search::Present("USNF_1.LIB"), // Added usnf_ prefix to libs
        Search::Absent("40.2D"),       // Removed vesa/vga stub programs
    ],
    cd_libs: &["USNF_3.LIB", "USNF_7.LIB", "USNF_8.LIB", "USNF_10.LIB"],
    optional_cd_libs: &[],
    all_libs: &[
        "USNF_1.LIB",
        "USNF_2.LIB",
        "USNF_3.LIB",
        "USNF_7.LIB",
        "USNF_8.LIB",
        "USNF_10.LIB",
    ],
    lib_prefix: "USNF_",
};

const ATF_GOLD: GameInfo = GameInfo {
    name: "ATF Gold",
    long_name: "Jane's ATF: Gold Edition",
    developer: "Jane's Combat Simulations",
    publisher: "Electronic Arts Inc.",
    release_year: 1997,
    release_month: 0,
    release_day: 0,
    test_dir: "ATFGOLD",
    allow_packed_t2: true,
    unique_files: &[Search::Present("ATF.EXE")],
    cd_libs: &["ATF_3.LIB", "ATF_4C.LIB", "ATF_10.LIB"],
    optional_cd_libs: &[],
    all_libs: &[
        "ATF_1.LIB",
        "ATF_2.LIB",
        "ATF_3.LIB",
        "ATF_4B.LIB",
        "ATF_4C.LIB",
        "ATF_10.LIB",
    ],
    lib_prefix: "ATF_",
};

const FIGHTERS_ANTHOLOGY: GameInfo = GameInfo {
    name: "Fighters Anthology",
    long_name: "Jane's Fighters Anthology",
    developer: "Jane's Combat Simulations",
    publisher: "Electronic Arts Inc.",
    release_year: 1998,
    release_month: 0,
    release_day: 0,
    test_dir: "FA",
    allow_packed_t2: true,
    unique_files: &[Search::Present("FA.EXE")],
    cd_libs: &[
        // CD1
        "FA_4C.LIB",
        "FA_7.LIB",
    ],
    optional_cd_libs: &[
        // CD2
        "FA_3.LIB",
        "FA_10.LIB",
        "FA_10B.LIB",
        "FA_11.LIB",
        "FA_11B.LIB",
    ],
    all_libs: &[
        "FA_1.LIB",
        "FA_2.LIB",
        "FA_3.LIB",
        "FA_4B.LIB",
        "FA_4C.LIB",
        "FA_4D.LIB",
        "FA_7.LIB",
        "FA_10.LIB",
        "FA_10B.LIB",
        "FA_11.LIB",
        "FA_11B.LIB",
    ],
    lib_prefix: "FA_",
};

pub const GAME_INFO: [&GameInfo; 7] = [
    &FIGHTERS_ANTHOLOGY,
    &ATF_GOLD,
    &USNF97,
    &ATF_NATO,
    &ATF,
    &USMF,
    &USNF,
];

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn can_sort_properly() {
        let mut games = GAME_INFO.to_vec();
        games.sort();
        games.reverse();
        assert_eq!(games.first().unwrap().test_dir, "FA");
        for game in games {
            println!("game: {}", game.name);
        }
    }
}
