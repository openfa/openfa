// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    Shape, ShapeFlagBuffer, ShapeId, ShapeIds, ShapeName, ShapeScale, ShapeTransformBuffer,
    ShapeXformBuffer,
};
use absolute_unit::prelude::*;
use anyhow::{Context, Result};
use bevy_ecs::prelude::*;
use catalog::{AssetCatalog, FileSystem};
use geometry::Aabb3;
use installations::Installations;
use mantle::Gpu;
use nitrous::{HeapMut, NamedEntityMut};
use sh::{DrawState, GearFootprintKind, ShCapabilities, ShXforms};

// Collect all the data we need to drive the shape once loaded and return a
// package of components that can be injected into an entity.
// Note: ShCode is stored on Shape to dedup across ShapeId and instances
// Note: ShAnalysis is also in Shape to dedup across instances
pub struct ShapeLoadPackage {
    // The actual shape
    id: ShapeId,

    // Related shapes, for when we need to spawn damage models
    ids: ShapeIds,

    // Shape name for display purpose
    name: ShapeName,

    // Dynamical scale for upload
    scale: ShapeScale,

    // Capabiltiies for determining what to upload
    //
    // Note: in theory this should be de-duped to Shape, but it's only a u32 and lets us avoid
    //       needing the Shape in the upload path.
    caps: ShCapabilities,

    // Local upload buffers to hold instance upload data so that we don't serialize
    // on the global instance upload buffers. This lets us parallelize x86 VM eval,
    // one of the most time-consuming aspects of computing shape layout.
    transform_buf: ShapeTransformBuffer,

    // Shapes without optional, switchable parts do not need a flags bundle.
    // Avoiding creating the FlagBuffer and Draw state makes the system not
    // run for this instance.
    flags_bundle: Option<(DrawState, ShapeFlagBuffer)>,

    // Shapes without animated parts do not need an xforms bundle.
    // Avoiding creating the XformBuffer cuts out the extremely expensive
    // Xform analysis for this shape and saves some per-frame upload overhead.
    xforms_bundle: Option<(ShXforms, ShapeXformBuffer)>,
}

impl ShapeLoadPackage {
    pub fn with_context(
        map_name: &str,
        shape_file_name: &str,
        external_scale: Scalar,
        gear_layout: Option<GearFootprintKind>,
        mut heap: HeapMut,
    ) -> Result<ShapeLoadPackage> {
        // Note: Keep this ability for now for experimentation.
        let runtime_scale = scalar!(1);

        // Load the shape.
        let shape_ids = heap
            .resource_scope(|heap, mut shapes: Mut<Shape>| {
                shapes.upload_shape(
                    heap.resource::<AssetCatalog>().lookup(shape_file_name)?,
                    (external_scale, gear_layout),
                    heap.resource::<AssetCatalog>(),
                    heap.resource::<Installations>().primary_palette(),
                    heap.resource::<Gpu>(),
                )
            })
            .with_context(|| format!("loading map {map_name}"))?;

        // Clone caps of shape_id locally to avoid serializing on instance flags upload later.
        let caps = heap
            .resource::<Shape>()
            .analysis(shape_ids.normal())?
            .capabilities();

        // Minimize our runtime work by only creating the components we really need.
        let mut flags_bundle = None;
        let mut xforms_bundle = None;
        if caps.needs_draw_state() {
            let draw_state = DrawState::empty(caps);
            flags_bundle = Some((draw_state, ShapeFlagBuffer::default()));
            if let Some(shape_xforms) = heap
                .resource::<Shape>()
                .analysis(shape_ids.normal())?
                .xforms_for_shape_bundle()
            {
                xforms_bundle = Some((shape_xforms, ShapeXformBuffer::default()));
            }
        }

        Ok(ShapeLoadPackage {
            id: shape_ids.normal(),
            ids: shape_ids,
            name: ShapeName::new(shape_file_name),
            scale: ShapeScale::new(runtime_scale.f64()),
            caps,
            transform_buf: ShapeTransformBuffer::default(),
            flags_bundle,
            xforms_bundle,
        })
    }

    /// Inject the set of non-none components into the given entity.
    pub fn inject_into(self, ent: &mut NamedEntityMut) -> Result<()> {
        ent.inject((
            self.id,
            self.ids,
            self.name,
            self.scale,
            self.caps,
            self.transform_buf,
        ))?;
        if let Some(flag_bundle) = self.flags_bundle {
            ent.inject(flag_bundle)?;
        }
        if let Some(xform_bundle) = self.xforms_bundle {
            ent.inject(xform_bundle)?;
        }
        Ok(())
    }

    pub fn shape_id(&self) -> &ShapeId {
        &self.id
    }

    pub fn bounding_box(&self, shape: &Shape) -> Result<Aabb3<Meters>> {
        Ok(shape
            .analysis(self.id)?
            .extent()
            .aabb_full()
            .as_rescaled(self.scale.scale() as f64))
    }

    pub fn ground_offset(&self, shape: &Shape) -> Result<Length<Meters>> {
        Ok(shape.analysis(self.id)?.extent().offset_to_ground() * self.scale.scalar())
    }
}
