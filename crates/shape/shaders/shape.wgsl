// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.

// ____ include <nitrogen/shader_shared/include/quaternion.wgsl>

//!include <nitrogen/shader_shared/include/consts.wgsl>
//!include <nitrogen/atmosphere/include/global.wgsl>
//!include <nitrogen/atmosphere/include/library.wgsl>
//!include <nitrogen/camera/include/camera.wgsl>
//!include <nitrogen/orrery/include/orrery.wgsl>
//!include <nitrogen/world/include/world.wgsl>
//!include <shape/include/library.wgsl>

//!bindings [0] <nitrogen/camera/bindings/camera.wgsl>
//!bindings [1] <nitrogen/atmosphere/bindings/atmosphere.wgsl>
//!bindings [1] <nitrogen/orrery/bindings/orrery.wgsl>

fn quat_to_rotation_rh(q: vec4<f32>) -> mat4x4<f32> {
    let x = q.x;
    let y = q.y;
    let z = q.z;
    let w = q.w;

    let r0c0 = 1. - 2.*y*y - 2.*z*z;
    let r0c1 = 2.*x*y + 2.*w*z;
    let r0c2 = 2.*x*z - 2.*w*y;
    let r1c0 = 2.*x*y - 2.*w*z;
    let r1c1 = 1. - 2.*x*x - 2.*z*z;
    let r1c2 = 2.*y*z + 2.*w*x;
    let r2c0 = 2.*x*z + 2.*w*y;
    let r2c1 = 2.*y*z - 2.*w*x;
    let r2c2 = 1. - 2.*x*x - 2.*y*y;

    return mat4x4(
        vec4(r0c0, r0c1, r0c2, 0.),
        vec4(r1c0, r1c1, r1c2, 0.),
        vec4(r2c0, r2c1, r2c2, 0.),
        vec4(0., 0., 0., 1.),
    );
}

var<private> TRANSFORM_SIZE: u32 = 8u;
var<private> XFORM_SIZE: u32 = 6u;
var<private> MAX_XFORM_ID: u32 = 32u;

struct VertexOut {
    @builtin(position) position: vec4<f32>,
    @location(0) v_position_w_m: vec4<f32>,
    @location(1) v_color: vec4<f32>,
    @location(2) v_normal_w: vec3<f32>,
    @location(3) v_tex_coord: vec2<f32>,
    @location(4) f_flags0: u32,
    @location(5) f_flags1: u32,
}

//// Chunk group
struct TextureAtlasProperties {
    width: u32,
    height: u32,
    pad0: u32,
    pad1: u32,
}
@group(2) @binding(0) var chunk_mega_atlas_texture: texture_2d<f32>;
@group(2) @binding(1) var chunk_mega_atlas_sampler: sampler;
@group(2) @binding(2) var<uniform> chunk_atlas_size: TextureAtlasProperties;

//// Per shape input
struct InstanceInfo {
    transform: array<f32, 8u>,
    flags0: u32,
    flags1: u32,
    xforms: array<array<f32, 6u>, 14u>,
}
@group(3) @binding(0) var<storage> instance_info: array<InstanceInfo>;

@vertex
fn vert_main(
    @location(0) model_position: vec3<f32>,
    @location(1) model_normal: vec3<f32>,
    @location(2) color: vec4<f32>,
    @location(3) tex_coord: vec2<u32>,
    @location(4) flags0: u32,
    @location(5) flags1: u32,
    @location(6) xform_id: u32,
    @builtin(instance_index) instance_idx: u32,
) -> VertexOut {
    // One transform per instance, so indexing is easy.
    // No slicing primitive, so do it manually.
    let info = instance_info[instance_idx];
    let model_transform = vec3(
        info.transform[0u],
        info.transform[1u],
        info.transform[2u],
    );
    let model_facing = vec4(
        info.transform[3u],
        info.transform[4u],
        info.transform[5u],
        info.transform[6u],
    );
    let model_to_eye_rotation = quat_to_rotation_rh(model_facing);
    let model_scale = info.transform[7u];

    // Xforms are packed, so look up the base offset first.
    var xform = array<f32,8>(0., 0., 0., 0., 0., 0., 0., 1.);
    if (xform_id < MAX_XFORM_ID) {
        var xform_raw = array<f32, 6u>(0., 0., 0., 0., 0., 0.);
        if (xform_id == 0u) { xform_raw = info.xforms[0u]; }
        else if (xform_id == 1u) { xform_raw = info.xforms[1u]; }
        else if (xform_id == 2u) { xform_raw = info.xforms[2u]; }
        else if (xform_id == 3u) { xform_raw = info.xforms[3u]; }
        else if (xform_id == 4u) { xform_raw = info.xforms[4u]; }
        else if (xform_id == 5u) { xform_raw = info.xforms[5u]; }
        else if (xform_id == 6u) { xform_raw = info.xforms[6u]; }
        else if (xform_id == 7u) { xform_raw = info.xforms[7u]; }
        else if (xform_id == 8u) { xform_raw = info.xforms[8u]; }
        else if (xform_id == 9u) { xform_raw = info.xforms[9u]; }
        else if (xform_id == 10u) { xform_raw = info.xforms[10u]; }
        else if (xform_id == 11u) { xform_raw = info.xforms[11u]; }
        else if (xform_id == 12u) { xform_raw = info.xforms[12u]; }
        else if (xform_id == 13u) { xform_raw = info.xforms[13u]; }
        xform[0u] = xform_raw[0u];
        xform[1u] = xform_raw[1u];
        xform[2u] = xform_raw[2u];
        xform[3u] = xform_raw[3u];
        xform[4u] = xform_raw[4u];
        xform[5u] = xform_raw[5u];
    }

    let v_position_w_m = camera.perspective_m *
                         matrix_for_transform_and_scale(model_transform, model_scale) *
                         camera.look_at_lhs_m *
                         model_to_eye_rotation *
                         matrix_for_xform(xform) * // transform sub-parts into place in model space
                         vec4(model_position, 1.); // vertex position in model space meters

    let v_normal_w = (model_to_eye_rotation *
                      rotation_for_xform(xform) *
                      vec4(model_normal, 1.)).xyz;

    let v_tex_coord = vec2(
        f32(tex_coord.x) / f32(chunk_atlas_size.width),
        f32(tex_coord.y) / f32(chunk_atlas_size.height)
    );

    let base_flag = instance_idx * 2u;
    let f_flags0 = flags0 & info.flags0;
    let f_flags1 = flags1 & info.flags1;

    return VertexOut(
        v_position_w_m,
        v_position_w_m,
        color,
        v_normal_w,
        v_tex_coord,
        f_flags0,
        f_flags1,
    );
}

//// Fragment shader
struct MaybeColor {
    color: vec4<f32>,
    should_discard: bool,
}

fn diffuse_color(v_color: vec4<f32>, v_tex_coord: vec2<f32>, f_flags0: u32, f_flags1: u32) -> MaybeColor {
    let tex_color = textureSample(chunk_mega_atlas_texture, chunk_mega_atlas_sampler, v_tex_coord);

    if ((f_flags0 & 0xFFFFFFFEu) == 0u && f_flags1 == 0u) {
        return MaybeColor(vec4(0.), true);
    } else if (v_tex_coord.x == 0.0) {
        return MaybeColor(v_color, false);
    } else if ((f_flags0 & 1u) == 1u) {
        return MaybeColor(vec4((1.0 - tex_color[3]) * v_color.xyz + tex_color[3] * tex_color.xyz, 1.0), false);
    } else if (tex_color.a < 0.5) {
        return MaybeColor(vec4(tex_color.rgb, 0.), true);
    } else {
        return MaybeColor(tex_color, false);
    }
}

struct FragOut {
    @location(0) f_color: vec4<f32>
}

@fragment
fn frag_main(
    @location(0) v_position_w_m: vec4<f32>,
    @location(1) v_color: vec4<f32>,
    @location(2) v_normal_w: vec3<f32>,
    @location(3) v_tex_coord: vec2<f32>,
    @location(4) f_flags0: u32,
    @location(5) f_flags1: u32,
) -> FragOut {
    let rv = diffuse_color(v_color, v_tex_coord, f_flags0, f_flags1);
    let diffuse = rv.color;
    let should_discard = rv.should_discard;

    let camera_position_w_km = camera.position_km.xyz;
    let sun_direction_w = orrery.sun_direction.xyz;
    let intersect_w_m = camera.inverse_view_m * camera.inverse_perspective_m * v_position_w_m;
    let intersect_w_km = intersect_w_m / 1000.;
    let camera_direction_w = normalize(intersect_w_km.xyz - camera_position_w_km);

    var radiance = radiance_at_point(
        atmosphere,
        transmittance_texture,
        transmittance_sampler,
        irradiance_texture,
        irradiance_sampler,
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        intersect_w_km.xyz,
        v_normal_w,
        diffuse.rgb,
        sun_direction_w,
        camera_position_w_km,
        camera_direction_w
    );

    // Afterburner tagged (and enabled), act as emissive
    if ((f_flags0 & 0x00000004u) != 0u) {
        radiance = diffuse.rgb * 10.5;
    }

    // We can only discard after doing all texture reads.
    // TODO: see if we can discard early by storing dx/dy and using sample_grad
    if (should_discard) {
        discard;
    }

    let color = tone_mapping(radiance, atmosphere.whitepoint.xyz, camera.exposure, camera.tone_gamma);
    return FragOut(vec4(color, diffuse.a));
}
