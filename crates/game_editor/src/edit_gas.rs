// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldRef, Form, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument,
};
use anyhow::Result;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::Ui;
use gas::{GasType, GasTypeIo};
use installations::GameInstallation;
use nitrous::HeapMut;
use smallvec::{smallvec, SmallVec};
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct GasEdit;

impl EditableDocument for GasEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<GasImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn graphics_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".GAS [External Tank Type]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".GAS") && ui.button("Edit").clicked() {
            return Ok(Some(GasImporter.import(info, install, collection, heap)?));
        }
        Ok(None)
    }
}

// Note: currently leaning on the TypeManager cache
#[derive(Debug, Default)]
pub struct GasImporter;

impl DocumentImportProvider for GasImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("GAS"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Description", "Weight", "Capacity"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(smallvec![
            info.name().to_owned(),
            xt.names().long_name().to_owned(),
            xt.gas()?.weight().to_string(),
            xt.gas()?.fuel().to_string(),
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(EditState::EditDocument(
            Box::new(GasEditState::new(xt.gas()?.to_owned())) as Box<dyn DocumentEditProvider>,
        ))
    }
}

#[derive(Clone, Debug)]
pub(crate) struct GasEditState {
    gas: GasTypeIo,
}

impl DocumentEditProvider for GasEditState {
    fn plugin(&self) -> &'static str {
        "GAS"
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        use FormField as FF;
        Ok(vec![
            Widget::H1(format!(
                "{}: {}",
                self.gas.si_names().file_name(),
                self.gas.si_names().long_name()
            )),
            Widget::Form(Form::from_table(
                "gas_form",
                vec![
                    FF::new("gas_name", self)?.with_label("Name"),
                    FF::new("gas_weight", self)?
                        .with_label("Tank Weight")
                        .with_hover_text("The weight of the tank without fuel"),
                    FF::new("gas_fuel", self)?
                        .with_label("Fuel Capacity (lbs)")
                        .with_hover_text("The weight (in lbs) of fuel that the tank can hold"),
                ],
            )),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        let gas = &mut self.gas;
        Ok(match name {
            "gas_name" => FieldRef::DosFilename(gas.si_names_mut().file_name_mut()),
            "gas_weight" => FieldRef::U16(gas.weight_mut()),
            "gas_fuel" => FieldRef::U32(gas.fuel_mut()),
            _ => panic!("Unknown name {name}"),
        })
    }

    fn serialize(&self, _heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>> {
        Ok(vec![(
            self.gas.si_names().file_name().to_owned(),
            self.gas.to_string().as_bytes().to_owned(),
        )])
    }
}

impl GasEditState {
    pub(crate) fn new(gas: GasType) -> Self {
        Self {
            gas: GasTypeIo::from(&gas),
        }
    }
}
