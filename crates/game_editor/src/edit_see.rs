// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldRef, Form, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument,
};
use anyhow::Result;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::Ui;
use installations::GameInstallation;
use nitrous::HeapMut;
use see::{SeeType, SeeTypeIo};
use smallvec::{smallvec, SmallVec};
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct SeeEdit;

impl EditableDocument for SeeEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<SeeImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn graphics_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".SEE [Seeker Type]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".SEE") && ui.button("Edit").clicked() {
            return Ok(Some(SeeImporter.import(info, install, collection, heap)?));
        }
        Ok(None)
    }
}

// Note: currently leaning on the TypeManager cache
#[derive(Debug, Default)]
pub struct SeeImporter;

impl DocumentImportProvider for SeeImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("SEE"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Description", "Weight"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(smallvec![
            info.name().to_owned(),
            xt.names().long_name().to_owned(),
            xt.see()?.weight().to_string()
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(EditState::EditDocument(
            Box::new(SeeEditState::new(xt.see()?.to_owned())) as Box<dyn DocumentEditProvider>,
        ))
    }
}

#[derive(Clone, Debug)]
pub(crate) struct SeeEditState {
    see: SeeTypeIo,
}

impl DocumentEditProvider for SeeEditState {
    fn plugin(&self) -> &'static str {
        "SEE"
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        use FormField as FF;
        Ok(vec![
            Widget::H1(format!(
                "{}: {}",
                self.see.si_names().file_name(),
                self.see.si_names().long_name()
            )),
            Widget::Form(Form::from_table(
                "see_form",
                vec![
                    FF::new("see_name", self)?.with_label("Name"),
                    FF::new("see_weight", self)?
                        .with_label("Pod Weight")
                        .with_hover_text("The weight of an external seeker"),
                    FF::new("see_externally_loadable", self)?
                        .with_label("Externally Loadable")
                        .with_hover_text("True if can be equipped externally"),
                    FF::new("see_seeker_type", self)?
                        .with_label("Seeker Type")
                        .with_hover_text("sig - TODO: make this a bitflags"),
                    FF::new("see_radar_type", self)?
                        .with_label("Radar Type")
                        .with_hover_text("flags: 0 or 1"),
                    // Params
                    FF::new_h2("radar_params_header")?.with_label("Radar Parameters"),
                    FF::new("see_look_down", self)?
                        .with_label("Look Down")
                        .with_hover_text(
                            "0, 20, 30, or 50. Probably an angle. No idea what it does",
                        ),
                    FF::new("see_doppler_speed_above", self)?
                        .with_label("Doppler Speed Above")
                        .with_hover_text("Always 0"),
                    FF::new("see_doppler_speed_below", self)?
                        .with_label("Doppler Speed Below")
                        .with_hover_text("Always 0"),
                    FF::new("see_doppler_min_range", self)?
                        .with_label("Doppler Minimum Range")
                        .with_hover_text("Always 0"),
                    FF::new("see_all_aspect", self)?
                        .with_label("All Aspect")
                        .with_hover_text("Only on IR Seekers; 0 or 50"),
                    // RWS (probably)
                    FF::new_h2("radar_rws_header")?.with_label("RWS Radar Mode"),
                    FF::new("see_rws_fov_horizontal", self)?
                        .with_label("RWS FOV Horizontal")
                        .with_hover_text("In units of 1/182 of a degree"),
                    FF::new("see_rws_fov_pitch", self)?
                        .with_label("RWS FOV Vertical")
                        .with_hover_text("In units of 1/182 of a degree"),
                    FF::new("see_rws_min_range", self)?
                        .with_label("RWS Minimum Range")
                        .with_hover_text("Always 0"),
                    FF::new("see_rws_max_range", self)?
                        .with_label("RWS Maximum Range")
                        .with_hover_text("In feet, nautical miles divisible by 6076'"),
                    FF::new("see_rws_min_altitude", self)?
                        .with_label("RWS Minimum Altitude")
                        .with_hover_text("Always -2147483648 (i32::MIN), or 256"),
                    FF::new("see_rws_max_altitude", self)?
                        .with_label("RWS Maximum Altitude")
                        .with_hover_text("Always 2147483647 (i32::MAX)"),
                    // TWS (probably)
                    FF::new_h2("radar_tws_header")?.with_label("TWS Radar Mode"),
                    FF::new("see_tws_fov_horizontal", self)?
                        .with_label("TWS FOV Horizontal")
                        .with_hover_text("In units of 1/182 of a degree"),
                    FF::new("see_tws_fov_pitch", self)?
                        .with_label("TWS FOV Vertical")
                        .with_hover_text("In units of 1/182 of a degree"),
                    FF::new("see_tws_min_range", self)?
                        .with_label("TWS Minimum Range")
                        .with_hover_text("Always 0"),
                    FF::new("see_tws_max_range", self)?
                        .with_label("TWS Maximum Range")
                        .with_hover_text("In feet, nautical miles divisible by 6076'"),
                    FF::new("see_tws_min_altitude", self)?
                        .with_label("TWS Minimum Altitude")
                        .with_hover_text("Always -2147483648 (i32::MIN), or 256"),
                    FF::new("see_tws_max_altitude", self)?
                        .with_label("TWS Maximum Altitude")
                        .with_hover_text("Always 2147483647 (i32::MAX)"),
                    // ECM effectiveness (on this seeker)
                    FF::new_h2("ecm_effect_header")?.with_label("ECM Effect"),
                    FF::new("see_chaff_flare_chance", self)?
                        .with_label("Chaff Flare Chance")
                        .with_hover_text("Always 100"),
                    FF::new("see_deception_chance", self)?
                        .with_label("Deception Chance")
                        .with_hover_text("Always 100"),
                ],
            )),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        let see = &mut self.see;
        Ok(match name {
            "see_name" => FieldRef::DosFilename(see.si_names_mut().file_name_mut()),
            "see_weight" => FieldRef::U16(see.weight_mut()),
            "see_externally_loadable" => FieldRef::U8(see.externally_loadable_mut()),
            "see_seeker_type" => FieldRef::U8(see.seeker_type_mut()),
            "see_radar_type" => FieldRef::U8(see.radar_type_mut()),
            // Params
            "see_look_down" => FieldRef::U8(see.look_down_mut()),
            "see_doppler_speed_above" => FieldRef::U8(see.doppler_speed_above_mut()),
            "see_doppler_speed_below" => FieldRef::U8(see.doppler_speed_below_mut()),
            "see_doppler_min_range" => FieldRef::U8(see.doppler_min_range_mut()),
            "see_all_aspect" => FieldRef::U8(see.all_aspect_mut()),
            // RWS
            "see_rws_fov_horizontal" => FieldRef::I16(see.rws_fov_horizontal_mut()),
            "see_rws_fov_pitch" => FieldRef::I16(see.rws_fov_pitch_mut()),
            "see_rws_min_range" => FieldRef::U32(see.rws_min_range_mut()),
            "see_rws_max_range" => FieldRef::U32(see.rws_max_range_mut()),
            "see_rws_min_altitude" => FieldRef::I32(see.rws_min_altitude_mut()),
            "see_rws_max_altitude" => FieldRef::I32(see.rws_max_altitude_mut()),
            // TWS
            "see_tws_fov_horizontal" => FieldRef::I16(see.tws_fov_horizontal_mut()),
            "see_tws_fov_pitch" => FieldRef::I16(see.tws_fov_pitch_mut()),
            "see_tws_min_range" => FieldRef::U32(see.tws_min_range_mut()),
            "see_tws_max_range" => FieldRef::U32(see.tws_max_range_mut()),
            "see_tws_min_altitude" => FieldRef::I32(see.tws_min_altitude_mut()),
            "see_tws_max_altitude" => FieldRef::I32(see.tws_max_altitude_mut()),
            // ECM Effect
            "see_chaff_flare_chance" => FieldRef::U8(see.chaff_flare_chance_mut()),
            "see_deception_chance" => FieldRef::U8(see.deception_chance_mut()),
            _ => panic!("Unknown name {name}"),
        })
    }

    fn serialize(&self, _heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>> {
        Ok(vec![(
            self.see.si_names().file_name().to_owned(),
            self.see.to_string().as_bytes().to_owned(),
        )])
    }
}

impl SeeEditState {
    pub(crate) fn new(see: SeeType) -> Self {
        Self {
            see: SeeTypeIo::from(&see),
        }
    }
}
