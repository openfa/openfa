// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldDisplay, FieldRef, Form, FormEvent, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument, PanelKind,
};
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use game_loader::GameLoader;
use geodesy::{Bearing, PitchCline};
use installations::{from_dos_string, GameInstallation};
use mmm::{MissionMap, ObjectProvider};
use nitrous::HeapMut;
use orrery::Orrery;
use smallvec::{smallvec, SmallVec};
use std::collections::HashMap;
use t2_terrain::T2TileSet;
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct MmEdit;

impl EditableDocument for MmEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<MmImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, _ui: &mut egui::Ui) -> Option<EditState> {
        None
    }
    fn graphics_menu(&self, ui: &mut egui::Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn waiting_menu(&self, ui: &mut egui::Ui) -> Option<EditState> {
        if ui.button(".MM [Mission Map]")
            .on_hover_text("Mission Maps contain a base set of objects that can be easily shared between Missions").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut egui::Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".MM") && ui.button("Edit").clicked() {
            return Ok(Some(
                MmImporter::default().import(info, install, collection, heap)?,
            ));
        }
        Ok(None)
    }
}

#[derive(Debug, Default)]
pub struct MmImporter {
    cache: HashMap<String, MissionMap>,
}

impl DocumentImportProvider for MmImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("MM"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Map", "Layer", "LayerId", "Object Count"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let typeman = heap.resource::<TypeManager>();
        let map = self.cache.entry(info.to_string()).or_insert_with(|| {
            let contents = from_dos_string(info.data().unwrap());
            MissionMap::from_str(&contents, typeman, collection).unwrap()
        });

        Ok(smallvec![
            info.name().to_owned(),
            map.map_name().t2_name(),
            map.layer_name().mm_name().to_owned(),
            map.layer_index().to_string(),
            map.objects().count().to_string(),
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState> {
        Ok(EditState::EditDocument(Box::new(MmEditState::new(
            &info, // heap.resource::<TypeManager>(),
            collection, heap,
        )?)
            as Box<dyn DocumentEditProvider>))
    }
}

#[derive(Debug)]
pub(crate) struct MmEditState {
    name: String,
    map: MissionMap,
}

impl DocumentEditProvider for MmEditState {
    fn plugin(&self) -> &'static str {
        "MM"
    }

    fn panel_requirements(&self) -> PanelKind {
        PanelKind::Side
    }

    fn show_custom_document_edit_ux(
        &mut self,
        _ui: &mut egui::Ui,
        mut _heap: HeapMut,
    ) -> Result<()> {
        // heap.resource_scope(|heap: HeapMut, mut markers: Mut<Markers>| todo!());
        Ok(())
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        let fields = vec![
            FormField::new("display_name", self)?.with_label("Name"),
            FormField::new_missing("airport_selector")?.with_display(
                FieldDisplay::DropdownString {
                    value: "<Select to View>".to_string(),
                    options: self
                        .map
                        .analysis()
                        .airports()
                        .iter()
                        .map(|ap| ap.name().to_owned())
                        .collect(),
                },
            ),
        ];
        Ok(vec![
            Widget::H1(self.name.clone()),
            Widget::Form(Form::from_table("_display_form", fields)),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        Ok(match name {
            //"tabs" => FieldRef::Tab(self.edit_mode.label().to_owned()),
            // display area
            "display_name" => FieldRef::DosFilename(&mut self.name),
            //"display_extents" => FieldRef::Checkbox(&mut self.show_extents),
            name => bail!("unknown field name {name}"),
        })
    }

    fn apply_event(
        &mut self,
        event: &FormEvent,
        _filesystem: (&GameInstallation, &AssetCollection),
        mut heap: HeapMut,
    ) -> Result<()> {
        match event.name() {
            "airport_selector" => {
                let name = event.next().string()?;
                let id = self.map.analysis().airport(name)?.part1();
                let fa_pos = self.map.object(id)?.position();

                let pos = heap
                    .get::<T2TileSet>("map_under_edit")
                    .mapper()
                    .fa_world_to_geodetic(fa_pos);
                // FIXME: smooth move
                // heap.get_mut::<ArcBallController>("camera")?.set_target(pos);
                println!("POS: {}", pos);
                heap.get_mut::<ArcBallController>("camera")?.move_to(
                    pos,
                    Bearing::north(),
                    PitchCline::level(),
                    meters!(100),
                )?;
            }
            name => bail!("unknown field name {name}"),
        }
        Ok(())
    }

    fn serialize(&self, _heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>> {
        // let sh_code = heap.resource::<Shape>().get_code(self.shape_ids.normal());
        // Ok((self.name.clone(), sh_code.compile_pe()?))
        //todo!()
        Ok(vec![])
    }

    fn enter_document_edit(&mut self, mut heap: HeapMut) -> Result<()> {
        // Just jump to the target to avoid flickering like crazy if it's too far off.
        // TODO: figure out local times and animate within a day cycle
        heap.resource_mut::<Orrery>()
            .set_date_time(1969, 1, 1, 20, 0, 0);

        // // Move the camera to where we want it
        // let arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
        // let init_position = arcball.map_frame().to_owned();
        // let init_distance = arcball.distance();
        // // let tgt_position = MapFrame::new(
        // //     Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
        // //     Bearing::new(degrees!(-113)),
        // //     PitchCline::new(degrees!(28.)),
        // // );
        // let tgt_distance = meters!(40);
        // // self.init_distance = init_distance;
        // // self.init_position = init_position.clone();
        // heap.resource_mut::<Timeline>().ease_in_out_to(
        //     Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
        //         let f = v[0].to_float()?;
        //         let mut arcball = heap.get_by_id_mut::<ArcBallController>(camera_ent)?;
        //         // arcball.set_map_frame(init_position.interpolate(&tgt_position, f));

        heap.resource_scope(|mut heap, loader: Mut<GameLoader>| {
            loader.load_mmm_common("map_under_edit", &self.map, &self.map, &mut heap)
        })?;

        /*
        for airport in self.map.analysis().airports() {
            let info = self.map.object(airport.part1())?;
            let fa_pos = info.position();
            let tile_set = heap.get::<T2TileSet>("map_under_edit");
            let pos = tile_set.mapper().fa_world_to_geodetic(fa_pos);

            let shape_info = if let Some(shape_file) = info.xt().ot()?.shape_file().as_ref() {
                let shape_ids = heap
                    .resource_scope(|heap, mut shapes: Mut<Shape>| {
                        shapes.upload_shape(
                            heap.resource::<Installations>().primary_palette(),
                            heap.resource::<AssetCatalog>().lookup(shape_file)?,
                            heap.resource::<AssetCatalog>(),
                            heap.resource::<Gpu>(),
                        )
                    })
                    .with_context(|| format!("loading map {map_name}"))?;
                Some((shape_file.to_owned(), shape_ids))
            } else {
                None
            };

            let id = Self::spawn_entity_common(
                airport.name(),
                shape_info,
                info,
                Some(tile_id),
                heap.as_mut(),
            )?;

            //heap.get_mut::<ArcBallController>("camera")?.set_target(pos);
        }
         */

        /*
        let arcball = heap.get_mut::<ArcBallController>("camera")?;
        let init_position = arcball.map_frame().to_owned();
        let init_distance = arcball.distance();
        let tgt_position = MapFrame::new(
            Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
            Bearing::new(degrees!(-113)),
            PitchCline::new(degrees!(28.)),
        );
        let tgt_distance = meters!(40);
        self.init_distance = init_distance;
        self.init_position = init_position.clone();
        heap.resource_mut::<Timeline>().ease_in_out_to(
            Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
                let f = v[0].to_float()?;
                let mut arcball = heap.get_mut::<ArcBallController>("camera")?;
                arcball.set_map_frame(init_position.interpolate(&tgt_position, f));
                arcball
                    .set_distance(init_distance + scalar!(f) * (tgt_distance - init_distance))?;
                Ok(Value::True())
            })),
            0.0.into(),
            1.0.into(),
            seconds!(1.5).f64(),
        )?;
         */

        Ok(())
    }

    fn leave_document_edit(&self, mut heap: HeapMut) -> Result<()> {
        heap.resource_scope(|heap: HeapMut, loader: Mut<GameLoader>| loader.unload_mission(heap))?;
        Ok(())
    }
}

impl MmEditState {
    fn new(
        info: &FileInfo,
        /*typeman: &TypeManager,*/ collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Self> {
        let contents = from_dos_string(info.data()?);
        let map = MissionMap::from_str(
            contents.as_ref(),
            heap.resource::<TypeManager>(),
            collection,
        )?;

        Ok(Self {
            name: info.name().to_owned(),
            map,
        })
    }
}
