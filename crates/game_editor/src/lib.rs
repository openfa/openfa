// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
mod edit_ecm;
mod edit_gas;
mod edit_hud;
mod edit_mmm;
mod edit_pt;
mod edit_see;
mod edit_sh;
mod widget;

use crate::{
    edit_ecm::EcmEdit,
    edit_gas::GasEdit,
    edit_hud::HudEdit,
    edit_mmm::MmEdit,
    edit_pt::PlaneTypeEdit,
    edit_see::SeeEdit,
    edit_sh::{ShapeEdit, ShapeImporter},
    widget::{FieldRef, FormEvent, Widget},
};
use anyhow::{bail, ensure, Result};
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::CameraStep;
use catalog::{AssetCatalog, AssetCollection, FileInfo, FileSystem, Order, Search};
use egui::TextBuffer;
use egui_extras::{Column, TableBuilder};
use event_mapper::{EventMapper, InputFocus};
use game_loader::GameStep;
use game_ux::{menubar::add_help_menu_items, UxState};
use geodb::GeoDbStep;
use gui::{egui_color, Gui, GuiStep};
use humansize::{format_size, DECIMAL};
use installations::{GameInstallation, Installations};
use mantle::{GpuStep, Window};
use marker::MarkersStep;
use nitrous::{inject_nitrous_component, method, HeapMut, NitrousComponent};
use orrery::{Orrery, OrreryStep};
use planck::rgba;
use runtime::{report_errors, Extension, Runtime, ScriptHerder, ScriptQueue, Scriptable};
use shape::ShapeStep;
use smallvec::{smallvec, SmallVec};
use spog::{DashboardStep, TerminalStep};
use std::{
    collections::BTreeMap,
    fmt::Debug,
    time::{Duration, Instant},
};
use terrain::TerrainStep;
use xt::TypeManager;

#[derive(Clone, Debug)]
pub enum DialogResponse {
    None,
    Save(Vec<(String, Vec<u8>)>),
    Close,
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Default)]
pub enum EditState {
    #[default]
    Waiting,
    ImportDocument(Box<dyn DocumentImportProvider>),
    EditDocument(Box<dyn DocumentEditProvider>),
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum EditLibUxStep {
    DrawUx,
}

// This trait defines high level functionality about a file type.
pub trait EditableDocument: Debug + Send + Sync + 'static {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider>;
    fn object_menu(&self, ui: &mut egui::Ui) -> Option<EditState>;
    fn graphics_menu(&self, ui: &mut egui::Ui) -> Option<EditState>;
    fn waiting_menu(&self, ui: &mut egui::Ui) -> Option<EditState>;
    fn document_actions(
        &self,
        ui: &mut egui::Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>>;
}

// Defines the interface for showing a list of documents. This should
// typically be constructed in EditableDocument::*_menu in order to move
// the UX to importing an item.
pub trait DocumentImportProvider: Debug + Send + Sync + 'static {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>>;
    fn column_headers(&self) -> SmallVec<[&'static str; 8]>;
    fn column_values(
        &mut self,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>>;
    fn import(
        &mut self,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState>;
}

pub enum PanelKind {
    Center,
    Side,
}

// Defines an editing interaction.
pub trait DocumentEditProvider: Debug + Send + Sync + 'static {
    // Should provide the file extension that the plugin is indexed under in the plugins list.
    fn plugin(&self) -> &'static str;

    fn panel_requirements(&self) -> PanelKind {
        PanelKind::Center
    }

    fn note_panel_intersect(&mut self, _in_panel: bool) {}

    fn toggle_show_help(&mut self) {}

    fn show_custom_document_edit_ux(&mut self, _ui: &mut egui::Ui, _heap: HeapMut) -> Result<()> {
        Ok(())
    }

    fn get_edit_widgets(&mut self, heap: HeapMut) -> Result<Vec<Widget>>;

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef>;

    fn apply_event(
        &mut self,
        event: &FormEvent,
        _filesystem: (&GameInstallation, &AssetCollection),
        _heap: HeapMut,
    ) -> Result<()> {
        bail!("did not expect event {}", event.name())
    }

    fn serialize(&self, heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>>;

    fn enter_document_edit(&mut self, _heap: HeapMut) -> Result<()> {
        Ok(())
    }

    fn leave_document_edit(&self, _heap: HeapMut) -> Result<()> {
        Ok(())
    }

    fn mouse_motion(
        &mut self,
        _cursor_x: f64,
        _cursor_y: f64,
        _shift_pressed: bool,
        _heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        Ok(smallvec![])
    }

    fn mouse_press(
        &mut self,
        _pressed: bool,
        _shift_pressed: bool,
        _heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        Ok(smallvec![])
    }
}

#[derive(Debug, NitrousComponent)]
pub struct EditLibUx {
    // The lib we are editing: and will save to.
    lib_name: String,

    // Current draw state
    search_collection: String,
    import_list_filter: String,
    state: EditState,
    undo_stack: Vec<FormEvent>,
    redo_stack: Vec<FormEvent>,

    // Hold the prior state live long enough for us to get a Heap to free it off of.
    leaving_state: EditState,

    // Document kinds for editing
    plugins: BTreeMap<String, Box<dyn EditableDocument>>,
}

impl Extension for EditLibUx {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_frame_system(
            Self::sys_draw_ux
                .pipe(report_errors)
                .in_set(EditLibUxStep::DrawUx)
                .after(GuiStep::StartFrame)
                .after(GpuStep::CreateCommandEncoder)
                .after(CameraStep::MoveCameraToFrame)
                .before(TerminalStep::Show)
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry)
                // Routines that may call quit on the window don't conflict with users of the size
                .after(CameraStep::HandleDisplayChange)
                .ambiguous_with(GpuStep::CreateTargetSurface)
                .ambiguous_with(TerrainStep::HandleDisplayChange)
                .ambiguous_with(OrreryStep::UploadToGpu)
                .ambiguous_with(GeoDbStep::CheckDownloads)
                // For consistency with markers and other edited items
                .before(CameraStep::UploadToGpu)
                .before(DashboardStep::Show)
                .before(ShapeStep::AnimateDrawState)
                .before(ShapeStep::ApplyXforms)
                .before(ShapeStep::ApplyFlags)
                .before(ShapeStep::ApplyTransforms)
                .before(TerminalStep::ReportScriptCompletions)
                .before(TerrainStep::OptimizePatches)
                .before(ShapeStep::ResetUploadCursor)
                .before(GameStep::LinkAirportHalves),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl EditLibUx {
    pub fn new(lib_name: &str, file_name: Option<&str>, mut heap: HeapMut) -> Result<Self> {
        heap.resource_mut::<Orrery>()
            .set_date_time(1964, 2, 24, 7, 0, 0);

        let mut arcball = heap.get_mut::<ArcBallController>("camera")?;
        let name = "ISS";
        let target = arcball.notable_location(name)?;
        let bearing = arcball.bearing_for_notable_location(name);
        let pitch = arcball.pitch_for_notable_location(name);
        let distance = arcball.distance_for_notable_location(name);
        arcball.move_to(target, bearing, pitch, distance)?;
        // FIXME: replace with smooth movement
        // arcball.set_target(target);
        // arcball.set_bearing(bearing);
        // arcball.set_pitch(pitch);
        // arcball.set_distance(distance)?;

        let mut plugins = BTreeMap::new();
        plugins.insert(
            "PT".into(),
            Box::<PlaneTypeEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "ECM".into(),
            Box::<EcmEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "GAS".into(),
            Box::<GasEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "SEE".into(),
            Box::<SeeEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "HUD".into(),
            Box::<HudEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "MM".into(),
            Box::<MmEdit>::default() as Box<dyn EditableDocument>,
        );
        plugins.insert(
            "SH".into(),
            Box::<ShapeEdit>::default() as Box<dyn EditableDocument>,
        );

        let search_collection = heap
            .resource::<AssetCatalog>()
            .primary_collection_name()
            .to_owned();
        let mut state = EditState::default();
        if let Some(filename) = file_name {
            for (ext, plugin) in plugins.iter() {
                if filename.ends_with(ext as &str) {
                    let mut imp = plugin.make_importer();

                    state =
                        heap.resource_scope(|mut heap: HeapMut, catalog: Mut<AssetCatalog>| {
                            heap.resource_scope(
                                |mut heap: HeapMut, installs: Mut<Installations>| {
                                    let collection =
                                        catalog.collection(&search_collection).unwrap();
                                    let install =
                                        installs.installation(&search_collection).unwrap();
                                    let info = collection.lookup(filename).unwrap();
                                    imp.import(info, install, collection, heap.as_mut())
                                },
                            )
                        })?;
                    heap.resource_mut::<ScriptQueue>()
                        .run("@ux.edit_lib_ux.enter_document_edit()");
                }
            }
        }
        Ok(Self {
            lib_name: lib_name.to_owned(),
            search_collection,
            import_list_filter: String::new(),
            state,
            undo_stack: Vec::new(),
            redo_stack: Vec::new(),
            leaving_state: EditState::default(),
            plugins,
        })
    }

    pub fn unload(&self, _heap: HeapMut) -> Result<()> {
        Ok(())
    }

    #[method]
    pub fn escape_pressed(&mut self, mut heap: HeapMut) -> Result<()> {
        let next_state = match &self.state {
            EditState::Waiting => {
                heap.resource_mut::<ScriptQueue>().run("ux.edit_libs()");
                EditState::Waiting
            }
            EditState::ImportDocument(_) => EditState::Waiting,
            EditState::EditDocument(edit) => {
                EditState::ImportDocument(self.plugins.get(edit.plugin()).unwrap().make_importer())
            }
        };
        let herder: &mut ScriptQueue = &mut heap.resource_mut::<ScriptQueue>();
        self.transition_to_state(next_state, herder);
        Ok(())
    }

    #[method]
    pub fn undo_pressed(&mut self, heap: HeapMut) -> Result<()> {
        let provider = if let EditState::EditDocument(provider) = &mut self.state {
            provider.as_mut()
        } else {
            // If we are not in a document edit mode, there is undo/redo stack.
            return Ok(());
        };
        Self::transfer_event(provider, &mut self.undo_stack, &mut self.redo_stack, heap)
    }

    #[method]
    pub fn redo_pressed(&mut self, heap: HeapMut) -> Result<()> {
        let provider = if let EditState::EditDocument(provider) = &mut self.state {
            provider.as_mut()
        } else {
            // If we are not in a document edit mode, there is undo/redo stack.
            return Ok(());
        };
        Self::transfer_event(provider, &mut self.redo_stack, &mut self.undo_stack, heap)
    }

    fn transfer_event(
        provider: &mut dyn DocumentEditProvider,
        from_buffer: &mut Vec<FormEvent>,
        to_buffer: &mut Vec<FormEvent>,
        mut heap: HeapMut,
    ) -> Result<()> {
        // Extract our most recently run event.
        let mut unpop_event: Option<FormEvent> = None;
        let mut last_event_info: Option<(String, Instant)> = None;
        while let Some(event) = from_buffer.pop() {
            // Compress related events by skipping past any events with the same name
            if let Some((prior_name, prior_time)) = &last_event_info {
                // We do not want to skip if the name of the prior event is different
                // or if there is significant elapsed time between them.
                if event.name() != prior_name
                    || prior_time.duration_since(event.time()) > Duration::from_millis(50)
                    || event.time().duration_since(*prior_time) > Duration::from_millis(50)
                {
                    // Note: we need to unpop this event since we're not running it.
                    unpop_event = Some(event);
                    break;
                }
            }

            // Invert and re-run it
            let event = event.invert();
            heap.resource_scope(|mut heap: HeapMut, installs: Mut<Installations>| {
                heap.resource_scope(|heap: HeapMut, catalog: Mut<AssetCatalog>| {
                    Self::apply_event(
                        &event,
                        provider,
                        (installs.primary(), catalog.primary()),
                        heap,
                    )
                })
            })?;

            // Push it back onto our other stack at the end.
            last_event_info = Some((event.name().to_string(), event.time()));
            to_buffer.push(event);
        }
        // Re-push our peek'd event
        if let Some(event) = unpop_event {
            from_buffer.push(event);
        }
        Ok(())
    }

    #[method]
    fn toggle_show_help(&mut self) {
        if let EditState::EditDocument(doc) = &mut self.state {
            doc.toggle_show_help();
        }
    }

    #[method]
    fn edit_sh(&mut self, name: String, mut heap: HeapMut) -> Result<()> {
        let next_state =
            heap.resource_scope(|mut heap: HeapMut, installations: Mut<Installations>| {
                heap.resource_scope(|heap: HeapMut, catalog: Mut<AssetCatalog>| {
                    let install = installations.primary();
                    let collection = catalog.primary();
                    collection.lookup(&name).map(|info| {
                        ShapeImporter::default().import(info, install, collection, heap)
                    })
                })
            })??;
        let herder: &mut ScriptQueue = &mut heap.resource_mut::<ScriptQueue>();
        self.transition_to_state(next_state, herder);
        Ok(())
    }

    fn transition_to_state<S: Scriptable>(&mut self, state: EditState, herder: &mut S) {
        // Make sure we close the old state before entering a new one.
        if matches!(self.state, EditState::EditDocument(_)) {
            self.close_document_edit(herder).ok();
        }

        // Scripts run in order, so we should now be able to enter the new doc.
        if matches!(state, EditState::EditDocument(_)) {
            herder.run("@ux.edit_lib_ux.enter_document_edit()");
        }

        self.state = state;
    }

    fn sys_draw_ux(world: &mut World) -> Result<()> {
        let mut heap = HeapMut::wrap(world);
        let mut slf = if let Ok(mut nem) = heap.entity_mut("ux") {
            if let Some(slf) = nem.take::<EditLibUx>() {
                slf
            } else {
                return Ok(());
            }
        } else {
            return Ok(());
        };
        let rv = heap.resource_scope(|mut heap: HeapMut, gui: Mut<Gui>| {
            // We don't want anyone going around Catalog or Installs to edit the wrong thing by accident.
            heap.resource_scope(|mut heap: HeapMut, mut catalog: Mut<AssetCatalog>| {
                heap.resource_scope(|heap: HeapMut, mut installs: Mut<Installations>| {
                    slf.draw_edit_lib_ux(&gui, &mut catalog, &mut installs, heap)
                })
            })
        });
        heap.entity_mut("ux")?.inject(slf)?;
        rv
    }

    fn draw_edit_lib_ux(
        &mut self,
        gui: &Gui,
        catalog: &mut AssetCatalog,
        installs: &mut Installations,
        mut heap: HeapMut,
    ) -> Result<()> {
        egui::TopBottomPanel::top("top_panel_edit_lib")
            .show(gui.screen().ctx(), |ui| -> Result<()> {
                egui::menu::bar(ui, |ui| -> Result<()> {
                    ui.menu_button("File", |ui| -> Result<()> {
                        if ui.button("Return to Game").clicked() {
                            // Note: saved by unload
                            heap.resource_mut::<ScriptHerder>()
                                .run_string("ux.return_to_top()")?;
                        }
                        if ui.button("Quit to Desktop").clicked() {
                            heap.resource_mut::<Window>().request_quit();
                        }
                        Ok(())
                    });

                    ui.menu_button("Objects", |ui| {
                        for plugin in self.plugins.values() {
                            if let Some(state) = plugin.object_menu(ui) {
                                self.state = state;

                                // I think this should remain set, once set?
                                // self.selected_catalog = libs.game_catalog().name().to_owned();
                            }
                        }
                    });

                    ui.menu_button("Graphics", |ui| {
                        for plugin in self.plugins.values() {
                            if let Some(state) = plugin.graphics_menu(ui) {
                                self.state = state;
                            }
                        }
                    });

                    ui.menu_button("Help", |ui| -> Result<()> {
                        add_help_menu_items(ui, &mut heap.resource_mut::<ScriptHerder>())
                    });

                    Ok(())
                })
                .inner
            })
            .inner?;

        // Left Panel: the lib content
        egui::SidePanel::left("left_panel_edit_lib_contents")
            .default_width(200.)
            .frame(
                egui::Frame::window(&gui.screen().ctx().style())
                    .shadow(egui::epaint::Shadow::NONE)
                    .fill(egui_color(rgba!(0x202020E0))),
            )
            .show(gui.screen().ctx(), |ui| -> Result<()> {
                self.draw_lib_content(ui, catalog, installs, heap.as_mut())?;
                Ok(())
            });

        // Middle panel: whatever we are currently doing.
        match &mut self.state {
            EditState::Waiting => {
                egui::CentralPanel::default()
                    .frame(
                        egui::Frame::window(&gui.screen().ctx().style())
                            .shadow(egui::epaint::Shadow::NONE)
                            .fill(egui_color(rgba!(0x202020E0))),
                    )
                    .show(gui.screen().ctx(), |ui| -> Result<()> {
                        self.draw_waiting_for_input(ui)?;
                        Ok(())
                    })
                    .inner?;
            }
            EditState::ImportDocument(_provider) => {
                egui::CentralPanel::default()
                    .frame(
                        egui::Frame::window(&gui.screen().ctx().style())
                            .shadow(egui::epaint::Shadow::NONE)
                            .fill(egui_color(rgba!(0x202020E0))),
                    )
                    .show(gui.screen().ctx(), |ui| -> Result<()> {
                        self.draw_document_importer(ui, catalog, installs, heap.as_mut())?;
                        Ok(())
                    })
                    .inner?;
            }
            EditState::EditDocument(provider) => match provider.panel_requirements() {
                PanelKind::Center => {
                    egui::CentralPanel::default()
                        .frame(
                            egui::Frame::window(&gui.screen().ctx().style())
                                .shadow(egui::epaint::Shadow::NONE)
                                .fill(egui_color(rgba!(0x202020E0))),
                        )
                        .show(gui.screen().ctx(), |ui| -> Result<()> {
                            self.draw_document_edit(ui, installs, catalog, heap.as_mut())?;
                            Ok(())
                        })
                        .inner?;
                }
                PanelKind::Side => {
                    egui::SidePanel::right("edit_document_side_panel")
                        .frame(
                            egui::Frame::window(&gui.screen().ctx().style())
                                .shadow(egui::epaint::Shadow::NONE)
                                .fill(egui_color(rgba!(0x202020E0))),
                        )
                        .resizable(true)
                        .default_width(500.)
                        .min_width(350.)
                        .show(gui.screen().ctx(), |ui| -> Result<()> {
                            self.draw_document_edit(ui, installs, catalog, heap.as_mut())?;
                            Ok(())
                        })
                        .inner?;
                }
            },
        }

        Ok(())
    }

    fn draw_lib_content(
        &mut self,
        ui: &mut egui::Ui,
        catalog: &mut AssetCatalog,
        installs: &mut Installations,
        mut heap: HeapMut,
    ) -> Result<()> {
        ui.vertical_centered(|ui| ui.heading(format!("{} Content", self.lib_name)));
        ui.separator();

        // Note: edit_libs shows only the primary, so our provider should be in the primary
        //       If you select a different game from the console in the mean time, god help you.
        let collection = catalog.primary();
        let install = installs.primary();
        let lib_provider = collection.provider(&self.lib_name)?;
        let lib_content = lib_provider.search(&Search::for_glob("*").sort(Order::Asc))?;

        // List all files in the lib.
        let row_height_sans_spacing = ui.text_style_height(&egui::TextStyle::Body);
        let table = TableBuilder::new(ui)
            .striped(true)
            .resizable(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .column(Column::auto_with_initial_suggestion(100.0).range(40.0..=300.0))
            .column(Column::auto_with_initial_suggestion(40.).range(40.0..=300.0))
            .column(Column::auto_with_initial_suggestion(40.).range(40.0..=300.0))
            .column(Column::remainder())
            .min_scrolled_height(0.0);
        let mut removal = None;
        let mut next_state = None;
        table
            .header(20.0, |mut header| {
                for column in ["Name", "Disk", "Mem", "Actions"] {
                    header.col(|ui| {
                        ui.strong(column);
                    });
                }
            })
            .body(|body| {
                body.rows(row_height_sans_spacing, lib_content.len(), |mut row| {
                    let info = lib_content[row.index()];
                    let meta = lib_provider.stat(info.name()).unwrap();
                    row.col(|ui| {
                        ui.label(info.name());
                    });
                    row.col(|ui| {
                        ui.label(format_size(meta.compressed_size(), DECIMAL));
                    });
                    row.col(|ui| {
                        ui.label(format_size(meta.size(), DECIMAL));
                    });
                    row.col(|ui| {
                        for plugin in self.plugins.values() {
                            if let Some(state) = plugin
                                .document_actions(ui, info, install, collection, heap.as_mut())
                                .expect("egui_extras::Table does not support graceful failure")
                            {
                                next_state = Some(state);
                            }
                        }
                        if ui.button("Remove").clicked() {
                            removal = Some(info.name().to_owned());
                        }
                    });
                });
            });

        if let Some(name) = removal {
            let collection = catalog.primary_mut();
            let lib_provider = collection.provider_mut(&self.lib_name)?;
            lib_provider.remove(&name)?;
        }

        if let Some(state) = next_state {
            let herder: &mut ScriptHerder = &mut heap.resource_mut::<ScriptHerder>();
            self.transition_to_state(state, herder);
        }

        Ok(())
    }

    // Link to show the kinds of documents we could be importing.
    fn draw_waiting_for_input(&mut self, ui: &mut egui::Ui) -> Result<()> {
        ui.heading("Import a file into this lib:");
        ui.separator();
        for provider in self.plugins.values() {
            if let Some(edit_state) = provider.waiting_menu(ui) {
                self.state = edit_state;
            }
        }
        Ok(())
    }

    fn draw_document_importer(
        &mut self,
        ui: &mut egui::Ui,
        catalog: &mut AssetCatalog,
        installs: &mut Installations,
        mut heap: HeapMut,
    ) -> Result<()> {
        // Note: because we need both self and self.state as mut here, doing the borrow requires
        //       a bit of extra work to inform rust that we are still being safe.
        let provider = if let EditState::ImportDocument(provider) = &mut self.state {
            provider.as_mut()
        } else {
            panic!("entered draw_document_importer in wrong state");
        };

        // Draw common controls
        egui::Grid::new("draw_document_importer_filter_grid")
            .num_columns(3)
            .show(ui, |ui| -> Result<()> {
                ui.label("Filter:");
                egui::ComboBox::from_label("")
                    .selected_text(&self.search_collection)
                    .show_ui(ui, |ui| {
                        ui.style_mut().wrap = Some(false);
                        ui.set_min_width(100.0);
                        for name in installs.installation_names() {
                            ui.selectable_value(&mut self.search_collection, name.to_owned(), name);
                        }
                    });
                ui.text_edit_singleline(&mut self.import_list_filter);
                ui.end_row();
                // FIXME: Sorting support!
                Ok(())
            });

        let collection = catalog.collection(&self.search_collection)?;
        let install = installs.installation(&self.search_collection)?;

        // Draw table
        let available_height = ui.available_height();
        let row_height_sans_spacing = ui.text_style_height(&egui::TextStyle::Body) + 2.;
        let table = TableBuilder::new(ui)
            .striped(true)
            .resizable(true)
            .min_scrolled_height(available_height)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .column(Column::auto_with_initial_suggestion(40.0).range(40.0..=300.0))
            .columns(
                Column::auto_with_initial_suggestion(100.0).range(40.0..=300.0),
                provider.column_headers().len() - 1,
            )
            .column(Column::remainder());
        let mut next_state: Option<EditState> = None;
        table
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.strong("");
                });
                for column in provider.column_headers() {
                    header.col(|ui| {
                        ui.strong(column);
                    });
                }
            })
            .body(|body| {
                let fids = provider
                    .all_documents(collection)
                    .expect("egui_extras::Table does not support graceful failure")
                    .drain(..)
                    .filter(|info| info.name().contains(&self.import_list_filter))
                    .collect::<Vec<_>>();
                body.rows(row_height_sans_spacing, fids.len(), |mut row| {
                    let info = &fids[row.index()];
                    row.col(|ui| {
                        if ui.button("Import").clicked() {
                            next_state = Some(
                                provider
                                    .import(*info, install, collection, heap.as_mut())
                                    .expect("egui_extras::Table does not support graceful failure"),
                            );
                            if let Some(EditState::EditDocument(_)) = next_state.as_ref() {
                                heap.resource_mut::<ScriptHerder>()
                                    .run_string("@ux.edit_lib_ux.enter_document_edit()")
                                    .ok();
                            }
                        }
                    });
                    for value in provider
                        .column_values(*info, install, collection, heap.as_mut())
                        .expect("egui_extras::Table does not support graceful failure")
                    {
                        row.col(|ui| {
                            ui.label(value);
                        });
                    }
                });
            });

        if let Some(state) = next_state {
            self.state = state;
        }

        Ok(())
    }

    fn apply_events(
        undo_stack: &mut Vec<FormEvent>,
        redo_stack: &mut Vec<FormEvent>,
        mut events: SmallVec<[FormEvent; 2]>,
        provider: &mut dyn DocumentEditProvider,
        filesystem: (&GameInstallation, &AssetCollection),
        mut heap: HeapMut,
    ) -> Result<()> {
        for event in events.drain(..) {
            Self::apply_event(&event, provider, filesystem, heap.as_mut())?;

            // Push the new event. Clobber the re-do part of the buffer, if we undid to get here.
            redo_stack.clear();
            undo_stack.push(event);
        }
        Ok(())
    }

    fn apply_event(
        event: &FormEvent,
        provider: &mut dyn DocumentEditProvider,
        filesystem: (&GameInstallation, &AssetCollection),
        mut heap: HeapMut,
    ) -> Result<()> {
        let next = event.next();
        let maybe_field = provider.get_field_ref(event.name());
        if maybe_field.is_err() {
            provider.apply_event(event, filesystem, heap.as_mut())?;
            return Ok(());
        }
        match maybe_field? {
            FieldRef::Line(s) => s.replace_with(next.string()?),
            FieldRef::DosFilename(s) => s.replace_with(next.string()?),
            FieldRef::MaybeLine(s) => s.clone_from(next.maybe_string()?),
            FieldRef::F64 { value, .. } => *value = next.f64()?,
            FieldRef::U8(n) => *n = next.u8()?,
            FieldRef::U16(n) => *n = next.u16()?,
            FieldRef::U32(n) => *n = next.u32()?,
            FieldRef::I16(n) => *n = next.i16()?,
            FieldRef::I32(n) => *n = next.i32()?,
            FieldRef::Checkbox(b) => *b = next.bool()?,
            FieldRef::CheckBit32(flag, offset) => {
                if next.bool()? {
                    *flag |= 1 << offset;
                } else {
                    *flag &= !(1 << offset);
                }
            }
            FieldRef::DropdownU8 { value, .. } => {
                *value = event.next().u8()?;
            }
            FieldRef::DropdownNullableExtensionSearch { value, .. } => {
                value.clone_from(next.maybe_string()?);
            }
            FieldRef::DropdownEnumU8 { .. } => {
                provider.apply_event(event, filesystem, heap.as_mut())?
            }
            FieldRef::DropdownEnumU16 { .. } => {
                provider.apply_event(event, filesystem, heap.as_mut())?
            }
            FieldRef::Tab(_) => provider.apply_event(event, filesystem, heap.as_mut())?,
            FieldRef::ToolboxEnumU8 { .. } => {
                provider.apply_event(event, filesystem, heap.as_mut())?
            }
        }
        Ok(())
    }

    fn draw_document_edit(
        &mut self,
        ui: &mut egui::Ui,
        installs: &Installations,
        catalog: &mut AssetCatalog,
        mut heap: HeapMut,
    ) -> Result<()> {
        // Note: because we need both self and self.state as mut here, doing the borrow requires
        //       a bit of extra work to inform rust that we are still being safe.
        let provider = if let EditState::EditDocument(provider) = &mut self.state {
            provider.as_mut()
        } else {
            panic!("entered draw_document_importer in wrong state");
        };

        let widgets = provider.get_edit_widgets(heap.as_mut())?;
        let mut events = smallvec![];
        for widget in &widgets {
            widget.draw(ui, (installs.primary(), catalog.primary()), &mut events)?;
        }
        Self::apply_events(
            &mut self.undo_stack,
            &mut self.redo_stack,
            events,
            provider,
            (installs.primary(), catalog.primary()),
            heap.as_mut(),
        )?;
        provider.show_custom_document_edit_ux(ui, heap.as_mut())?;

        // Pass additional context
        provider.note_panel_intersect(ui.rect_contains_pointer(ui.available_rect_before_wrap()));

        // Save/Close Button Bar
        ui.separator();
        // Rust cannot infer that the ui closure is only called once, so
        // we have to pull the write out of line.
        let response = ui
            .horizontal(|ui| -> Result<DialogResponse> {
                if ui.button("Save").clicked() {
                    let files = provider.serialize(heap.as_mut())?;
                    return Ok(DialogResponse::Save(files));
                }
                if ui.button("Close").clicked() {
                    return Ok(DialogResponse::Close);
                }
                Ok(DialogResponse::None)
            })
            .inner?;

        match response {
            DialogResponse::None => {}
            DialogResponse::Close => {
                let herder: &mut ScriptHerder = &mut heap.resource_mut::<ScriptHerder>();
                self.close_document_edit(herder)?;
            }
            DialogResponse::Save(files) => {
                for (name, content) in files {
                    catalog
                        .primary_mut()
                        .provider_mut(&self.lib_name)?
                        .write(&name, &content)?;
                    if let Ok(res) = catalog.lookup(&name) {
                        heap.resource::<TypeManager>().unload(res).ok();
                    }
                }
                // FIXME: shape unload?
            }
        };

        Ok(())
    }

    fn close_document_edit<S: Scriptable>(&mut self, herder: &mut S) -> Result<()> {
        ensure!(matches!(self.state, EditState::EditDocument(_)));
        // Set up the "leave" state and queue a call so we can free Heap resources.
        self.leaving_state = EditState::Waiting;
        std::mem::swap(&mut self.leaving_state, &mut self.state);
        herder.run("@ux.edit_lib_ux.leave_document_edit()");
        Ok(())
    }

    #[method]
    fn enter_document_edit(&mut self, mut heap: HeapMut) -> Result<()> {
        if let EditState::EditDocument(edit) = &mut self.state {
            // Tag ourselves
            heap.resource_mut::<EventMapper>()
                .set_input_focus(InputFocus::new(
                    UxState::EditLib.as_str().to_owned() + ":" + edit.plugin(),
                ));

            edit.enter_document_edit(heap)?;
        }
        Ok(())
    }

    #[method]
    fn leave_document_edit(&mut self, mut heap: HeapMut) -> Result<()> {
        if let EditState::EditDocument(edit) = &mut self.leaving_state {
            // Untag ourselves
            heap.resource_mut::<EventMapper>()
                .set_input_focus(UxState::EditLib.focus());

            edit.leave_document_edit(heap)?;
        }
        self.leaving_state = EditState::Waiting;
        Ok(())
    }

    #[method]
    fn mouse_motion(
        &mut self,
        cursor_x: f64,
        cursor_y: f64,
        shift_pressed: bool,
        mut heap: HeapMut,
    ) -> Result<()> {
        if let EditState::EditDocument(edit) = &mut self.state {
            let events = edit.mouse_motion(cursor_x, cursor_y, shift_pressed, heap.as_mut())?;
            heap.resource_scope(|mut heap: HeapMut, installs: Mut<Installations>| {
                heap.resource_scope(|heap: HeapMut, catalog: Mut<AssetCatalog>| {
                    Self::apply_events(
                        &mut self.undo_stack,
                        &mut self.redo_stack,
                        events,
                        edit.as_mut(),
                        (installs.primary(), catalog.primary()),
                        heap,
                    )
                })
            })?;
        }
        Ok(())
    }

    #[method]
    fn mouse_press(&mut self, pressed: bool, shift_pressed: bool, mut heap: HeapMut) -> Result<()> {
        if let EditState::EditDocument(edit) = &mut self.state {
            let events = edit.mouse_press(pressed, shift_pressed, heap.as_mut())?;
            heap.resource_scope(|mut heap: HeapMut, installs: Mut<Installations>| {
                heap.resource_scope(|heap: HeapMut, catalog: Mut<AssetCatalog>| {
                    Self::apply_events(
                        &mut self.undo_stack,
                        &mut self.redo_stack,
                        events,
                        edit.as_mut(),
                        (installs.primary(), catalog.primary()),
                        heap,
                    )
                })
            })?;
        }
        Ok(())
    }
}
