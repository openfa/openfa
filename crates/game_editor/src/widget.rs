// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{edit_sh::co_vertex::CoVertex, DocumentEditProvider};
use anyhow::{bail, Result};
use atlas::Frame as AtlasFrame;
use catalog::{AssetCollection, FileSystem, Search};
use egui::{Align2, Color32};
use image::{Pixel, Rgba};
use installations::GameInstallation;
use itertools::Itertools;
use smallvec::SmallVec;
use std::{
    ops::RangeInclusive,
    time::{Duration, Instant},
};
use uuid::Uuid;

// Widget support two separate data access patterns. The first and
// preferred where possible is to implement get_field_ref and return
// mutable references to the data being displayed. This requires only
// the one reference site to the data.
//
// Unfortunately Bevy's World type's resource_scope lifetimes are
// limited to the closure (probably for good reason!), so we cannot
// return mutable refs that escape the closure, so we can't use our
// normal system for data stored in the Heap. One option would be
// to live in the scope and take heap refs to everything we may need
// to edit, but this would require us to pass everyone's internal
// heap details through all of our generic functions. The second
// alternative is to deref and pass the data manually when creating
// the fields and then implement apply_event to handle updates.
// This requires two refs to the data in places that work on the heap
// but isolates the internal details.

#[derive(Debug)]
pub enum Widget {
    Tabs(TabSet),
    H1(String),
    Form(Form),
}

impl Widget {
    pub fn draw(
        &self,
        ui: &mut egui::Ui,
        (install, collection): (&GameInstallation, &AssetCollection),
        events: &mut SmallVec<[FormEvent; 2]>,
    ) -> Result<()> {
        match self {
            Self::Tabs(tabs) => tabs.draw(ui, (install, collection), events),
            Self::Form(form) => form.draw(ui, (install, collection), events),
            Self::H1(label) => {
                ui.heading(label);
                ui.separator();
                Ok(())
            }
        }
    }
}

#[derive(Debug)]
pub struct TabSet {
    name: String,
    selected: &'static str,
    tabs: Vec<&'static str>,
}

impl TabSet {
    pub fn from_iter<S: Into<String>>(
        name: S,
        selected: &'static str,
        iter: impl Iterator<Item = &'static str>,
    ) -> Self {
        Self {
            name: name.into(),
            selected,
            tabs: iter.collect(),
        }
    }

    pub fn draw(
        &self,
        ui: &mut egui::Ui,
        (_install, _collection): (&GameInstallation, &AssetCollection),
        events: &mut SmallVec<[FormEvent; 2]>,
    ) -> Result<()> {
        egui::Grid::new(&self.name)
            .num_columns(self.tabs.len())
            .spacing([4.0, 4.0])
            .striped(false)
            .show(ui, |ui| -> Result<()> {
                for &tab in &self.tabs {
                    if tab == self.selected {
                        ui.button(tab).highlight();
                    } else if ui.button(tab).clicked() {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::String(self.selected.to_owned()),
                            FieldValue::String(tab.to_owned()),
                        ));
                    }
                }
                Ok(())
            });
        ui.separator();
        Ok(())
    }
}

#[derive(Debug)]
pub struct Form {
    name: String,
    fields: Vec<FormField>,
}

impl Form {
    pub fn from_table<S: Into<String>>(name: S, fields: Vec<FormField>) -> Self {
        Self {
            name: name.into(),
            fields,
        }
    }

    pub fn draw(
        &self,
        ui: &mut egui::Ui,
        (install, collection): (&GameInstallation, &AssetCollection),
        events: &mut SmallVec<[FormEvent; 2]>,
    ) -> Result<()> {
        egui::Grid::new(format!("form_grid_{}", self.name))
            .num_columns(2)
            .spacing([10.0, 4.0])
            .striped(true)
            .show(ui, |ui| -> Result<()> {
                for field in &self.fields {
                    field.draw(ui, (install, collection), events)?;
                }
                Ok(())
            })
            .inner?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct FormField {
    name: String,
    label: Option<String>,
    heading: Option<usize>,
    hover_text: Option<String>,
    active: bool,
    value: FieldDisplay,
}

impl FormField {
    const PAL_BUTTON_SIZE: [f32; 2] = [24., 24.];

    pub fn new_missing<S: Into<String>>(name: S) -> Result<Self> {
        let name = name.into();
        Ok(Self {
            name,
            label: None,
            heading: None,
            hover_text: None,
            active: true,
            value: FieldDisplay::Missing,
        })
    }

    pub fn new<S: Into<String>>(name: S, provider: &mut dyn DocumentEditProvider) -> Result<Self> {
        let name = name.into();
        let value = provider.get_field_ref(&name)?.into_display();
        Self::new_missing(name).map(|mut v| {
            v.value = value;
            v
        })
    }

    pub fn new_h1<S: Into<String>>(name: S) -> Result<Self> {
        Self::new_missing(name).map(|mut v| {
            v.heading = Some(1);
            v
        })
    }

    pub fn new_h2<S: Into<String>>(name: S) -> Result<Self> {
        Self::new_missing(name).map(|mut v| {
            v.heading = Some(2);
            v
        })
    }

    pub fn inactive(mut self) -> Self {
        self.active = false;
        self
    }

    pub fn with_label<S: Into<String>>(mut self, label: S) -> Self {
        self.label = Some(label.into());
        self
    }

    pub fn with_hover_text<S: Into<String>>(mut self, text: S) -> Self {
        self.hover_text = Some(text.into());
        self
    }

    pub fn with_display(mut self, display: FieldDisplay) -> Self {
        self.value = display;
        self
    }

    pub fn draw(
        &self,
        ui: &mut egui::Ui,
        (install, collection): (&GameInstallation, &AssetCollection),
        events: &mut SmallVec<[FormEvent; 2]>,
    ) -> Result<()> {
        fn make_color_button<'a>(clr: Rgba<u8>) -> egui::Button<'a> {
            egui::Button::new(" ")
                .rounding(0.)
                .frame(false)
                .fill(Color32::from_rgb(clr.0[0], clr.0[1], clr.0[2]))
        }
        let label = self.label.as_ref().unwrap_or(&self.name).as_str();
        let hover_text = self.hover_text.as_deref().unwrap_or(label);
        match self.heading {
            None => ui.label(label),
            Some(1) => ui.heading(label),
            Some(2) => ui.strong(label),
            Some(lvl) => bail!("unknown heading level {lvl}"),
        }
        .on_hover_text(hover_text);
        ui.add_enabled_ui(self.active, |ui| -> Result<()> {
            match &self.value {
                FieldDisplay::Missing => {
                    ui.label("").on_hover_text(hover_text);
                }
                FieldDisplay::Line(prior) => {
                    let mut next = prior.clone();
                    if ui
                        .text_edit_singleline(&mut next)
                        .on_hover_text(hover_text)
                        .changed()
                    {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::String(prior.clone()),
                            FieldValue::String(next),
                        ));
                    }
                }
                FieldDisplay::DosFilename(prior) => {
                    let mut next = prior.clone();
                    let te = egui::TextEdit::singleline(&mut next).char_limit(12);
                    if ui.add(te).on_hover_text(hover_text).changed() {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::String(prior.clone()),
                            FieldValue::String(next),
                        ));
                    }
                }
                FieldDisplay::Label(prior) => {
                    ui.label(prior).on_hover_text(hover_text);
                }
                FieldDisplay::MaybeLine(prior) => match prior {
                    None => {
                        if ui.button("Add Value").clicked() {
                            events.push(FormEvent::new(
                                &self.name,
                                FieldValue::MaybeString(None),
                                FieldValue::MaybeString(Some("".to_owned())),
                            ));
                        }
                    }
                    Some(prior) => {
                        let mut next = prior.clone();
                        if ui
                            .text_edit_singleline(&mut next)
                            .on_hover_text(hover_text)
                            .changed()
                        {
                            events.push(FormEvent::new(
                                &self.name,
                                FieldValue::MaybeString(Some(prior.clone())),
                                FieldValue::MaybeString(Some(next)),
                            ));
                        }
                        if ui.button("Remove").clicked() {
                            events.push(FormEvent::new(
                                &self.name,
                                FieldValue::MaybeString(Some(prior.clone())),
                                FieldValue::MaybeString(None),
                            ));
                        }
                    }
                },
                FieldDisplay::F64 {
                    value,
                    speed,
                    range,
                } => {
                    let mut next = *value;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(*speed)
                            .clamp_range(range.to_owned()),
                    )
                    .on_hover_text(hover_text);
                    if next != *value {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::F64(*value),
                            FieldValue::F64(next),
                        ));
                    }
                }
                FieldDisplay::U8(prior) => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(u8::MIN..=u8::MAX),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U8(*prior),
                            FieldValue::U8(next),
                        ));
                    }
                }
                FieldDisplay::U16(prior) => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(u16::MIN..=u16::MAX),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U16(*prior),
                            FieldValue::U16(next),
                        ));
                    }
                }
                FieldDisplay::U32(prior) => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(u32::MIN..=u32::MAX),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U32(*prior),
                            FieldValue::U32(next),
                        ));
                    }
                }
                FieldDisplay::U32Clamped {
                    value: prior,
                    range,
                } => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(range.to_owned()),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U32(*prior),
                            FieldValue::U32(next),
                        ));
                    }
                }
                FieldDisplay::I16(prior) => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(i16::MIN..=i16::MAX),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::I16(*prior),
                            FieldValue::I16(next),
                        ));
                    }
                }
                FieldDisplay::I32(prior) => {
                    let mut next = *prior;
                    ui.add(
                        egui::DragValue::new(&mut next)
                            .speed(1.)
                            .clamp_range(i32::MIN..=i32::MAX),
                    )
                    .on_hover_text(hover_text);
                    if next != *prior {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::I32(*prior),
                            FieldValue::I32(next),
                        ));
                    }
                }
                FieldDisplay::U16x2(v) => {
                    let mut next = *v;
                    egui::Grid::new(format!("form_u16x2_grid_{}", self.name))
                        .num_columns(2)
                        .striped(false)
                        .show(ui, |ui| -> Result<()> {
                            ui.add(egui::DragValue::new(&mut next[0]).speed(1.));
                            ui.add(egui::DragValue::new(&mut next[1]).speed(1.));
                            ui.end_row();
                            Ok(())
                        });
                    if next != *v {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U16x2(*v),
                            FieldValue::U16x2(next),
                        ));
                    }
                }
                FieldDisplay::U16x2Clamped { value, ranges } => {
                    let mut next = *value;
                    egui::Grid::new(format!("form_u16x2_clamped_grid_{}", self.name))
                        .num_columns(2)
                        .striped(false)
                        .show(ui, |ui| -> Result<()> {
                            ui.add(
                                egui::DragValue::new(&mut next[0])
                                    .speed(1.)
                                    .clamp_range(ranges[0].clone()),
                            );
                            ui.add(
                                egui::DragValue::new(&mut next[1])
                                    .speed(1.)
                                    .clamp_range(ranges[1].clone()),
                            );
                            ui.end_row();
                            Ok(())
                        });
                    if next != *value {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U16x2(*value),
                            FieldValue::U16x2(next),
                        ));
                    }
                }
                FieldDisplay::I8x3(v) => {
                    let mut next = *v;
                    egui::Grid::new(format!("form_i8x3_grid_{}", self.name))
                        .num_columns(3)
                        .striped(false)
                        .show(ui, |ui| -> Result<()> {
                            ui.add(egui::DragValue::new(&mut next[0]).speed(1.));
                            ui.add(egui::DragValue::new(&mut next[1]).speed(1.));
                            ui.add(egui::DragValue::new(&mut next[2]).speed(1.));
                            ui.end_row();
                            Ok(())
                        });
                    if next != *v {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::I8x3(*v),
                            FieldValue::I8x3(next),
                        ));
                    }
                }
                FieldDisplay::I16x3(v) => {
                    let mut next = *v;
                    egui::Grid::new(format!("form_i16x3_grid_{}", self.name))
                        .num_columns(3)
                        .striped(false)
                        .show(ui, |ui| -> Result<()> {
                            ui.add(egui::DragValue::new(&mut next[0]).speed(1.));
                            ui.add(egui::DragValue::new(&mut next[1]).speed(1.));
                            ui.add(egui::DragValue::new(&mut next[2]).speed(1.));
                            ui.end_row();
                            Ok(())
                        });
                    if next != *v {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::I16x3(*v),
                            FieldValue::I16x3(next),
                        ));
                    }
                }
                crate::widget::FieldDisplay::F32x3(v) => {
                    let mut next = *v;
                    egui::Grid::new(format!("form_f32x3_grid_{}", self.name))
                        .num_columns(3)
                        .striped(false)
                        .show(ui, |ui| -> Result<()> {
                            ui.add(egui::DragValue::new(&mut next[0]).speed(0.001));
                            ui.add(egui::DragValue::new(&mut next[1]).speed(0.001));
                            ui.add(egui::DragValue::new(&mut next[2]).speed(0.001));
                            ui.end_row();
                            Ok(())
                        });
                    if next != *v {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::F32x3(*v),
                            FieldValue::F32x3(next),
                        ));
                    }
                }
                FieldDisplay::Button { prior, next } => {
                    if ui.button(label).on_hover_text(hover_text).clicked() {
                        events.push(FormEvent::new(
                            &self.name,
                            prior.to_owned(),
                            next.to_owned(),
                        ))
                    }
                }
                FieldDisplay::RadioButton(b) => {
                    if ui.radio(*b, "").on_hover_text(hover_text).clicked() {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::Boolean(*b),
                            FieldValue::Boolean(true),
                        ));
                    }
                }
                FieldDisplay::Checkbox(b) => {
                    let mut value = *b;
                    ui.checkbox(&mut value, "").on_hover_text(hover_text);
                    if value != *b {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::Boolean(*b),
                            FieldValue::Boolean(value),
                        ));
                    }
                }
                FieldDisplay::PaletteColorButton(color) => {
                    let clr = install.palette().rgba(*color as usize);
                    let btn = make_color_button(clr);
                    if ui
                        .add_sized(Self::PAL_BUTTON_SIZE, btn)
                        .on_hover_text(hover_text)
                        .clicked()
                    {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::None,
                            FieldValue::None,
                        ));
                    }
                    let mut new_color = *color;
                    ui.add(egui::DragValue::new(&mut new_color));
                    if new_color != *color {
                        events.push(FormEvent::new(
                            &self.name,
                            FieldValue::U8(*color),
                            FieldValue::U8(new_color),
                        ));
                    }
                    ui.label(format!(
                        "{:02X} (#{:02X}{:02X}{:02X})",
                        *color, clr.0[0], clr.0[1], clr.0[2]
                    ));
                }
                FieldDisplay::PaletteColorSelector(color) => {
                    let pos = ui.next_widget_position();
                    let pal_size = [
                        Self::PAL_BUTTON_SIZE[0] * 16. + 16.,
                        Self::PAL_BUTTON_SIZE[1] * 16. + 16.,
                    ];
                    egui::Window::new("Color Picker")
                        .default_size(pal_size)
                        .min_size(pal_size)
                        .title_bar(false)
                        .anchor(Align2::RIGHT_TOP, [-pal_size[0], pos.y])
                        .show(ui.ctx(), |ui| {
                            let mut hover_color = None;
                            egui::Grid::new(format!(
                                "form_palette_color_selector_outter_{}",
                                self.name
                            ))
                            .num_columns(1)
                            .striped(false)
                            .show(ui, |ui| -> Result<()> {
                                egui::Grid::new(format!(
                                    "form_palette_color_selector_{}",
                                    self.name
                                ))
                                .num_columns(16)
                                .spacing([0., 0.])
                                .max_col_width(8.)
                                .min_col_width(8.)
                                .striped(false)
                                .show(ui, |ui| -> Result<()> {
                                    for row in 0..16 {
                                        for col in 0..16 {
                                            let pcolor = row * 16 + col;
                                            let clr = install.palette().rgba(pcolor);
                                            let btn = make_color_button(clr);
                                            if pcolor as u8 == *color {
                                                let border_color = if clr.to_luma().0[0] > 128 {
                                                    Color32::BLACK
                                                } else {
                                                    Color32::WHITE
                                                };
                                                ui.add_sized(
                                                    Self::PAL_BUTTON_SIZE,
                                                    btn.stroke((2., border_color)),
                                                );
                                            } else {
                                                let resp = ui.add_sized(Self::PAL_BUTTON_SIZE, btn);
                                                if resp.clicked() {
                                                    events.push(FormEvent::new(
                                                        &self.name,
                                                        FieldValue::U8(*color),
                                                        FieldValue::U8(pcolor as u8),
                                                    ));
                                                }
                                                if resp.hovered() {
                                                    hover_color = Some(pcolor as u8);
                                                }
                                            }
                                        }
                                        ui.end_row();
                                    }
                                    Ok(())
                                });
                                ui.end_row();
                                let clr = install.palette().rgba(*color as usize);
                                ui.label(format!(
                                    "Current: {:02X} (#{:02X}{:02X}{:02X})",
                                    *color, clr.0[0], clr.0[1], clr.0[2]
                                ));
                                ui.end_row();
                                if let Some(pcolor) = hover_color {
                                    let clr = install.palette().rgba(pcolor as usize);
                                    ui.label(format!(
                                        "Hovered: {:02X} (#{:02X}{:02X}{:02X})",
                                        pcolor, clr.0[0], clr.0[1], clr.0[2]
                                    ));
                                } else {
                                    ui.label("<none>");
                                }
                                Ok(())
                            });
                        });
                }
                FieldDisplay::NullableExtensionSearch { value, ext } => {
                    let mut next = value.to_owned().unwrap_or_else(String::new);
                    let rv = egui::ComboBox::from_id_source(format!("form_combobox_{}", self.name))
                        .selected_text(&next)
                        .show_ui(ui, |ui| -> Result<()> {
                            ui.style_mut().wrap = Some(false);
                            ui.set_min_width(60.0);
                            ui.selectable_value(&mut next, "None".to_owned(), "None");
                            for info in collection.search(Search::for_extension(ext))? {
                                ui.selectable_value(&mut next, info.name().to_owned(), info.name());
                            }
                            let next = if &next == "None" { None } else { Some(next) };
                            if value != &next {
                                events.push(FormEvent::new(
                                    &self.name,
                                    FieldValue::MaybeString(value.to_owned()),
                                    FieldValue::MaybeString(next),
                                ));
                            }
                            Ok(())
                        })
                        .inner;
                    // We only get a result from our closure if the dropdown is dropped.
                    if let Some(rv) = rv {
                        rv?;
                    }
                }
                FieldDisplay::DropdownU8 {
                    value,
                    options,
                    labels,
                } => {
                    let mut next = *value;
                    let (index, _) = options
                        .iter()
                        .find_position(|&v| *v == next)
                        .expect("value not in map");
                    let rv =
                        egui::ComboBox::from_id_source(format!("form_dropdown_u8_{}", self.name))
                            .selected_text(labels[index])
                            .show_ui(ui, |ui| -> Result<()> {
                                for (opt, &label) in options.iter().zip(labels.iter()) {
                                    ui.selectable_value(&mut next, *opt, label);
                                }
                                if *value != next {
                                    events.push(FormEvent::new(
                                        &self.name,
                                        FieldValue::U8(*value),
                                        FieldValue::U8(next),
                                    ));
                                }
                                Ok(())
                            })
                            .inner;
                    if let Some(rv) = rv {
                        rv?;
                    }
                }
                FieldDisplay::ToolboxU8 {
                    value,
                    options,
                    labels,
                } => {
                    for (option, label) in options.iter().zip(labels.iter()) {
                        let btn = if *value == *option {
                            ui.button(*label).highlight()
                        } else {
                            ui.button(*label)
                        };
                        if btn.clicked() {
                            events.push(FormEvent::new(
                                &self.name,
                                FieldValue::U8(*value),
                                FieldValue::U8(*option),
                            ));
                        }
                    }
                }
                FieldDisplay::DropdownU16 {
                    value,
                    options,
                    labels,
                } => {
                    let mut next = *value;
                    let (index, _) = options
                        .iter()
                        .find_position(|&v| *v == next)
                        .expect("value not in map");
                    let rv =
                        egui::ComboBox::from_id_source(format!("form_dropdown_u16_{}", self.name))
                            .selected_text(labels[index])
                            .show_ui(ui, |ui| -> Result<()> {
                                for (opt, &label) in options.iter().zip(labels.iter()) {
                                    ui.selectable_value(&mut next, *opt, label);
                                }
                                if *value != next {
                                    events.push(FormEvent::new(
                                        &self.name,
                                        FieldValue::U16(*value),
                                        FieldValue::U16(next),
                                    ));
                                }
                                Ok(())
                            })
                            .inner;
                    if let Some(rv) = rv {
                        rv?;
                    }
                }
                FieldDisplay::DropdownString { value, options } => {
                    let mut next = value.clone();
                    let rv = egui::ComboBox::from_id_source(format!(
                        "form_dropdown_string_{}",
                        self.name
                    ))
                    .selected_text(value)
                    .show_ui(ui, |ui| -> Result<()> {
                        for opt in options.iter() {
                            ui.selectable_value(&mut next, opt.clone(), opt.clone());
                        }
                        if *value != next {
                            events.push(FormEvent::new(
                                &self.name,
                                FieldValue::String(value.clone()),
                                FieldValue::String(next),
                            ));
                        }
                        Ok(())
                    })
                    .inner;
                    if let Some(rv) = rv {
                        rv?;
                    }
                }
            }
            Ok(())
        })
        .inner?;
        ui.end_row();
        Ok(())
    }
}

/// A mutable reference to a field, with display type, if such is possible, otherwise
/// just the display value.
#[derive(Debug)]
pub enum FieldRef<'a> {
    Tab(String),
    Line(&'a mut String),
    DosFilename(&'a mut String),
    MaybeLine(&'a mut Option<String>),
    F64 {
        value: &'a mut f64,
        speed: f64,
        range: RangeInclusive<f64>,
    },
    U8(&'a mut u8),
    U16(&'a mut u16),
    U32(&'a mut u32),
    I16(&'a mut i16),
    I32(&'a mut i32),
    Checkbox(&'a mut bool),
    CheckBit32(&'a mut u32, u32),
    DropdownU8 {
        value: &'a mut u8,
        options: Vec<u8>,
        labels: Vec<&'static str>,
    },
    ToolboxEnumU8 {
        value: u8,
        options: Vec<u8>,
        labels: Vec<&'static str>,
    },
    DropdownEnumU8 {
        value: u8,
        options: Vec<u8>,
        labels: Vec<&'static str>,
    },
    DropdownEnumU16 {
        value: u16,
        options: Vec<u16>,
        labels: Vec<&'static str>,
    },
    DropdownNullableExtensionSearch {
        value: &'a mut Option<String>,
        ext: &'static str,
    },
    // PaletteColor(&'a mut u8),
}

impl<'a> FieldRef<'a> {
    pub fn into_display(self) -> FieldDisplay {
        match self {
            Self::Tab(_) => FieldDisplay::Missing, // Drawn through Widgets
            Self::Line(s) => FieldDisplay::Line(s.to_owned()),
            Self::DosFilename(s) => FieldDisplay::DosFilename(s.to_owned()),
            Self::MaybeLine(s) => FieldDisplay::MaybeLine(s.to_owned()),
            Self::F64 {
                value,
                speed,
                range,
            } => FieldDisplay::F64 {
                value: *value,
                speed,
                range,
            },
            Self::U8(n) => FieldDisplay::U8(*n),
            Self::U16(n) => FieldDisplay::U16(*n),
            Self::U32(n) => FieldDisplay::U32(*n),
            Self::I16(n) => FieldDisplay::I16(*n),
            Self::I32(n) => FieldDisplay::I32(*n),
            Self::Checkbox(b) => FieldDisplay::Checkbox(*b),
            Self::CheckBit32(flag, offset) => FieldDisplay::Checkbox((*flag >> offset) & 1 == 1),
            Self::DropdownU8 {
                value,
                options,
                labels,
            } => FieldDisplay::DropdownU8 {
                value: *value,
                options,
                labels,
            },
            Self::ToolboxEnumU8 {
                value,
                options,
                labels,
            } => FieldDisplay::ToolboxU8 {
                value,
                options,
                labels,
            },
            Self::DropdownEnumU8 {
                value,
                options,
                labels,
            } => FieldDisplay::DropdownU8 {
                value,
                options,
                labels,
            },
            Self::DropdownEnumU16 {
                value,
                options,
                labels,
            } => FieldDisplay::DropdownU16 {
                value,
                options,
                labels,
            },
            Self::DropdownNullableExtensionSearch { value, ext } => {
                FieldDisplay::NullableExtensionSearch {
                    value: value.to_owned(),
                    ext,
                }
            }
        }
    }
}

/// A clone of field data with display type.
#[derive(Debug)]
pub enum FieldDisplay {
    Missing,
    Line(String),
    DosFilename(String),
    MaybeLine(Option<String>),
    Label(String),
    F64 {
        value: f64,
        speed: f64,
        range: RangeInclusive<f64>,
    },
    U8(u8),
    U16(u16),
    U32(u32),
    U32Clamped {
        value: u32,
        range: RangeInclusive<u32>,
    },
    // I8(i8),
    I16(i16),
    I32(i32),
    U16x2([u16; 2]),
    U16x2Clamped {
        value: [u16; 2],
        ranges: [RangeInclusive<u16>; 2],
    },
    I8x3([i8; 3]),
    I16x3([i16; 3]),
    F32x3([f32; 3]),
    Button {
        prior: FieldValue,
        next: FieldValue,
    },
    RadioButton(bool),
    Checkbox(bool),
    PaletteColorButton(u8),
    PaletteColorSelector(u8),
    NullableExtensionSearch {
        value: Option<String>,
        ext: &'static str,
    },
    DropdownU8 {
        value: u8,
        options: Vec<u8>,
        labels: Vec<&'static str>,
    },
    ToolboxU8 {
        value: u8,
        options: Vec<u8>,
        labels: Vec<&'static str>,
    },
    DropdownU16 {
        value: u16,
        options: Vec<u16>,
        labels: Vec<&'static str>,
    },
    DropdownString {
        value: String,
        options: Vec<String>,
    },
}

/// A low-level field value, without display encoding.
#[derive(Clone, Debug)]
pub enum FieldValue {
    None,
    String(String),
    MaybeString(Option<String>),
    Boolean(bool),
    F64(f64),
    U8(u8),
    U16(u16),
    U32(u32),
    I16(i16),
    I32(i32),
    U16x2([u16; 2]),
    I8x3([i8; 3]),
    I16x3([i16; 3]),
    F32x3([f32; 3]),
    F64x2([f64; 2]),
    F64x3([f64; 3]),
    UuidVec(Vec<Uuid>),
    CoVertVec(Vec<CoVertex>),
    TextureWrite {
        frame_index: usize,
        frame: AtlasFrame,
        st: [u32; 2],
        color: u8,
    },
}

impl FieldValue {
    pub fn string(&self) -> Result<&str> {
        let Self::String(s) = self else {
            bail!("not a string value");
        };
        Ok(s)
    }

    pub fn maybe_string(&self) -> Result<&Option<String>> {
        let Self::MaybeString(s) = self else {
            bail!("not a maybe_string value");
        };
        Ok(s)
    }

    // Radio or Check box value
    pub fn bool(&self) -> Result<bool> {
        let Self::Boolean(b) = self else {
            bail!("not a boolean value");
        };
        Ok(*b)
    }

    pub fn f64(&self) -> Result<f64> {
        let Self::F64(v) = self else {
            bail!("not an f64 value");
        };
        Ok(*v)
    }

    pub fn u8(&self) -> Result<u8> {
        let Self::U8(v) = self else {
            bail!("not a u8 value");
        };
        Ok(*v)
    }

    pub fn u16(&self) -> Result<u16> {
        let Self::U16(v) = self else {
            bail!("not a u16 value");
        };
        Ok(*v)
    }

    pub fn u32(&self) -> Result<u32> {
        let Self::U32(v) = self else {
            bail!("not a u32 value");
        };
        Ok(*v)
    }

    pub fn i16(&self) -> Result<i16> {
        let Self::I16(v) = self else {
            bail!("not a i16 value");
        };
        Ok(*v)
    }

    pub fn i32(&self) -> Result<i32> {
        let Self::I32(v) = self else {
            bail!("not a i32 value");
        };
        Ok(*v)
    }

    pub fn u16x2(&self) -> Result<[u16; 2]> {
        let Self::U16x2(v) = self else {
            bail!("not a u16x2 value");
        };
        Ok(*v)
    }

    pub fn i8x3(&self) -> Result<[i8; 3]> {
        let Self::I8x3(v) = self else {
            bail!("not an i8x3 value");
        };
        Ok(*v)
    }

    pub fn i16x3(&self) -> Result<[i16; 3]> {
        let Self::I16x3(v) = self else {
            bail!("not an i16x3 value");
        };
        Ok(*v)
    }

    pub fn f32x3(&self) -> Result<[f32; 3]> {
        let Self::F32x3(v) = self else {
            bail!("not an f32x3 value");
        };
        Ok(*v)
    }

    pub fn f64x2(&self) -> Result<[f64; 2]> {
        let Self::F64x2(v) = self else {
            bail!("not an f64x2 value");
        };
        Ok(*v)
    }

    pub fn f64x3(&self) -> Result<[f64; 3]> {
        let Self::F64x3(v) = self else {
            bail!("not an f64x3 value");
        };
        Ok(*v)
    }

    pub fn uuid_vec(&self) -> Result<&[Uuid]> {
        let Self::UuidVec(v) = self else {
            bail!("not a Uuid vector");
        };
        Ok(v)
    }

    pub fn co_vert_vec(&self) -> Result<&[CoVertex]> {
        let Self::CoVertVec(v) = self else {
            bail!("not a CoVertex vector");
        };
        Ok(v)
    }
}

#[derive(Debug)]
pub struct FormEvent {
    name: String,
    time: Instant,
    prior: FieldValue,
    next: FieldValue,
}

impl FormEvent {
    pub fn new<S: Into<String>>(name: S, prior: FieldValue, next: FieldValue) -> Self {
        Self {
            name: name.into(),
            time: Instant::now(),
            prior,
            next,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn time(&self) -> Instant {
        self.time
    }

    pub fn next(&self) -> &FieldValue {
        &self.next
    }

    pub fn prior(&self) -> &FieldValue {
        &self.prior
    }

    pub fn with_next(mut self, next: Self) -> Self {
        self.next = next.next;
        self.time = next.time;
        self
    }

    pub fn invert(mut self) -> Self {
        std::mem::swap(&mut self.prior, &mut self.next);
        self
    }

    pub fn duration_since(&self, earlier: &Self) -> Duration {
        self.time.duration_since(earlier.time)
    }
}
