// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
pub(crate) mod co_vertex;

use crate::{
    edit_sh::co_vertex::{CoVertex, VertexRef},
    widget::{FieldDisplay, FieldRef, FieldValue, Form, FormEvent, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument, PanelKind,
};
use absolute_unit::{approx::relative_eq, prelude::*};
use animate::Timeline;
use anyhow::{anyhow, bail, Result};
use arcball::ArcBallController;
use atlas::Frame as AtlasFrame;
use bevy_ecs::prelude::*;
use camera::ScreenCamera;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use egui::FontFamily;
use game_loader::GameLoader;
use geodesy::{Bearing, Geodetic, PitchCline};
use geometry::intersect::sphere_vs_ray;
use geometry::{algorithm::compute_normal, intersect::ray_vs_triangle, Arrow, Plane, Ray, Sphere};
use glam::DVec3;
use image::{DynamicImage, GenericImage};
use installations::GameInstallation;
use itertools::Itertools;
use mantle::Gpu;
use marker::Markers;
use nitrous::{HeapMut, HeapRef, Value};
use ordered_float::OrderedFloat;
use orrery::Orrery;
use pal::Palette;
use phase::{Frame, MapFrame};
use pic::{Pic, PicPalette};
use planck::{rgb, Color};
use runtime::PlayerMarker;
use sh::{
    fa_face_norm_to_vec, fa_pos_to_pt3, iFC_Face, pt3_to_fa_pos, DrawSelection, FaceContentFlags,
    FaceLayoutFlags, GearFootprintKind, InstrDetail, ShCode, ShExtent, ShInstr,
};
use shape::{Shape, ShapeId, ShapeIds, ShapeLoadPackage, ShapeVertex};
use smallvec::{smallvec, SmallVec};
use std::{
    collections::{BTreeMap, HashMap, HashSet},
    sync::Arc,
    time::Duration,
};
use uuid::Uuid;
use vehicle::{
    AirbrakeControl, AirbrakeEffector, Airframe, BayControl, BayEffector, FlapsControl,
    FlapsEffector, FuelSystem, FuelTank, FuelTankKind, GearControl, GearEffector, GliderEngine,
    HookControl, HookEffector, PitchInceptor, PowerSystem, RollInceptor, ThrottleInceptor,
    ThrustVectorPitchControl, ThrustVectorPitchEffector, ThrustVectorYawControl,
    ThrustVectorYawEffector, VtolAngleControl, VtolAngleEffector, YawInceptor,
};

const HELP_TEXT: &str = r"Key Bindings:
F1 / ?       - Show/hide this help screen
Escape       - Return to shape selector (without saving)
LeftMouse    - Apply the currently selected tool
Shift+Left   - Deselect in selection tools
MiddleMouse  - Rotate the view
Ctrl+Middle  - Change time of day
MouseWheel   - Zoom in and out
RightMouse   - Move the view
Ctrl+z       - Undo
Ctrl+Shift+z - Redo
1-5          - Military thrust level
6            - Enable afterburners
b/f/g/h/o    - Toggle airbrake, flaps, gear, etc
Arrow Keys   - Move elevator and ailerons
,/.          - Move rudder
Ctrl+Arrows  - Move thrust vectoring
z/x/0        - Move VTOL vectoring
";

const LATITUDE: f64 = 58.287_f64;
const LONGITUDE: f64 = 25.641_f64;
const ALTITUDE: f64 = 20_000.0_f64;

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum EditSelection {
    #[default]
    Normal,
    Damage(usize),
}

impl EditSelection {
    fn ordinal(&self) -> u8 {
        match self {
            Self::Normal => 0,
            Self::Damage(i) => (i + 1) as u8,
        }
    }

    fn options(shape_ids: &ShapeIds) -> Vec<u8> {
        let mut out = vec![0];
        for i in 0u8..shape_ids.damage().len() as u8 {
            out.push(i + 1);
        }
        out
    }

    pub fn labels(shape_ids: &ShapeIds) -> Vec<&'static str> {
        let mut out = vec!["Normal"];
        for i in 0..shape_ids.damage().len() {
            out.push(match i {
                0 => {
                    if shape_ids.damage().len() == 1 {
                        "Damage"
                    } else {
                        "Damage 1"
                    }
                }
                1 => "Damage 2",
                2 => "Damage 3",
                3 => "Damage 4",
                _ => unreachable!("too many damage models"),
            });
        }
        out
    }
}

impl TryFrom<u8> for EditSelection {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            0 => Self::Normal,
            1 => Self::Damage(0),
            2 => Self::Damage(1),
            3 => Self::Damage(2),
            4 => Self::Damage(3),
            _ => bail!("unrecognized edit selection"),
        })
    }
}

#[repr(u8)]
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum ShapeTool {
    #[default]
    SelectFace = 0,
    SelectVertex = 1,
    ColorPicker = 2,
    Pencil = 3,
    Scale = 4,
}

impl ShapeTool {
    pub fn options() -> Vec<u8> {
        vec![0, 1, 2, 3, 4]
    }

    pub fn labels() -> Vec<&'static str> {
        vec!["Face", "Vert", "Eyedrop", "Pencil", "Scale"]
    }
}

impl TryFrom<u8> for ShapeTool {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            0 => Self::SelectFace,
            1 => Self::SelectVertex,
            2 => Self::ColorPicker,
            3 => Self::Pencil,
            4 => Self::Scale,
            _ => bail!("unrecognized tool type"),
        })
    }
}

#[repr(u8)]
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum DragState {
    #[default]
    Select = 0,
    MoveX = 1,
    MoveY = 2,
    MoveZ = 3,
}

impl TryFrom<u8> for DragState {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            0 => Self::Select,
            1 => Self::MoveX,
            2 => Self::MoveY,
            3 => Self::MoveZ,
            _ => bail!("not a valid DragState {value}"),
        })
    }
}

// A face and its owned vertices so that we can group move a face independently
// of the co-verts that own it.
#[derive(Clone, Debug)]
pub struct FaceLink {
    // The link to the face instruction
    face: Uuid,
}

impl FaceLink {
    pub fn new(face: Uuid) -> Self {
        Self { face }
    }
}

// A selection is a set of selected CoVerts and the set of all faces for which all
// VertexRefs are selected via CoVertex (e.g. a fully-circled face).
#[derive(Debug, Default)]
pub struct ShapeSelection {
    vertices: Vec<CoVertex>,
    faces: Vec<FaceLink>,
}

#[derive(Clone, Debug, Default)]
pub struct ShapeEdit;

impl EditableDocument for ShapeEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<ShapeImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, _ui: &mut egui::Ui) -> Option<EditState> {
        None
    }
    fn graphics_menu(&self, ui: &mut egui::Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn waiting_menu(&self, ui: &mut egui::Ui) -> Option<EditState> {
        if ui.button(".SH [Shape]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut egui::Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".SH") && ui.button("Edit").clicked() {
            return Ok(Some(
                ShapeImporter::default().import(info, install, collection, heap)?,
            ));
        }
        Ok(None)
    }
}

#[derive(Debug, Default)]
pub struct ShapeImporter {
    cache: HashMap<String, ShCode>,
}

impl DocumentImportProvider for ShapeImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("SH"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec![
            "Name", "# Instr", "Damage", "Anim", "AB", "Bay", "Gear", "Wing", "Flaps", "TV", "VT"
        ]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        _heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let code = self.cache.entry(info.to_string()).or_insert_with(|| {
            ShCode::from_bytes(&info.data().unwrap(), Some(GearFootprintKind::Nose)).unwrap()
        });
        let caps = code.analysis(DrawSelection::NormalModel)?.capabilities();
        fn two_opt(b: bool) -> String {
            match b {
                false => "",
                true => "✔",
            }
            .to_owned()
        }
        fn three_opt(toggle: bool, anim: bool) -> String {
            match toggle {
                false => "",
                true => match anim {
                    false => "✔",
                    true => "▶",
                },
            }
            .to_owned()
        }
        Ok(smallvec![
            info.name().to_owned(),
            code.code().len().to_string(),
            two_opt(caps.has_damage_model()),
            two_opt(caps.has_animation()),
            two_opt(caps.has_afterburner()),
            three_opt(caps.has_bay(), caps.has_dynamic_bay()),
            three_opt(caps.has_gear(), caps.has_dynamic_gear()),
            two_opt(caps.has_dynamic_wing()),
            two_opt(caps.has_flaps()),
            two_opt(caps.has_dynamic_tv()),
            two_opt(caps.has_dynamic_vt()),
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        mut heap: HeapMut,
    ) -> Result<EditState> {
        heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
            Ok(EditState::EditDocument(Box::new(ShapeEditState::new(
                info,
                install,
                collection,
                heap.resource::<Gpu>(),
                &mut shape,
            )?)
                as Box<dyn DocumentEditProvider>))
        })
    }
}

#[derive(Debug)]
pub struct TextureLink {
    name: String,
    uuid: Uuid,
    image: DynamicImage,
    data: Vec<u8>,
    palette: Palette,
}

impl TextureLink {
    pub fn uuid(&self) -> &Uuid {
        &self.uuid
    }

    pub fn set_name<S: ToString>(&mut self, name: S) {
        self.name = name.to_string();
    }

    pub fn pick_palettized_color(&self, st: [u32; 2]) -> u8 {
        self.data[(st[0] + self.image.width() * st[1]) as usize]
    }

    pub fn put_palettized_color(&mut self, st: [u32; 2], color: u8) {
        self.data[(st[0] + self.image.width() * st[1]) as usize] = color;
        self.image
            .put_pixel(st[0], st[1], self.palette.rgba(color as usize));
    }
}

#[derive(Debug)]
pub(crate) struct ShapeEditState {
    // Core data
    name: String,
    shape_ids: ShapeIds,

    // For unwinding
    entity: Entity,
    init_position: MapFrame,
    init_distance: Length<Meters>,

    // UX state
    showing_help: bool,
    tool: ShapeTool,
    texture: HashMap<ShapeId, TextureLink>,

    // Selection state
    edit_selection: EditSelection,
    selection_color: Color,
    selection_line_size: f32,
    selection: ShapeSelection,
    selecting_palette_color: bool,
    selection_press_start: Option<[f64; 2]>,
    selection_rectangle: Option<[Pt3<Meters>; 4]>,

    // Eyedropper / color state
    colorpicker_color: u8,
    colorpicker_open: bool,
    pencil_size: u32,

    // Drag state: recompute positions from the drag start every frame to avoid janky relative movement
    drag_state: DragState,
    drag_start_cursor: [f64; 2],
    drag_start_position: Vec<CoVertex>,
    move_faces: bool,

    // Scale state
    scale_target: [f64; 3],

    // Draw state
    cursor: [f64; 2],
    in_panel: bool,
    frame: Frame,
    extent: ShExtent,
}

impl DocumentEditProvider for ShapeEditState {
    fn plugin(&self) -> &'static str {
        "SH"
    }

    fn panel_requirements(&self) -> PanelKind {
        PanelKind::Side
    }

    fn note_panel_intersect(&mut self, in_panel: bool) {
        self.in_panel = in_panel;
    }

    fn toggle_show_help(&mut self) {
        self.showing_help = !self.showing_help;
    }

    fn show_custom_document_edit_ux(&mut self, ui: &mut egui::Ui, mut heap: HeapMut) -> Result<()> {
        if self.showing_help {
            self.draw_help(ui);
        }

        heap.resource_scope(|mut heap: HeapMut, shape: Mut<Shape>| {
            heap.resource_scope(|heap: HeapMut, mut markers: Mut<Markers>| {
                self.show_edit_ui(&shape, &mut markers, heap)
            })
        })
    }

    fn get_edit_widgets(&mut self, heap: HeapMut) -> Result<Vec<Widget>> {
        // Common fields shared by all tools
        let mut fields = vec![];
        fields.push(FormField::new("display_name", self)?.with_label("Name"));
        if !self.shape_ids.damage().is_empty() {
            fields.push(
                FormField::new("shape_selection", self)?
                    .with_label("Model")
                    .with_hover_text("Choose which model to edit"),
            );
        }
        if let Some(name) = self.maybe_texture().map(|tl| tl.name.clone()) {
            fields.push(
                FormField::new_missing("texture_name")?
                    .with_label("Texture Name")
                    .with_display(FieldDisplay::DosFilename(name)),
            );
        }
        fields.push(
            FormField::new("toolbox", self)?
                .with_label("Tool")
                .with_hover_text("Select the current tool"),
        );
        match self.tool {
            ShapeTool::SelectFace | ShapeTool::SelectVertex => {
                match self.selection.faces.len() {
                    0 => {}
                    1 => {
                        let selection = &self.selection.faces[0].face;
                        let shape = heap.resource::<Shape>();
                        fields
                            .push(FormField::new_h2("_face_edit_heading")?.with_label("Edit Face"));
                        self.face_edit_color(selection, shape, &mut fields)?;
                        self.face_edit_texcoords(selection, shape, &mut fields)?;
                        self.face_edit_normal(selection, shape, &mut fields)?;
                        self.face_edit_center(selection, shape, &mut fields)?;
                        self.face_edit_content_flags(selection, shape, &mut fields)?;
                        self.face_edit_xform(selection, shape, heap.as_ref(), &mut fields)?;
                        // Informational
                        self.face_show_layout_flags(selection, shape, &mut fields)?;
                        self.face_show_info(selection, shape, &mut fields)?;
                        self.face_show_instr_info(selection, shape, &mut fields)?;
                    }
                    _ => {
                        let selection = &self.selection.faces[0].face;
                        fields.push(
                            FormField::new_h2("_faces_edit_heading")?.with_label("Edit Face Group"),
                        );
                        self.face_edit_color(selection, heap.resource::<Shape>(), &mut fields)?;
                        self.face_edit_content_flags(
                            selection,
                            heap.resource::<Shape>(),
                            &mut fields,
                        )?;
                    }
                }
                // FIXME: we need a proper mapping from the selection to the normals.
                match self.selection.vertices.len() {
                    0 => {}
                    1 => {
                        // Show the covert position and the position of all individual vertices
                        let selected = &self.selection.vertices[0];
                        // let shape = heap.resource::<Shape>();
                        self.co_vertex_edit_position(
                            0,
                            selected,
                            heap.resource::<Shape>(),
                            &mut fields,
                        )?;
                        let mut visited = HashSet::new();
                        for (i, vert_ref) in selected.vert_refs().enumerate() {
                            if visited.insert((vert_ref.vxbuf_uuid(), vert_ref.vxbuf_offset())) {
                                self.vert_ref_edit_position(
                                    i,
                                    vert_ref,
                                    heap.resource::<Shape>(),
                                    &mut fields,
                                )?;
                            }
                        }
                    }
                    2..=10 => {
                        // Show just the covert positions if too many selected
                        for (i, co_vert) in self.selection.vertices.iter().enumerate() {
                            fields.push(FormField::new_h2(format!(
                                "{} Co-located Vertices",
                                co_vert.vert_refs().count()
                            ))?);
                            self.co_vertex_edit_position(
                                i,
                                co_vert,
                                heap.resource::<Shape>(),
                                &mut fields,
                            )?;
                        }
                    }
                    _ => {}
                }
            }
            ShapeTool::ColorPicker => {
                fields.push(
                    FormField::new_missing("texture_eyedrop_display")?
                        .with_label("Picked")
                        .with_hover_text("Select to show the clicked-on texture color")
                        .with_display(FieldDisplay::PaletteColorButton(self.colorpicker_color)),
                );
            }
            ShapeTool::Pencil => {
                if self.colorpicker_open {
                    fields.push(
                        FormField::new_missing("texture_pick_color")?
                            .with_label("Color")
                            .with_hover_text("Select a color to draw with")
                            .with_display(FieldDisplay::PaletteColorSelector(
                                self.colorpicker_color,
                            )),
                    );
                } else {
                    fields.push(
                        FormField::new_missing("texture_pick_color_open")?
                            .with_label("Picked")
                            .with_hover_text("Select to show the clicked-on texture color")
                            .with_display(FieldDisplay::PaletteColorButton(self.colorpicker_color)),
                    );
                }
                fields.push(
                    FormField::new_missing("texture_pencil_size")?
                        .with_label("Color")
                        .with_hover_text("Select a color to draw with")
                        .with_display(FieldDisplay::U32Clamped {
                            value: self.pencil_size,
                            range: 1..=16,
                        }),
                );
            }
            ShapeTool::Scale => {
                fields.push(FormField::new_h2("scale_warning")?
                    .with_label("Warning")
                    .with_hover_text("Use with caution")
                    .with_display(FieldDisplay::Label("If any shape changes have changed the initial bounding box,\nreload before using this tool as well as after rescaling.".into()))
                );
                fields.push(
                    FormField::new_h2("scale_legend")?
                        .with_label("Legend")
                        .with_hover_text("What do the colors mean?")
                        .with_display(FieldDisplay::Label("Color Meanings:\n  Green Box: Airbrake\n  Orange Box: Afterburner\n  Blue Box: Bay\n  Red Box: Gear\n  Grey Box: Gear Footprint\n  Cyan Box: Hook\n  Magenta Box: Body (* this is what is measured below)".into())),
                );
                // is an aircraft
                let (
                    (label_width, hover_width),
                    (label_length, hover_length),
                    (label_height, hover_height),
                ) = if self.extent.aabb_afterburner().is_finite()
                    || self.extent.aabb_airbrake().is_finite()
                    || self.extent.aabb_bay().is_finite()
                    || self.extent.aabb_gear().is_finite()
                    || self.extent.aabb_hook().is_finite()
                {
                    (
                        ("Wingspan", "Wingspan of the current bounding box"),
                        ("Length", "Tip to tail length of the current bounding box"),
                        ("Height", "Not including the gear, extend bay or brakes"),
                    )
                } else {
                    (
                        ("Width", "Width of the object"),
                        ("Length", "Length of the object"),
                        ("Height", "Height of the object"),
                    )
                };
                fields.push(
                    FormField::new_missing("scale_current_width")?
                        .with_label(label_width)
                        .with_hover_text(hover_width)
                        .with_display(FieldDisplay::Label(format!(
                            "{:.2}",
                            feet!(self.extent.aabb_body().span(0))
                        ))),
                );
                fields.push(
                    FormField::new_missing("scale_current_length")?
                        .with_label(label_length)
                        .with_hover_text(hover_length)
                        .with_display(FieldDisplay::Label(format!(
                            "{:.2}",
                            feet!(self.extent.aabb_body().span(2))
                        ))),
                );
                fields.push(
                    FormField::new_missing("scale_current_height")?
                        .with_label(label_height)
                        .with_hover_text(hover_height)
                        .with_display(FieldDisplay::Label(format!(
                            "{:.2}",
                            feet!(self.extent.aabb_body().span(1))
                        ))),
                );
                fields.push(
                    FormField::new_h2("scale_legend")?
                        .with_label("Target Size")
                        .with_hover_text("What do the colors mean?")
                        .with_display(FieldDisplay::Label(
                            "Fill the boxes below with the actual sizes and hit the button to go"
                                .into(),
                        )),
                );
                fields.push(
                    FormField::new("scale_target_width", self)?
                        .with_label(label_width)
                        .with_hover_text(hover_width),
                );
                fields.push(
                    FormField::new("scale_target_length", self)?
                        .with_label(label_length)
                        .with_hover_text(hover_length),
                );
                fields.push(
                    FormField::new("scale_target_height", self)?
                        .with_label(label_height)
                        .with_hover_text(hover_height),
                );
                fields.push(
                    FormField::new_missing("scale_reset_button")?
                        .with_label("Reset")
                        .with_display(FieldDisplay::Button {
                            prior: FieldValue::F64x3(self.scale_target),
                            next: FieldValue::F64x3([
                                feet!(self.extent.aabb_body().span(0)).f64(),
                                feet!(self.extent.aabb_body().span(1)).f64(),
                                feet!(self.extent.aabb_body().span(2)).f64(),
                            ]),
                        }),
                );
                fields.push(
                    FormField::new_missing("scale_apply_button")?
                        .with_label("Apply")
                        .with_display(FieldDisplay::Button {
                            prior: FieldValue::F64x3([
                                feet!(self.extent.aabb_body().span(0)).f64(),
                                feet!(self.extent.aabb_body().span(1)).f64(),
                                feet!(self.extent.aabb_body().span(2)).f64(),
                            ]),
                            next: FieldValue::F64x3(self.scale_target),
                        }),
                );
            }
        }
        Ok(vec![
            Widget::H1(self.name.clone()),
            Widget::Form(Form::from_table("_display_form", fields)),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        Ok(match name {
            // display area
            "display_name" => FieldRef::DosFilename(&mut self.name),
            "shape_selection" => FieldRef::DropdownEnumU8 {
                value: self.edit_selection.ordinal(),
                options: EditSelection::options(&self.shape_ids),
                labels: EditSelection::labels(&self.shape_ids),
            },
            "toolbox" => FieldRef::ToolboxEnumU8 {
                value: self.tool as u8,
                options: ShapeTool::options(),
                labels: ShapeTool::labels(),
            },
            "scale_target_width" => FieldRef::F64 {
                value: &mut self.scale_target[0],
                speed: 1. / 3.,
                range: (1. / 3.)..=u16::MAX as f64 / 3.,
            },
            "scale_target_length" => FieldRef::F64 {
                value: &mut self.scale_target[2],
                speed: 1. / 3.,
                range: (1. / 3.)..=u16::MAX as f64 / 3.,
            },
            "scale_target_height" => FieldRef::F64 {
                value: &mut self.scale_target[1],
                speed: 1. / 3.,
                range: (1. / 3.)..=u16::MAX as f64 / 3.,
            },
            name => bail!("unknown field name {name}"),
        })
    }

    fn apply_event(
        &mut self,
        event: &FormEvent,
        (install, _collection): (&GameInstallation, &AssetCollection),
        mut heap: HeapMut,
    ) -> Result<()> {
        use FaceContentFlags as FCF;
        match event.name() {
            "shape_selection" => {
                self.edit_selection = EditSelection::try_from(event.next().u8()?)?;
                // Note: normally when we destroy a shape, we destroy the entity and scatter debris
                // as completely new entities (with components cloned off the original). This is a
                // different use case and so we instead perform a shape-transplant on the fly.
                let mut ent = heap.entity_by_id_mut(self.entity)?;
                ent.remove::<ShapeId>()?;
                ent.inject(self.shape_id())?;
            }
            "toolbox" => self.tool = ShapeTool::try_from(event.next().u8()?)?,
            // Selection
            "select_faces" => {
                let selected = event.next().uuid_vec()?;
                self.selection = self.faces_to_selection(selected, heap.resource::<Shape>())?;
            }
            "select_verts" => {
                let selected = event.next().co_vert_vec()?;
                self.selection =
                    self.coverts_to_selection(selected.to_owned(), heap.resource::<Shape>())?;
            }
            // Move handling
            "vertex_move_start_cursor" => {
                self.drag_start_cursor = event.next().f64x2()?;
            }
            "vertex_move_start_position" => {
                event
                    .next()
                    .co_vert_vec()?
                    .clone_into(&mut self.drag_start_position);
            }
            "vertex_move_start_state" => {
                self.drag_state = DragState::try_from(event.next().u8()?)?;
            }
            "move_selected_vertices" => {
                self.update_vertex_positions(event.next().f64x2()?, heap)?
            }
            // Paint
            "texture_name" => {
                self.texture_mut()?.set_name(event.next().string()?);
                let mut shape = heap.resource_mut::<Shape>();
                let InstrDetail::TextureFile(tex_file) = shape
                    .get_code_mut(self.shape_id())
                    .instruction_mut(self.texture()?.uuid())?
                    .detail_mut()
                else {
                    bail!("expected an iE2_TextureFile");
                };
                tex_file.set_filename(event.next().string()?);
            }
            "texture_eyedrop_display" => { /* explicitly empty */ }
            "texture_pick_color_open" => self.colorpicker_open = true,
            "texture_pick_color" => {
                self.colorpicker_color = event.next().u8()?;
                self.colorpicker_open = false;
            }
            "texture_pencil_size" => {
                self.pencil_size = event.next().u32()?;
            }
            "texture_write_color" => {
                let FieldValue::TextureWrite {
                    frame, st, color, ..
                } = event.next()
                else {
                    bail!("not a texture write");
                };
                let shape_id = self.shape_id();
                self.texture_mut()?.put_palettized_color(*st, *color);
                let tex = self.texture()?;
                heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
                    let atlas = shape.get_atlas_mut(shape_id);
                    atlas.overwrite_aligned_image(
                        frame,
                        tex.image.as_rgba8().unwrap(),
                        heap.resource::<Gpu>(),
                    )
                })?;
            }
            // Scale Tool
            "scale_reset_button" => {
                self.scale_target = event.next().f64x3()?;
            }
            "scale_apply_button" => {
                // Use the scale target to rescale the model
                let prior = event.prior().f64x3()?;
                let next = event.next().f64x3()?;
                let scale = [next[0] / prior[0], next[1] / prior[1], next[2] / prior[2]];
                self.handle_rescale_shape(scale, &mut heap.resource_mut::<Shape>())?;
            }
            // Face Edit
            "face_color_toggle" => {
                // We may have moved the drag value
                if let Ok(new_color) = event.next().u8() {
                    self.update_faces_color(new_color, install, heap)?;
                } else {
                    self.selecting_palette_color = true;
                }
            }
            "face_color_selector" => {
                self.selecting_palette_color = false;
                self.update_faces_color(event.next().u8()?, install, heap)?;
            }
            "face_content_flag_FaceContentFlags(UNK1)" => {
                self.update_faces_flag(FCF::UNK1, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(HAVE_FACE_NORMAL)" => {
                self.update_faces_flag(FCF::HAVE_FACE_NORMAL, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(UNK2)" => {
                self.update_faces_flag(FCF::UNK2, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(UNK3)" => {
                self.update_faces_flag(FCF::UNK3, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(UNK4)" => {
                self.update_faces_flag(FCF::UNK4, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(HAVE_TEXCOORDS)" => {
                self.update_faces_flag(FCF::HAVE_TEXCOORDS, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(FILL_BACKGROUND)" => {
                self.update_faces_flag(FCF::FILL_BACKGROUND, event.next().bool()?, heap)?;
            }
            "face_content_flag_FaceContentFlags(UNK5)" => {
                self.update_faces_flag(FCF::UNK5, event.next().bool()?, heap)?;
            }
            "face_normal" => {
                self.update_face_selection(
                    &mut heap.resource_mut::<Shape>(),
                    |face| {
                        face.set_normal(event.next().f32x3()?);
                        Ok(())
                    },
                    |_loop_index, vert| {
                        if !vert.is_vertex_normal() {
                            vert.set_fa_face_normal(&event.next().f32x3()?);
                        }
                        Ok(())
                    },
                )?;
            }
            "face_center" => {
                self.update_face_selection(
                    &mut heap.resource_mut::<Shape>(),
                    |face| {
                        face.set_raw_fa_center(event.next().i16x3()?);
                        Ok(())
                    },
                    |_loop_index, _vert| Ok(()),
                )?;
            }
            name => {
                if let Some(index_str) = name.strip_prefix("face_texcoord_") {
                    let index = index_str.parse::<usize>()?;
                    self.update_face_texcoord(index, event.next().u16x2()?, heap.as_mut())?;
                } else if let Some(index_str) = name.strip_prefix("covert_position_") {
                    let i = index_str.parse::<usize>()?;
                    let shape_id = self.shape_id();
                    let next = event.next().i16x3()?;
                    let load_scale = self.load_scale(heap.resource::<Shape>());
                    if let Some(co_vertex) = self.selection.vertices.get_mut(i) {
                        co_vertex.update_position(
                            next,
                            load_scale,
                            true,
                            true,
                            shape_id,
                            &mut heap.resource_mut::<Shape>(),
                        )?;
                    }
                } else if let Some(index_str) = name.strip_prefix("vert_ref_position_") {
                    let i = index_str.parse::<usize>()?;
                    let selected = self.selection.vertices[0].vert_ref(i);
                    let next = event.next().i16x3()?;
                    let mut shape = heap.resource_mut::<Shape>();
                    let load_scale = self.load_scale(&shape);
                    let vxbuf_instr = shape
                        .get_code_mut(self.shape_id())
                        .instruction_mut(selected.vxbuf_uuid())?;
                    let InstrDetail::VertexBuffer(vxbuf) = vxbuf_instr.detail_mut() else {
                        bail!("expected a vxbuf for vert_ref_position_");
                    };
                    *vxbuf.vertex_mut(selected.vxbuf_offset()) = next;
                    shape.get_vertices_mut(self.shape_id())[selected.vb_offset()]
                        .set_fa_position(&next, load_scale);
                } else {
                    bail!("unknown event {name}")
                };
            }
        }
        Ok(())
    }

    fn serialize(&self, heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>> {
        let sh_code = heap.resource::<Shape>().get_code(self.shape_id());
        let mut out = vec![(self.name.clone(), sh_code.compile_pe()?)];
        for tex in self.texture.values() {
            let buffer = tex.image.as_rgba8().unwrap().to_owned();
            let data = Pic::make_texture(buffer, PicPalette::new_inline(&tex.palette), 1);
            out.push((tex.name.clone(), data));
        }
        Ok(out)
    }

    fn enter_document_edit(&mut self, mut heap: HeapMut) -> Result<()> {
        // Just jump to the target to avoid flickering like crazy if it's too far off.
        // TODO: figure out local times and animate within a day cycle
        heap.resource_mut::<Orrery>()
            .set_date_time(1969, 1, 1, 20, 0, 0);

        let arcball = heap.get_mut::<ArcBallController>("camera")?;
        let init_position = arcball.map_frame().to_owned();
        let init_distance = arcball.distance();
        let tgt_position = MapFrame::new(
            Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
            Bearing::new(degrees!(-113)),
            PitchCline::new(degrees!(28.)),
        );
        let tgt_distance = meters!(40);
        // arcball.animate_to(tgt_position, meters!(40));
        self.init_distance = init_distance;
        self.init_position = init_position.clone();
        // FIXME: use arcball mover
        heap.resource_mut::<Timeline>().ease_in_out_to(
            Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
                let f = v[0].to_float()?;
                let mut arcball = heap.get_mut::<ArcBallController>("camera")?;
                arcball.set_map_frame(init_position.interpolate(&tgt_position, f));
                arcball
                    .set_distance(init_distance + scalar!(f) * (tgt_distance - init_distance))?;
                Ok(Value::True())
            })),
            0.0.into(),
            1.0.into(),
            seconds!(1.5),
        )?;

        // Make sure the Baltic map is loaded.
        heap.resource_scope(|heap, game: Mut<GameLoader>| game.load_map("BAL.MM", heap))?;

        // Load the shape and all associated components using the standard load path.
        let shape_pkt = ShapeLoadPackage::with_context(
            "edit_mm",
            &self.name,
            scalar!(1. / 3.),
            Some(GearFootprintKind::Nose),
            heap.as_mut(),
        )?;

        self.extent = heap
            .resource::<Shape>()
            .analysis(*shape_pkt.shape_id())?
            .extent()
            .clone();

        let fuel = FuelSystem::default()
            .with_internal_tank(FuelTank::new(FuelTankKind::Center, kilograms!(0_f64)))?;
        let power = PowerSystem::default().with_engine(GliderEngine::default());
        self.entity = {
            let mut ent = heap.spawn("Player")?;
            ent.inject((
                PlayerMarker,
                self.frame.clone(),
                Airframe::new(kilograms!(10_f64)),
                fuel,
                power,
                (
                    PitchInceptor::default(),
                    RollInceptor::default(),
                    YawInceptor::default(),
                    ThrottleInceptor::new_min_power(),
                    ThrustVectorPitchControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    ThrustVectorYawControl::new(degrees!(-30), degrees!(30), degrees!(1)),
                    VtolAngleControl::new(degrees!(-100), degrees!(10), degrees!(10)),
                    AirbrakeControl::default(),
                    BayControl::default(),
                    GearControl::default(),
                    FlapsControl::default(),
                    HookControl::default(),
                ),
                (
                    ThrustVectorPitchEffector::new(0., Duration::from_millis(1)),
                    ThrustVectorYawEffector::new(0., Duration::from_millis(1)),
                    VtolAngleEffector::new(0., Duration::from_secs(1)),
                    AirbrakeEffector::new(0., Duration::from_millis(1)),
                    BayEffector::new(0., Duration::from_secs(2)),
                    FlapsEffector::new(0., Duration::from_millis(1)),
                    GearEffector::new(0., Duration::from_secs(4)),
                    HookEffector::new(0., Duration::from_millis(1)),
                ),
            ))?;
            shape_pkt.inject_into(&mut ent)?;
            ent.id()
        };

        Ok(())
    }

    fn leave_document_edit(&self, mut heap: HeapMut) -> Result<()> {
        assert_ne!(self.entity, Entity::PLACEHOLDER);

        // FIXME: use arcball mover
        let arcball = heap.get_mut::<ArcBallController>("camera")?;
        let init_position = arcball.map_frame().to_owned();
        let init_distance = arcball.distance();
        let tgt_position = self.init_position.clone();
        let tgt_distance = self.init_distance;
        heap.resource_mut::<Timeline>().ease_in_out_to(
            Value::RustMethod(Arc::new(move |v: &[Value], mut heap| -> Result<Value> {
                let f = v[0].to_float()?;
                let mut arcball = heap.get_mut::<ArcBallController>("camera")?;
                arcball.set_map_frame(init_position.interpolate(&tgt_position, f));
                arcball
                    .set_distance(init_distance + scalar!(f) * (tgt_distance - init_distance))?;
                Ok(Value::True())
            })),
            0.0.into(),
            1.0.into(),
            seconds!(1.5),
        )?;

        heap.resource_scope(|heap, game: Mut<GameLoader>| game.unload_mission(heap))?;
        heap.despawn_by_id(self.entity);

        Ok(())
    }

    fn mouse_motion(
        &mut self,
        cursor_x: f64,
        cursor_y: f64,
        shift_pressed: bool,
        heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // Origin is top-left corner
        self.cursor = [cursor_x, cursor_y];

        match self.drag_state {
            DragState::MoveX | DragState::MoveY | DragState::MoveZ => {
                Ok(smallvec![FormEvent::new(
                    "move_selected_vertices",
                    FieldValue::F64x2(self.drag_start_cursor),
                    FieldValue::F64x2(self.cursor)
                )])
            }
            DragState::Select => self.region_select_or_paint(shift_pressed, heap),
        }
    }

    fn mouse_press(
        &mut self,
        pressed: bool,
        shift_pressed: bool,
        heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        if !pressed {
            self.selection_press_start = None;
            self.selection_rectangle = None;
            // Note: This is unpress, so we can't start a selection or move so no need to handle.
            // However, we do want to unconditionally end any moves on mouseup.
            return Ok(if self.drag_state != DragState::Select {
                smallvec![FormEvent::new(
                    "vertex_move_start_state",
                    FieldValue::U8(self.drag_state as u8),
                    FieldValue::U8(DragState::Select as u8),
                )]
            } else {
                smallvec![]
            });
        }

        // Do not pass along the click if the click is on the panel
        if self.in_panel {
            return Ok(smallvec![]);
        }

        let camera = heap.resource::<ScreenCamera>();
        let wrld_m = camera.mouse_to_world((self.cursor[0], self.cursor[1]));
        let click_ray = Ray::new(
            camera.position::<Meters>(),
            (wrld_m.dvec3() - camera.position::<Meters>().dvec3()).normalize(),
        );

        let intersect_x = self.intersect_move_x(&click_ray);
        let intersect_y = self.intersect_move_y(&click_ray);
        let intersect_z = self.intersect_move_z(&click_ray);
        if intersect_x || intersect_y || intersect_z {
            // Note: undoing the initial move should always return to move start state,
            //       not the previous move state.
            Ok(smallvec![
                FormEvent::new(
                    "vertex_move_start_cursor",
                    FieldValue::F64x2(self.cursor),
                    FieldValue::F64x2(self.cursor),
                ),
                FormEvent::new(
                    "vertex_move_start_position",
                    FieldValue::CoVertVec(self.selection.vertices.clone()),
                    FieldValue::CoVertVec(self.selection.vertices.clone()),
                ),
                FormEvent::new(
                    "vertex_move_start_state",
                    FieldValue::U8(self.drag_state as u8),
                    FieldValue::U8(if intersect_x {
                        DragState::MoveX
                    } else if intersect_y {
                        DragState::MoveY
                    } else {
                        DragState::MoveZ
                    } as u8),
                )
            ])
        } else if self.selection_press_start.is_none() && pressed {
            self.ray_select_or_paint(shift_pressed, heap)
        } else {
            Ok(smallvec![])
        }
    }
}

impl ShapeEditState {
    pub(crate) fn new(
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        gpu: &Gpu,
        shape: &mut Shape,
    ) -> Result<Self> {
        // Note: we upload here to pin the ShCode and other resources and store pointers here.
        // We can't create the entities at this point, however, so wait for later.
        // It's also worth noting that we do not use the cached shape in any way, so any
        // uuid or other reference that we pull from there is not going to interact.
        let shape_ids = shape.upload_shape(
            info,
            (scalar!(1. / 3.), Some(GearFootprintKind::Nose)),
            collection,
            install.palette(),
            gpu,
        )?;

        let mut texture = HashMap::new();
        for (shape_id, _source) in shape_ids.all_shapes().drain(..) {
            if let Some((name, uuid)) = shape.analysis(shape_id)?.singular_texture_ref() {
                let info = collection.lookup(&name.to_ascii_uppercase())?;
                let data = info.data()?;
                let pic = Pic::from_bytes(&data)?;
                texture.insert(
                    shape_id,
                    TextureLink {
                        name: name.to_owned(),
                        uuid: *uuid,
                        image: pic.image(install.palette())?,
                        data: pic.pixel_data()?.to_vec(),
                        palette: match pic.palette_fragment() {
                            Some(frag) => frag.to_palette(Some(install.palette()))?,
                            None => install.palette().clone(),
                        },
                    },
                );
            }
        }

        Ok(Self {
            name: info.name().to_owned(),
            shape_ids,
            entity: Entity::PLACEHOLDER,
            init_distance: meters!(0),
            init_position: MapFrame::default(),
            showing_help: false,
            tool: ShapeTool::default(),
            texture,
            edit_selection: EditSelection::default(),
            selection_color: rgb!(0xcf0061),
            selection_line_size: 2.,
            selection: Default::default(),
            selecting_palette_color: false,
            selection_press_start: None,
            selection_rectangle: None,
            colorpicker_color: 255,
            colorpicker_open: false,
            pencil_size: 1,
            drag_state: DragState::default(),
            drag_start_cursor: [0.; 2],
            drag_start_position: vec![],
            move_faces: false,
            scale_target: Default::default(),
            in_panel: false,
            cursor: [0.; 2],
            frame: Frame::from_geodetic_bearing_and_pitch(
                Geodetic::new(degrees!(LATITUDE), degrees!(LONGITUDE), feet!(ALTITUDE)),
                Bearing::new(degrees!(0_f64)),
                PitchCline::new(degrees!(0_f64)),
            ),
            extent: ShExtent::default(),
        })
    }

    // Ux drawing and handling

    fn draw_help(&self, ui: &mut egui::Ui) {
        egui::Window::new("Help")
            .default_size((640., 480.))
            .show(ui.ctx(), |ui| {
                ui.label(
                    egui::RichText::new(HELP_TEXT)
                        .family(FontFamily::Monospace)
                        .size(16.),
                );
            });
    }

    fn update_face_selection<F0, F1>(
        &mut self,
        shape: &mut Shape,
        mut update_face: F0,
        mut update_vert: F1,
    ) -> Result<()>
    where
        F0: FnMut(&mut iFC_Face) -> Result<()>,
        F1: FnMut(usize, &mut ShapeVertex) -> Result<()>,
    {
        for face_link in self.selection.faces.clone().into_iter() {
            // Update the code.
            let face = self.face_mut(&face_link.face, shape)?;
            update_face(face)?;

            // Update the GPU representation.
            let face_loop = shape.get_faces(self.shape_id())[&face_link.face]
                .face_loop()
                .to_owned();
            let verts = shape.get_vertices_mut(self.shape_id());
            for (i, offset) in face_loop.into_iter().enumerate() {
                update_vert(i, &mut verts[offset])?;
            }
        }
        Ok(())
    }

    fn update_faces_color(
        &mut self,
        color: u8,
        install: &GameInstallation,
        mut heap: HeapMut,
    ) -> Result<()> {
        self.update_face_selection(
            &mut heap.resource_mut::<Shape>(),
            |face| {
                *face.color_mut() = color;
                Ok(())
            },
            |_loop_index, vert| {
                let clr = install.palette().rgba_f32(color as usize)?;
                vert.set_color(clr);
                Ok(())
            },
        )
    }

    fn update_faces_flag(
        &mut self,
        flag: FaceContentFlags,
        value: bool,
        mut heap: HeapMut,
    ) -> Result<()> {
        let mut flags = self
            .face(&self.selection.faces[0].face, heap.resource::<Shape>())?
            .content_flags();
        flags.set(flag, value);
        self.update_face_selection(
            &mut heap.resource_mut::<Shape>(),
            |face| {
                *face.content_flags_mut() = flags;
                Ok(())
            },
            |_loop_index, _vert| Ok(()),
        )
    }

    fn update_face_texcoord(
        &mut self,
        index: usize,
        value: [u16; 2],
        mut heap: HeapMut,
    ) -> Result<()> {
        let shape: &mut Shape = &mut heap.resource_mut::<Shape>();
        let selection = &self.selection.faces[0].face;
        let frame_index = shape.get_faces(self.shape_id())[selection].active_atlas_frame_index();
        let Some(frame_index) = frame_index else {
            bail!("expected face with tex_coords to have a frame")
        };
        let (_texture_name, frame) = shape
            .get_texture_frames(self.shape_id())
            .get(frame_index)
            .ok_or_else(|| anyhow!("no such texture id"))?;
        let (base_s, base_t) = frame.raw_base();
        self.update_face_selection(
            shape,
            |face| {
                face.set_tex_coord_arr(index, value)?;
                Ok(())
            },
            |face_loop_index, vert| {
                if face_loop_index == index {
                    vert.set_raw_tex_coords(
                        base_s + value[0] as u32,
                        base_t.saturating_sub(value[1] as u32),
                    );
                }
                Ok(())
            },
        )?;
        Ok(())
    }

    fn update_vertex_positions(&mut self, cursor: [f64; 2], mut heap: HeapMut) -> Result<()> {
        assert!(
            !self.selection.vertices.is_empty(),
            "expected verts to move"
        );
        let shape_id = self.shape_id();
        let load_scale = self.load_scale(heap.resource::<Shape>());
        let wrld_m = heap
            .resource::<ScreenCamera>()
            .mouse_to_world((cursor[0], cursor[1]));
        let click_ray = Ray::new(
            heap.resource::<ScreenCamera>().position::<Meters>(),
            (wrld_m.dvec3() - heap.resource::<ScreenCamera>().position::<Meters>().dvec3())
                .normalize(),
        );
        let delta = match self.drag_state {
            DragState::Select => bail!("did not expect DragState::Select in move"),
            DragState::MoveX => DVec3::X,
            DragState::MoveY => DVec3::Y,
            DragState::MoveZ => DVec3::Z,
        };
        let origin = self.drag_start_center().expect("have a move frame");
        let move_ray = Ray::new(origin, (self.frame.facing() * delta).normalize());
        let approach = move_ray.closest_point_to_other(&click_ray);
        let delta = delta * (approach.offset - self.drag_sphere_offset());
        if self.move_faces {
            // To implement this properly, we need:
            //   1. a new representation of CoVert that separates the vb indices
            //      from the VxBuf refs.
            //   2. a way to highlight that a move will require new vertices
            //   3. the ability to create new vertices
            //   4. easy references to the VxBuf and VxBuf to vb indices mappings
            todo!();
        } else {
            assert_eq!(
                self.drag_start_position.len(),
                self.selection.vertices.len()
            );
            for (init, covert) in self
                .drag_start_position
                .iter()
                .zip(self.selection.vertices.iter_mut())
            {
                let p = *init.position() + delta;
                covert.update_position(
                    pt3_to_fa_pos(p, load_scale),
                    load_scale,
                    true, // self.recompute_face_centers,
                    true, // self.recompute_face_normals,
                    shape_id,
                    &mut heap.resource_mut::<Shape>(),
                )?;
            }
        }
        Ok(())
    }

    fn handle_rescale_shape(&self, scale: [f64; 3], shape: &mut Shape) -> Result<()> {
        // Rescaling what is displayed is easy: it's just the vxbuf.
        let load_scale = self.load_scale(shape);
        for vert in shape.get_vertices_mut(self.shape_id()) {
            let mut p = vert.position();
            p.set_x(meters!(p.x().f64() * scale[0]));
            p.set_y(meters!(p.y().f64() * scale[1]));
            p.set_z(meters!(p.z().f64() * scale[2]));
            vert.set_position(p);
        }

        // And for the SH file.
        //
        // We need to rescale only what was shown. Damage models have different outlines
        // and so cannot be scaled with the same factors and get reasonable outcomes, so
        // we can't just visit all vertex buffers in the SH. Instead, iterate over faces
        // and grab the indicated vb source for each vertex in each face.
        //
        // However! Scaling is not idempotent. As such it's very important that we only
        // visit each vertex once, despite many faces re-using vertex data. Thus, we have
        // to make a pass to dedup everything first, before doing our resize pass.
        let mut update_map: HashMap<Uuid, HashSet<usize>> = HashMap::new();
        for (face_uuid, face_info) in shape.get_faces(self.shape_id()) {
            let face_instr = shape.get_code(self.shape_id()).instruction(face_uuid)?;
            let InstrDetail::Face(face_detail) = face_instr.detail() else {
                bail!("invalid face reference");
            };
            for i in 0..face_info.face_loop().len() {
                let vxbuf_uuid = face_info.vxbuf_uuid();
                let vxbuf_offset = face_info.vxbuf_offset(i, face_detail);
                update_map
                    .entry(*vxbuf_uuid)
                    .and_modify(|v| {
                        v.insert(vxbuf_offset);
                    })
                    .or_insert_with(|| HashSet::from_iter([vxbuf_offset]));
            }
        }
        for (vxbuf_uuid, vxbuf_offsets) in &update_map {
            let InstrDetail::VertexBuffer(vxbuf) = shape
                .get_code_mut(self.shape_id())
                .instruction_mut(vxbuf_uuid)?
                .detail_mut()
            else {
                bail!("invalid vxbuf reference");
            };
            for offset in vxbuf_offsets {
                let mut p = fa_pos_to_pt3(vxbuf.vertex(*offset), load_scale);
                p.set_x(meters!(p.x().f64() * scale[0]));
                p.set_y(meters!(p.y().f64() * scale[1]));
                p.set_z(meters!(p.z().f64() * scale[2]));
                *vxbuf.vertex_mut(*offset) = pt3_to_fa_pos(p, load_scale);
            }
        }

        // FIXME: we need a way to re-run analysis passes, not just for this,
        //        but for anything that moves vertices as it could impact lots
        //        of downstream systems, not just scaling.

        Ok(())
    }

    fn face_edit_color(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        fields.push(if !self.selecting_palette_color {
            FormField::new_missing("face_color_toggle")?
                .with_label("Face Color")
                .with_hover_text("The base color of this face.")
                .with_display(FieldDisplay::PaletteColorButton(
                    self.face(selection, shape)?.color(),
                ))
        } else {
            FormField::new_missing("face_color_selector")?
                .with_label("Face Color")
                .with_hover_text("Select a new base color for this face.")
                .with_display(FieldDisplay::PaletteColorSelector(
                    self.face(selection, shape)?.color(),
                ))
        });
        Ok(())
    }

    fn face_edit_content_flags(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let flags = self.face(selection, shape)?.content_flags();
        use FaceContentFlags as FCF;
        fn make_field(flags: FCF, flag: FCF) -> Result<FormField> {
            Ok(
                FormField::new_missing(format!("face_content_flag_{flag:?}"))?
                    .with_label(flag.to_string())
                    .with_display(FieldDisplay::Checkbox(flags.contains(flag))),
            )
        }
        fields.extend([
            FormField::new_h2("face_content_flags_header")?.with_label("Face Content Flags:"),
            make_field(flags, FCF::UNK1)?,
            make_field(flags, FCF::HAVE_FACE_NORMAL)?.inactive(),
            make_field(flags, FCF::UNK2)?,
            make_field(flags, FCF::UNK3)?,
            make_field(flags, FCF::UNK4)?,
            make_field(flags, FCF::HAVE_TEXCOORDS)?.inactive(),
            make_field(flags, FCF::FILL_BACKGROUND)?,
            make_field(flags, FCF::UNK5)?,
        ]);
        Ok(())
    }

    fn face_edit_xform(
        &self,
        selection: &Uuid,
        shape: &Shape,
        heap: HeapRef,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let Some(xform_uuid) = shape.get_faces(self.shape_id())[selection].get_xform_source(
            self.entity,
            self.shape_id(),
            heap,
        )?
        else {
            // Nothing to edit if we don't have an xform source
            return Ok(());
        };
        let (usage, translation, rotation) = match shape
            .get_code(self.shape_id())
            .instruction(&xform_uuid)?
            .detail()
        {
            InstrDetail::XformUnmask(xform) => {
                (xform.usage(), xform.translation(), xform.rotation())
            }
            InstrDetail::XformUnmaskLong(xform) => {
                (xform.usage(), xform.translation(), xform.rotation())
            }
            _ => {
                return Ok(());
            }
        };
        fields.push(
            FormField::new_h2("face_edit_xform_header")?
                .with_label("Edit XForm")
                .with_hover_text("Will apply to all faces controlled by this Xform"),
        );
        fields.push(
            FormField::new_missing("face_edit_xform_usage")?
                .with_label("Usage")
                .with_hover_text("What does this face do?")
                .with_display(FieldDisplay::Label(format!("{usage:?}"))),
        );
        fields.push(
            FormField::new_missing("face_edit_xform_translation")?
                .with_label("Translation")
                .with_hover_text("Edit the base xform offset from untransformed position")
                .with_display(FieldDisplay::Label(format!("{translation:?}"))),
        );
        fields.push(
            FormField::new_missing("face_edit_xform_rotation")?
                .with_label("Rotation")
                .with_hover_text("Edit the base xform rotation from untransformed position")
                .with_display(FieldDisplay::Label(format!("{rotation:?}"))),
        );
        Ok(())
    }

    fn face_edit_texcoords(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face = self.face(selection, shape)?;
        if face.tex_coords().is_empty() {
            return Ok(());
        }
        fields.push(
            FormField::new_h2("face_texcoords_header")?
                .with_label("Tex Coords")
                .with_hover_text(
                    "Each vertices texture coordinate in the order specified on the face",
                ),
        );
        let (width, height) = self
            .maybe_texture()
            .map(|txt| (txt.image.width() as u16, txt.image.height() as u16))
            .unwrap_or((255, 255));
        for (i, (s, t)) in face.tex_coords().iter().enumerate() {
            fields.push(
                FormField::new_missing(format!("face_texcoord_{i}"))?
                    .with_label(format!("Offset {i}"))
                    .with_hover_text(format!("Texture Coordinate {i}"))
                    .with_display(FieldDisplay::U16x2Clamped {
                        value: [*s, *t],
                        ranges: [0..=width, 0..=height],
                    }),
            );
        }
        Ok(())
    }

    fn face_edit_normal(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let Some(normal) = self.face(selection, shape)?.normal() else {
            return Ok(());
        };
        {
            // Skip edit if VxNormal instructions override the face normal
            let face_loop = shape.get_faces(self.shape_id())[selection].face_loop();
            let verts = shape.get_vertices(self.shape_id());
            for vert_index in face_loop {
                if verts[*vert_index].is_vertex_normal() {
                    return Ok(());
                }
            }
        }
        fields.push(
            FormField::new_missing("face_normal")?
                .with_label("Face Normal")
                .with_hover_text("The normal vector of this face.")
                .with_display(FieldDisplay::F32x3(*normal)),
        );
        Ok(())
    }

    fn face_edit_center(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let Some(center) = self.face(selection, shape)?.raw_fa_center() else {
            return Ok(());
        };
        fields.push(
            FormField::new_missing("face_center")?
                .with_label("Face Center")
                .with_hover_text("The \"center\" location for this face.")
                .with_display(FieldDisplay::I16x3(*center)),
        );
        Ok(())
    }

    fn face_show_layout_flags(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let flags = self.face(selection, shape)?.layout_flags();
        use FaceLayoutFlags as FLF;
        fn make_field(label: &str, hover: &str, flag: FLF, flags: FLF) -> Result<FormField> {
            Ok(
                FormField::new_missing(format!("face_layout_flag_{flag:?}"))?
                    .with_label(label)
                    .with_hover_text(hover)
                    .inactive()
                    .with_display(FieldDisplay::Checkbox(flags.contains(flag))),
            )
        }
        fields.extend([
            FormField::new_h2("Face Layout Flags")?
                .with_hover_text("The layout flags are an implementation detail."),
            make_field(
                "unk0*",
                "I'm not sure what this flag controls.\nOther flags in this byte control layout,\nbut we don't seem to need this for decoding",
                FLF::UNK0,
                flags,
            )?,
            make_field(
                "Word Indices",
                "Indices are represented on disk as a u16 (instead of a u8)",
                FLF::USE_SHORT_INDICES,
                flags,
            )?,
            make_field(
                "Byte Face Center",
                "Face center coords are represented on disk as a u8 (instead of a u16)",
                FLF::USE_BYTE_FACE_CENTER,
                flags,
            )?,
            make_field(
                "Byte TexCoords",
                "Tex coords for this face are represented as u8 (instead of u16)",
                FLF::USE_BYTE_TEXCOORDS,
                flags,
            )?,
        ]);
        Ok(())
    }

    fn face_show_info(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let face = self.face(selection, shape)?;
        fields.extend([
            FormField::new_h2("Face Info")?.with_hover_text("We cannot yet insert vertices."),
            FormField::new_missing("Indices")?
                .with_hover_text("The vertex indices into the vertex pool (not the VxBuf instruction) that comprise this Face.")
                .inactive()
                .with_display(FieldDisplay::Label(format!("{:?}", face.indices()))),
            FormField::new_missing("Usage")?
                .with_hover_text("The vertex usage tracks what kind of control instruction references this face.")
                .inactive()
                .with_display(FieldDisplay::Label(format!("{:?}", face.usage()))),
        ]);
        Ok(())
    }

    fn face_show_instr_info(
        &self,
        selection: &Uuid,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let instr = self.instr(selection, shape)?;
        fields.extend([
            FormField::new_h2("Instruction Info")?.with_hover_text(
                "Details about the underlying 0xFC data to help with hex editing.",
            ),
            FormField::new_missing("Instr Magic")?
                .with_display(FieldDisplay::Label(format!("{:0X?}", instr.magic()))),
            FormField::new_missing("Instr Offset")?
                .with_hover_text("Offset in bytes from the start of the CODE section")
                .with_display(FieldDisplay::Label(format!(
                    "{} (0x{:04X})",
                    instr.instr_offset(),
                    instr.instr_offset()
                ))),
            FormField::new_missing("Instr Size")?
                .with_hover_text("Number of bytes that comprise this face")
                .with_display(FieldDisplay::Label(format!("{} bytes", instr.instr_size()))),
        ]);
        Ok(())
    }

    fn co_vertex_edit_position(
        &self,
        i: usize,
        co_vertex: &CoVertex,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let code = shape.get_code(self.shape_id());
        let mut vxbufs = BTreeMap::new();
        for vert_ref in co_vertex.vert_refs() {
            vxbufs
                .entry(*vert_ref.vxbuf_uuid())
                .or_insert_with(Vec::new)
                .push(vert_ref.vxbuf_offset());
        }
        for (vxbuf_uuid, offsets) in &vxbufs {
            let instr = code.instruction(vxbuf_uuid)?;
            fields.push(
                FormField::new_missing(format!(
                    "VxBuf Code Offset 0x{:04X}",
                    instr.instr_offset()
                ))?
                .with_hover_text("Informationa about all the overlapping vertices at this position")
                .inactive()
                .with_display(FieldDisplay::Label(format!("0x{:?}", offsets))),
            );
        }
        fields.push(
            FormField::new_missing(format!("covert_position_{i}"))?
                .with_label("CoVert Position")
                .with_hover_text("Change the position of this vertex")
                .with_display(FieldDisplay::I16x3(
                    co_vertex.get_fa_vertex_position(self.shape_id(), shape)?,
                )),
        );
        Ok(())
    }

    fn vert_ref_edit_position(
        &self,
        i: usize,
        vert_ref: &VertexRef,
        shape: &Shape,
        fields: &mut Vec<FormField>,
    ) -> Result<()> {
        let uuid = vert_ref.vxbuf_uuid();
        let offset = vert_ref.vxbuf_offset();
        let code = shape.get_code(self.shape_id()).instruction(uuid)?;
        let code_offset = code.instr_offset();
        let InstrDetail::VertexBuffer(vxbuf) = code.detail() else {
            bail!("expected a vertex buffer");
        };
        fields.push(
            FormField::new_missing(format!("vert_ref_position_{i}"))?
                .with_label(format!("VxBuf {code_offset:04X}, offset {offset} Position"))
                .with_hover_text("Change the position of a single vertex")
                .with_display(FieldDisplay::I16x3(*vxbuf.vertex(offset))),
        );
        Ok(())
    }

    // Drag Move Layout and Handling

    // The drag origin for computing relative offset from cursor position
    fn drag_start_center(&self) -> Option<Pt3<Meters>> {
        self.drag_start_position
            .first()
            .map(|v| self.frame.position() + self.frame.facing() * *v.position())
    }

    fn move_frame_center(&self) -> Option<Pt3<Meters>> {
        self.selection
            .vertices
            .first()
            .map(|v| self.frame.position() + self.frame.facing() * *v.position())
    }

    fn move_frame_offset(&self, dir: DVec3) -> V3<Length<Meters>> {
        let off = dir * self.drag_sphere_offset();
        self.frame.facing() * V3::<Length<Meters>>::new(off.x(), off.y(), off.z())
    }

    fn drag_sphere_offset(&self) -> Length<Meters> {
        meters!(1)
    }

    fn move_x_sphere(&self) -> Option<Sphere<Meters>> {
        self.move_frame_center().map(|p| {
            let center = p + self.move_frame_offset(DVec3::X);
            Sphere::from_center_and_radius(center, meters!(feet!(0.5)))
        })
    }

    fn move_y_sphere(&self) -> Option<Sphere<Meters>> {
        self.move_frame_center().map(|p| {
            let center = p + self.move_frame_offset(DVec3::Y);
            Sphere::from_center_and_radius(center, meters!(feet!(0.5)))
        })
    }

    fn move_z_sphere(&self) -> Option<Sphere<Meters>> {
        self.move_frame_center().map(|p| {
            let center = p + self.move_frame_offset(DVec3::Z);
            Sphere::from_center_and_radius(center, meters!(feet!(0.5)))
        })
    }

    fn intersect_move_x(&self, click_ray: &Ray<Meters>) -> bool {
        if let Some(sphere) = self.move_x_sphere() {
            return sphere_vs_ray(&sphere, click_ray).is_some();
        }
        false
    }

    fn intersect_move_y(&self, click_ray: &Ray<Meters>) -> bool {
        if let Some(sphere) = self.move_y_sphere() {
            return sphere_vs_ray(&sphere, click_ray).is_some();
        }
        false
    }

    fn intersect_move_z(&self, click_ray: &Ray<Meters>) -> bool {
        if let Some(sphere) = self.move_z_sphere() {
            return sphere_vs_ray(&sphere, click_ray).is_some();
        }
        false
    }

    // On-Screen Elements

    pub(crate) fn show_edit_ui(
        &mut self,
        shape: &Shape,
        markers: &mut Markers,
        mut heap: HeapMut,
    ) -> Result<()> {
        if self.tool == ShapeTool::Scale {
            self.show_extents(markers);
        }
        self.show_selection_rectangle(markers);
        self.show_face_selections(shape, markers, heap.as_mut())?;
        self.show_vertex_selections_and_handles(shape, markers, heap.as_mut())?;
        self.show_move_frame(markers)?;
        Ok(())
    }

    fn show_extents(&self, markers: &mut Markers) {
        let extent = &self.extent;
        let frame = &self.frame;
        for pos in extent.gear_touchpoints() {
            let pos = frame.position() + frame.facing() * *pos;
            markers.draw_arrow(
                rgb!(0xFFFFFF),
                &Arrow::new(
                    pos,
                    frame.facing() * DVec3::Y,
                    *extent.gear_max_deflection(),
                    meters!(0.1),
                ),
            );
        }

        markers.draw_local_bounding_box(
            Color::MAGENTA,
            Color::GREEN,
            extent.aabb_body(),
            meters!(0.05),
            frame,
        );
        markers.draw_local_bounding_box(
            rgb!(0xFF7777),
            Color::RED,
            extent.aabb_afterburner(),
            meters!(0.02),
            frame,
        );
        markers.draw_local_bounding_box(
            Color::GREEN,
            rgb!(0x77FF77),
            extent.aabb_airbrake(),
            meters!(0.02),
            frame,
        );
        markers.draw_local_bounding_box(
            Color::BLUE,
            rgb!(0x7777FF),
            extent.aabb_bay(),
            meters!(0.02),
            frame,
        );
        markers.draw_local_bounding_box(
            Color::RED,
            rgb!(0xFF7777),
            extent.aabb_gear(),
            meters!(0.02),
            frame,
        );
        markers.draw_local_bounding_box(
            rgb!(0x444444),
            rgb!(0x440000),
            extent.aabb_gear_footprint(),
            meters!(0.02),
            frame,
        );
        markers.draw_local_bounding_box(
            rgb!(0x7777FF),
            rgb!(0xFF7777),
            extent.aabb_hook(),
            meters!(0.02),
            frame,
        );
    }

    fn show_selection_rectangle(&self, markers: &mut Markers) {
        if let Some(sel) = &self.selection_rectangle {
            // Note: only show the selection rectangle when selecting
            if matches!(self.tool, ShapeTool::SelectFace | ShapeTool::SelectVertex) {
                let radius = 6.;
                for i in 0..4 {
                    let j = (i + 1) % 4;
                    markers.draw_screen_point(sel[i], radius, Color::BLUE);
                    markers.draw_screen_line(sel[i], sel[j], radius, Color::BLUE);
                }
            }
        }
    }

    fn show_face_selections(
        &self,
        shape: &Shape,
        markers: &mut Markers,
        mut heap: HeapMut,
    ) -> Result<()> {
        let p = self.frame.position();
        let q = self.frame.facing();
        let shape_id = self.shape_id();
        let verts = shape.get_vertices(shape_id);

        // Draw face selections
        for face_link in &self.selection.faces {
            let face_uuid = &face_link.face;
            let face_info = shape.get_faces(shape_id).get(face_uuid).unwrap();
            let m = face_info.get_xform_matrix(self.entity, shape_id, shape, heap.as_mut())?;

            let face_loop = &shape.get_faces(shape_id)[face_uuid].face_loop();
            for (i, &v0i) in face_loop.iter().enumerate() {
                let j = (i + 1) % face_loop.len();
                let v1j = face_loop[j];
                let v0 = &verts[v0i];
                let v1 = &verts[v1j];
                let p0 = p + q * v0.position().transform_by(&m);
                let p1 = p + q * v1.position().transform_by(&m);
                // Note: no point drawing a point since the vertex selection will cover it
                markers.draw_screen_line(p0, p1, self.selection_line_size, self.selection_color);
            }

            let face = self.face(face_uuid, shape)?;
            if let Some(center) = face.center(self.load_scale(shape)) {
                let c = p + q * center;
                let color = if face.content_flags().contains(FaceContentFlags::UNK2) {
                    Color::GREEN
                } else {
                    Color::MAGENTA
                };

                if let Some(normal) = face.normal() {
                    let n = q * fa_face_norm_to_vec(normal).as_dvec3();
                    markers.draw_arrow(color, &Arrow::new(c, n, meters!(1), meters!(0.1)));
                } else {
                    markers.draw_screen_point(c, 6. * self.selection_line_size, color);
                }
            }
        }
        Ok(())
    }

    fn show_vertex_selections_and_handles(
        &self,
        shape: &Shape,
        markers: &mut Markers,
        mut heap: HeapMut,
    ) -> Result<()> {
        let p = self.frame.position();
        let q = self.frame.facing();
        let shape_id = self.shape_id();
        let verts = shape.get_vertices(shape_id);

        // Draw vertex selections
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m = face_info.get_xform_matrix(self.entity, shape_id, shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let p = p + q * v.position().transform_by(&m);
                let contains_vertex = self
                    .selection
                    .vertices
                    .iter()
                    .any(|co_vert| co_vert.position() == &v.position());
                if contains_vertex {
                    if v.is_vertex_normal() {
                        let dir = q * v.normal().as_dvec3();
                        markers.draw_arrow(
                            rgb!(0xFF80FF),
                            &Arrow::new(p, dir, meters!(0.5), meters!(0.025)),
                        );
                    }
                    markers.draw_screen_point(
                        p,
                        4. * self.selection_line_size,
                        self.selection_color,
                    );
                } else if self.tool == ShapeTool::SelectVertex {
                    markers.draw_point(p, meters!(0.05), rgb!(0x00FFFF));
                }
            }
        }
        Ok(())
    }

    fn show_move_frame(&self, markers: &mut Markers) -> Result<()> {
        if self.selection.vertices.is_empty() {
            return Ok(());
        }

        let p = self.move_frame_center().expect("have a move frame");
        let offset = meters!(feet!(1));
        let radius = meters!(feet!(0.2));
        let x = self.move_x_sphere().expect("have a move frame");
        let y = self.move_y_sphere().expect("have a move frame");
        let z = self.move_z_sphere().expect("have a move frame");
        markers.draw_cylinder(
            Color::RED,
            p + self.frame.facing() * V3::new(offset, meters!(0), meters!(0)),
            *x.center(),
            radius,
        );
        markers.draw_sphere(&x, Color::RED);
        markers.draw_cylinder(
            Color::GREEN,
            p + self.frame.facing() * V3::new(meters!(0), offset, meters!(0)),
            *y.center(),
            radius,
        );
        markers.draw_sphere(&y, Color::GREEN);
        markers.draw_cylinder(
            Color::BLUE,
            p + self.frame.facing() * V3::new(meters!(0), meters!(0), offset),
            *z.center(),
            radius,
        );
        markers.draw_sphere(&z, Color::BLUE);

        Ok(())
    }

    // Utility Methods

    pub fn shape_id(&self) -> ShapeId {
        match self.edit_selection {
            EditSelection::Normal => self.shape_ids.normal(),
            EditSelection::Damage(i) => self.shape_ids.damage()[i],
        }
    }

    pub fn load_scale(&self, shape: &Shape) -> Scalar {
        // Note: scale is uniform across all instructions, so any will do.
        shape.analysis(self.shape_id()).unwrap().load_scale()
    }

    pub fn maybe_texture(&self) -> Option<&TextureLink> {
        self.texture.get(&self.shape_id())
    }

    pub fn maybe_texture_mut(&mut self) -> Option<&mut TextureLink> {
        self.texture.get_mut(&self.shape_id())
    }

    pub fn texture(&self) -> Result<&TextureLink> {
        self.maybe_texture().ok_or_else(|| {
            anyhow!(
                "expected a texture to be present for {:?}",
                self.edit_selection
            )
        })
    }

    pub fn texture_mut(&mut self) -> Result<&mut TextureLink> {
        self.maybe_texture_mut()
            .ok_or_else(|| anyhow!("expected a texture to be present"))
    }

    fn instr<'a>(&self, uuid: &Uuid, shape: &'a Shape) -> Result<&'a ShInstr> {
        let sh_code = shape.get_code(self.shape_id());
        sh_code.instruction(uuid)
    }

    fn face<'a>(&self, uuid: &Uuid, shape: &'a Shape) -> Result<&'a iFC_Face> {
        let instr = self.instr(uuid, shape)?;
        Ok(match instr.detail() {
            InstrDetail::Face(face) => face,
            _ => bail!("not a face instruction"),
        })
    }

    fn face_mut<'a>(&mut self, uuid: &Uuid, shape: &'a mut Shape) -> Result<&'a mut iFC_Face> {
        let sh_code = shape.get_code_mut(self.shape_id());
        let InstrDetail::Face(face) = sh_code.instruction_mut(uuid)?.detail_mut() else {
            bail!("expected a face instruction");
        };
        Ok(face)
    }

    pub fn selected_faces(&self) -> Vec<Uuid> {
        self.selection.faces.iter().map(|fl| fl.face).collect()
    }

    // Selection Management

    fn ray_select_or_paint(
        &mut self,
        shift_pressed: bool,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        self.selection_press_start = Some(self.cursor);
        heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| {
            let camera = heap.resource::<ScreenCamera>();
            let wrld_m = camera.mouse_to_world((self.cursor[0], self.cursor[1]));
            let click_ray = Ray::new(
                camera.position::<Meters>(),
                (wrld_m.dvec3() - camera.position::<Meters>().dvec3()).normalize(),
            );

            match self.tool {
                ShapeTool::SelectFace => {
                    self.ray_select_face(&click_ray, shift_pressed, &mut shape, heap)
                }
                ShapeTool::SelectVertex => {
                    self.ray_select_vertex(&wrld_m, &click_ray, shift_pressed, &mut shape, heap)
                }
                ShapeTool::ColorPicker => {
                    if let Some((_, _, st)) =
                        self.intersect_texture(&click_ray, &mut shape, heap)?
                    {
                        Ok(smallvec![FormEvent::new(
                            "texture_pick_color",
                            FieldValue::U8(self.colorpicker_color),
                            FieldValue::U8(self.texture()?.pick_palettized_color(st)),
                        )])
                    } else {
                        Ok(smallvec![])
                    }
                }
                ShapeTool::Pencil => {
                    if let Some(intersect) = self.intersect_texture(&click_ray, &mut shape, heap)? {
                        self.handle_stroke(intersect)
                    } else {
                        Ok(smallvec![])
                    }
                }
                ShapeTool::Scale => Ok(smallvec![]),
            }
        })
    }

    fn region_select_or_paint(
        &mut self,
        shift_pressed: bool,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        let Some(origin) = self.selection_press_start else {
            return Ok(smallvec![]);
        };

        // Region select
        let cursor = self.cursor;

        // Early exit if the selection is empty
        if relative_eq!(origin[0], cursor[0]) || relative_eq!(origin[1], cursor[1]) {
            self.selection_rectangle = None;
            return Ok(smallvec![]);
        }

        // Get the 4 points of the selection, sorted clockwise.
        let mut points = [
            (OrderedFloat(origin[0]), OrderedFloat(origin[1])),
            (OrderedFloat(cursor[0]), OrderedFloat(origin[1])),
            (OrderedFloat(origin[0]), OrderedFloat(cursor[1])),
            (OrderedFloat(cursor[0]), OrderedFloat(cursor[1])),
        ]
        .to_vec();
        points.sort();
        points.swap(2, 3);

        // Move all points into world space.
        let points = points
            .into_iter()
            .map(|p| {
                heap.resource::<ScreenCamera>()
                    .mouse_to_world((p.0.into(), p.1.into()))
            })
            .collect::<Vec<_>>();
        let region = [points[0], points[1], points[2], points[3]];

        // Build planes from our camera origin and pairs of points.
        let position = heap.resource::<ScreenCamera>().position::<Meters>();
        let planes = [
            Plane::from_triangle(&position, &region[0], &region[1]),
            Plane::from_triangle(&position, &region[1], &region[2]),
            Plane::from_triangle(&position, &region[2], &region[3]),
            Plane::from_triangle(&position, &region[3], &region[0]),
        ];

        self.selection_rectangle = Some(region);
        heap.resource_scope(|heap: HeapMut, mut shape: Mut<Shape>| match self.tool {
            ShapeTool::SelectFace => {
                self.region_select_faces(&planes, shift_pressed, &mut shape, heap)
            }
            ShapeTool::SelectVertex => {
                self.region_select_vertices(&planes, shift_pressed, &mut shape, heap)
            }
            ShapeTool::ColorPicker => {
                // Note: drag for color select is just confusing
                Ok(smallvec![])
            }
            ShapeTool::Pencil => {
                let camera = heap.resource::<ScreenCamera>();
                let wrld_m = camera.mouse_to_world((cursor[0], cursor[1]));
                let click_ray = Ray::new(
                    camera.position::<Meters>(),
                    (wrld_m.dvec3() - camera.position::<Meters>().dvec3()).normalize(),
                );
                if let Some(intersect) = self.intersect_texture(&click_ray, &mut shape, heap)? {
                    self.handle_stroke(intersect)
                } else {
                    Ok(smallvec![])
                }
            }
            ShapeTool::Scale => Ok(smallvec![]),
        })
    }

    // Use point intersection on mouse-press to immediately select
    // whatever is under the cursor.
    pub fn ray_select_face(
        &self,
        click_ray: &Ray<Meters>,
        shift_pressed: bool,
        shape: &mut Shape,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // Get shape, but allow us to still get the xforms mutably
        let mut intersections = vec![];
        // Note: this is the slice of vertices specific to this shape.
        let verts = shape.get_vertices(self.shape_id());
        for (uuid, info) in shape.get_faces(self.shape_id()) {
            let m = info.get_xform_matrix(self.entity, self.shape_id(), shape, heap.as_mut())?;
            for tri in info.triangles(verts, &self.frame, &m) {
                if let Some(intersect) = ray_vs_triangle(click_ray, &tri[0], &tri[1], &tri[2]) {
                    intersections.push((intersect, *uuid));
                }
            }
        }
        intersections.sort_by_key(|(intersect, _)| intersect.distance::<Meters>());

        let prior = self.selected_faces();
        if !shift_pressed {
            if let Some((_intersect, uuid)) = intersections.first() {
                // Click to select a face
                Ok(smallvec![FormEvent::new(
                    "select_faces",
                    FieldValue::UuidVec(prior),
                    FieldValue::UuidVec(vec![*uuid]),
                )])
            } else {
                // FIXME: Click off of a face should unselect, but we shouldn't unselect
                //        if we are clicking on a GUI element -- bring our gui filtering here too
                Ok(smallvec![])
            }
        } else if let Some((_intersect, uuid)) = intersections.first() {
            if prior.contains(uuid) {
                // Shift + Click on selected to unselect
                let mut next = prior.clone();
                next.retain(|v| v != uuid);
                Ok(smallvec![FormEvent::new(
                    "select_faces",
                    FieldValue::UuidVec(prior),
                    FieldValue::UuidVec(next),
                )])
            } else {
                // Shift + Click on an unselected face to select it
                let mut next = prior.clone();
                next.push(*uuid);
                Ok(smallvec![FormEvent::new(
                    "select_faces",
                    FieldValue::UuidVec(prior),
                    FieldValue::UuidVec(next),
                )])
            }
        } else {
            // Shift + Click on nothing does not unselect
            Ok(smallvec![])
        }
    }

    pub fn region_select_faces(
        &self,
        planes: &[Plane<Meters>; 4],
        shift_pressed: bool,
        shape: &mut Shape,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // Transform from model to world space for the shape.
        let p = self.frame.position();
        let q = self.frame.facing();

        // Note: this is the slice of vertices specific to this shape.
        let mut intersections = vec![];
        let verts = shape.get_vertices(self.shape_id());
        for (uuid, info) in shape.get_faces(self.shape_id()) {
            // Compute the normal of the first tri to see if we are front facing or back facing.
            let xform =
                info.get_xform_matrix(self.entity, self.shape_id(), shape, heap.as_mut())?;
            let camera = heap.resource::<ScreenCamera>();
            let tri = info.triangles(verts, &self.frame, &xform).next().unwrap();
            let normal = compute_normal(&tri[0], &tri[1], &tri[2]);
            if normal.dot(*camera.forward()) > 0.5 {
                continue;
            }
            // Find any point in the face loop that is fully enclosed.
            for index in info.face_loop() {
                let pt = p + q * verts[*index].position().transform_by(&xform);
                if planes.iter().all(|plane| plane.point_is_in_front(&pt)) {
                    intersections.push(*uuid);
                    break;
                }
            }
        }

        let prior = self.selected_faces();
        if !shift_pressed && !intersections.is_empty() {
            // Click and drag to select all intersected faces
            Ok(smallvec![FormEvent::new(
                "select_faces",
                FieldValue::UuidVec(prior),
                FieldValue::UuidVec(intersections),
            )])
        } else {
            // Shift + Click and drag to add intersected faces to the selection
            let mut next = prior.clone();
            next.extend(&intersections);
            Ok(smallvec![FormEvent::new(
                "select_faces",
                FieldValue::UuidVec(prior),
                FieldValue::UuidVec(next),
            )])
        }
    }

    pub fn faces_to_selection(&self, selected: &[Uuid], shape: &Shape) -> Result<ShapeSelection> {
        // Collect all face pts
        let mut all_face_pts = vec![];
        for uuid in selected {
            let face_info = &shape.get_faces(self.shape_id())[uuid];
            for &vert_index in face_info.face_loop() {
                let vx = shape.get_vertices(self.shape_id())[vert_index];
                all_face_pts.push(vx.position());
            }
        }

        // Convert raw points to co-verts
        let co_verts = self.positions_to_coverts(&all_face_pts, shape)?;

        // Search for our faces in the coverts to get linked faces
        let mut faces = Vec::with_capacity(selected.len());
        for uuid in selected {
            faces.push(FaceLink::new(*uuid));
        }
        Ok(ShapeSelection {
            vertices: co_verts,
            faces,
        })
    }

    pub fn positions_to_coverts(
        &self,
        positions: &[Pt3<Meters>],
        shape: &Shape,
    ) -> Result<Vec<CoVertex>> {
        // Get unique face points, throwing away all but one copy of shared vertices
        let uniq_pts = positions
            .iter()
            .unique_by(|&v| (v.at(0).of64(), v.at(1).of64(), v.at(2).of64()))
            .collect::<Vec<_>>();

        // Map points to coverts
        let mut co_verts = vec![];
        for &pos in &uniq_pts {
            // Note: handle move gracefully by rejecting defunct positions
            if let Some(co_vert) = CoVertex::from_position(pos, self.shape_id(), shape)? {
                co_verts.push(co_vert);
            }
        }

        Ok(co_verts)
    }

    pub fn ray_select_vertex(
        &self,
        wrld_m: &Pt3<Meters>,
        click_ray: &Ray<Meters>,
        shift_pressed: bool,
        shape: &mut Shape,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // Transform from model to world space for the shape.
        let p = self.frame.position();
        let q = self.frame.facing();

        // Note: this will select all co-located vertices, but may select multiple loci
        // of co-vertices: e.g. if multiple points intersect the ray. We only want to select
        // the closest co-vertices, so we take the closest. We have to revisit all verts
        // via the faces anyway in order to discover the associated faces.
        let mut intersections = vec![];
        let shape_id = self.shape_id();
        let verts = shape.get_vertices(shape_id);
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m =
                face_info.get_xform_matrix(self.entity, self.shape_id(), shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let pt = p + q * v.position().transform_by(&m);
                let sphere = Sphere::from_center_and_radius(pt, meters!(0.05));
                if sphere_vs_ray(&sphere, click_ray).is_some() {
                    intersections.push((v.position(), index));
                }
            }
        }

        // FIXME: we should be able to check if the mouse is in a UX element. If we just
        //        naively deselect on click, trying to edit will unselect the thing we are
        //        trying to edit!
        if intersections.is_empty() {
            return Ok(smallvec![]);
        }

        // Get the closest location.
        intersections.sort_by_key(|(intersect, _)| {
            OrderedFloat((intersect.dvec3() - wrld_m.dvec3()).length())
        });
        let (pos, _) = intersections.first().unwrap();

        let Some(co_vert) = CoVertex::from_position(pos, shape_id, shape)? else {
            bail!("expected to find a co-vertex for an already found position");
        };

        let prior = self.selection.vertices.clone();
        if !shift_pressed {
            Ok(smallvec![FormEvent::new(
                "select_verts",
                FieldValue::CoVertVec(prior),
                FieldValue::CoVertVec(vec![co_vert]),
            )])
        } else if self
            .selection
            .vertices
            .iter()
            .map(|v| v.position())
            .collect::<Vec<_>>()
            .contains(&co_vert.position())
        {
            let mut next = prior.clone();
            next.retain(|v| v.position() != co_vert.position());
            Ok(smallvec![FormEvent::new(
                "select_verts",
                FieldValue::CoVertVec(prior),
                FieldValue::CoVertVec(next),
            )])
        } else {
            let mut next = prior.clone();
            next.push(co_vert);
            Ok(smallvec![FormEvent::new(
                "select_verts",
                FieldValue::CoVertVec(prior),
                FieldValue::CoVertVec(next),
            )])
        }
    }

    pub fn region_select_vertices(
        &self,
        planes: &[Plane<Meters>; 4],
        shift_pressed: bool,
        shape: &mut Shape,
        mut heap: HeapMut,
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // Transform from model to world space for the shape.
        let p = self.frame.position();
        let q = self.frame.facing();

        // Note: this is the slice of vertices specific to this shape.
        let mut intersections = vec![];
        let shape_id = self.shape_id();
        let verts = shape.get_vertices(shape_id);
        let mut visited = HashSet::with_capacity(verts.len());
        for face_info in shape.get_faces(shape_id).values() {
            let m = face_info.get_xform_matrix(self.entity, shape_id, shape, heap.as_mut())?;
            for index in face_info.face_loop() {
                if !visited.insert(*index) {
                    continue;
                }
                let v = &verts[*index];
                let pt = p + q * v.position().transform_by(&m);
                if planes.iter().all(|plane| plane.point_is_in_front(&pt)) {
                    intersections.push(v.position());
                }
            }
        }
        let uniq_intersections = intersections
            .iter()
            .unique_by(|v| OrderedFloat((v.at(0) + v.at(1) + v.at(2)).f64()))
            .collect::<Vec<_>>();
        let mut co_verts = vec![];
        for pos in &uniq_intersections {
            let Some(co_vert) = CoVertex::from_position(pos, shape_id, shape)? else {
                bail!("expected to find a co-vertex for an already found position");
            };
            co_verts.push(co_vert);
        }

        // FIXME: we should be able to check if the mouse is in a UX element. If we just
        //        naively deselect on click, trying to edit will unselect the thing we are
        //        trying to edit.
        let prior = self.selection.vertices.clone();
        if !shift_pressed && !co_verts.is_empty() {
            Ok(smallvec![FormEvent::new(
                "select_verts",
                FieldValue::CoVertVec(prior),
                FieldValue::CoVertVec(co_verts),
            )])
        } else {
            co_verts.retain(|cv| {
                !self
                    .selection
                    .vertices
                    .iter()
                    .any(|sv| sv.position() == cv.position())
            });
            Ok(smallvec![FormEvent::new(
                "select_verts",
                FieldValue::CoVertVec(prior),
                FieldValue::CoVertVec(co_verts),
            )])
        }
    }

    pub fn coverts_to_selection(
        &self,
        selected: Vec<CoVertex>,
        shape: &Shape,
    ) -> Result<ShapeSelection> {
        // Collect all faces.
        let mut all_faces = HashSet::new();
        for cv in &selected {
            for v in cv.vert_refs() {
                all_faces.insert(*v.face_uuid());
            }
        }

        // For each face, check if the coverts contains all of its face loop.
        let mut selected_faces = vec![];
        for face_uuid in &all_faces {
            let face_info = &shape.get_faces(self.shape_id())[face_uuid];
            if face_info
                .face_loop()
                .iter()
                .all(|index| selected.iter().any(|cv| cv.contains_vb_index(*index)))
            {
                selected_faces.push(*face_uuid);
            }
        }

        let mut faces = Vec::with_capacity(selected.len());
        for uuid in &selected_faces {
            faces.push(FaceLink::new(*uuid));
        }

        Ok(ShapeSelection {
            vertices: selected,
            faces,
        })
    }

    fn intersect_texture(
        &self,
        click_ray: &Ray<Meters>,
        shape: &mut Shape,
        mut heap: HeapMut,
    ) -> Result<Option<(usize, AtlasFrame, [u32; 2])>> {
        let shape_id = self.shape_id();

        // Get shape, but allow us to still get the xforms mutably
        let mut intersections = vec![];
        // Note: this is the slice of vertices specific to this shape.
        let verts = shape.get_vertices(shape_id);
        for (uuid, info) in shape.get_faces(shape_id) {
            let m = info.get_xform_matrix(self.entity, shape_id, shape, heap.as_mut())?;
            for (i, tri) in info.triangles(verts, &self.frame, &m).enumerate() {
                if let Some(intersect) = ray_vs_triangle(click_ray, &tri[0], &tri[1], &tri[2]) {
                    intersections.push((intersect, *uuid, i));
                }
            }
        }
        if intersections.is_empty() {
            return Ok(None);
        }

        // The texture coordinate stored on the vertex is a u32 pixel offset into
        // the chunk mega-atlas (s,t){atlas}. The (u,v){atlas} is computed in the
        // vertex buffer by dividing by the _current_ size of the atlas. This
        // indirection allows us to change the atlas size between frames.
        // In order to find the pixel of the texcoord in the texture, we need to
        // subtract out the base (s,t){atlas} where the PIC for this SH is loaded.
        // That is the "frame" of the texture, stored in the Shape.

        // Find first intersection: that is the face we are painting on
        intersections.sort_by_key(|(intersect, _, _)| intersect.distance::<Meters>());
        let (intersect, uuid, tri_offset) = &intersections[0];
        let info = &shape.get_faces(shape_id)[uuid];
        let tri = info.triangle(*tri_offset, verts)?;
        let Some(frame_index) = info.active_atlas_frame_index() else {
            return Ok(None);
        };

        // Compute the uv and st of the intersection
        // We need to undo this:
        //
        // let (frame_base_s, frame_base_t) = frame.raw_base();
        // let tex_coord = face.tex_coord(facet_vertex_offset);
        // v.set_raw_tex_coords(
        //     frame_base_s + tex_coord[0] as u32,
        //     frame_base_t.saturating_sub(tex_coord[1] as u32),
        // );
        //
        // raw_base is [s0, t0], since FA works top to bottom.
        // Then we add s to s as normal to get the atlas pixel offset.
        // Then we then take our bottom atlas t and subtract out the vertex t
        //
        // vx_s = frame_base_s + face_s
        // vx_t = frame_base_t - face_t
        //
        // For drawing we need u/v on the atlas
        // vx_u = vx_s / atlas.width()
        // vx_v = vx_t / atlas.height()
        //
        // But for this, we need u/v on the underlying texture
        // face_u = face_s / frame.width()
        // face_v = face_t / frame.height()
        //
        // So, we need to find face s/t, given frame_base (raw) and the vertex s/t
        // face_s = vx_s - frame_base_s
        // face_t = frame_base_t - vx_t
        //
        // We now have a fractional place on the triangle `uvw` that we
        // got from intersecting that gives us a fractional amount to take
        // from each vertex, given we can get a uv there instead of an st.
        // So we need a uv for each vertex first.
        let (_texture_name, frame) = shape
            .get_texture_frames(shape_id)
            .get(frame_index)
            .ok_or_else(|| anyhow!("no such texture id"))?;
        fn compute_uv(v: &ShapeVertex, frame: &AtlasFrame, f: f64) -> [f64; 2] {
            let (frame_base_s, frame_base_t) = frame.raw_base();
            let vx = v.tex_coord();
            let face_s = vx[0].saturating_sub(frame_base_s);
            let face_t = frame_base_t - vx[1];
            let face_u = face_s as f64 / frame.width() as f64;
            let face_v = (frame.height() as f64 - face_t as f64) / frame.height() as f64;
            [face_u * f, face_v * f]
        }
        let uv0 = compute_uv(tri[1], frame, intersect.u());
        let uv1 = compute_uv(tri[2], frame, intersect.v());
        let uv2 = compute_uv(tri[0], frame, intersect.w());
        let uv = [
            (uv0[0] + uv1[0] + uv2[0]).clamp(0., 1.),
            (uv0[1] + uv1[1] + uv2[1]).clamp(0., 1.),
        ];
        let st = [
            (uv[0] * frame.width() as f64) as u32,
            (uv[1] * frame.height() as f64) as u32,
        ];

        Ok(Some((frame_index, frame.to_owned(), st)))
    }

    fn handle_stroke(
        &self,
        (frame_index, frame, st): (usize, AtlasFrame, [u32; 2]),
    ) -> Result<SmallVec<[FormEvent; 2]>> {
        // let (image, raw, _palette, _name) = &self.textures[frame_index];
        let Some(tex) = self.maybe_texture() else {
            bail!("no texture in handle_stroke")
        };
        let pix = tex.data[(st[0] + tex.image.width() * st[1]) as usize];
        let size = self.pencil_size.clamp(1, 16);
        let px_left = (size - 1) / 2;
        let px_right = size / 2;
        let left = st[0].saturating_sub(px_left);
        let right = st[0].saturating_add(px_right);
        let bottom = st[1].saturating_sub(px_left);
        let top = st[1].saturating_add(px_right);
        let mut out = smallvec![];
        for x in left..=right {
            for y in bottom..=top {
                out.push(FormEvent::new(
                    "texture_write_color",
                    FieldValue::TextureWrite {
                        frame_index,
                        frame,
                        st: [x, y],
                        color: pix,
                    },
                    FieldValue::TextureWrite {
                        frame_index,
                        frame,
                        st: [x, y],
                        color: self.colorpicker_color,
                    },
                ));
            }
        }
        Ok(out)
    }
}
