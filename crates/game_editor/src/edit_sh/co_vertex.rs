// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, Result};
use geometry::algorithm::compute_normal;
use sh::{vec_to_fa_face_norm, InstrDetail};
use shape::{Shape, ShapeId};
use uuid::Uuid;

// Holds all locations where a vertex is referenced.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct VertexRef {
    // Offset within the rendering vertex buffer (shape.get_vertices)
    vb_offset: usize,

    // VxBuf instruction and offset within the VxBuf in the source file
    vxbuf: (Uuid, usize),

    // Back-Reference to owning face
    face: Uuid,
}

impl VertexRef {
    fn compute_face_centroid(&self, shape_id: ShapeId, shape: &Shape) -> Pt3<Meters> {
        let face_loop = shape.get_faces(shape_id)[&self.face].face_loop();
        let verts = shape.get_vertices(shape_id);
        let mut centroid = Pt3::<Meters>::zero();
        for vert_index in face_loop {
            centroid += verts[*vert_index].position();
        }
        (centroid.v3() / scalar!(face_loop.len())).pt3()
    }

    pub fn face_uuid(&self) -> &Uuid {
        &self.face
    }

    pub fn vxbuf_uuid(&self) -> &Uuid {
        &self.vxbuf.0
    }

    pub fn vxbuf_offset(&self) -> usize {
        self.vxbuf.1
    }

    pub fn vb_offset(&self) -> usize {
        self.vb_offset
    }

    fn update_position(
        &self,
        next: [i16; 3],
        load_scale: Scalar,
        shape_id: ShapeId,
        shape: &mut Shape,
    ) -> Result<Pt3<Meters>> {
        // Update the referenced VxBuf instruction
        let (vxbuf_uuid, vxbuf_offset) = self.vxbuf;
        let InstrDetail::VertexBuffer(vxbuf) = shape
            .get_code_mut(shape_id)
            .instruction_mut(&vxbuf_uuid)?
            .detail_mut()
        else {
            bail!("expected a vertex buffer at {vxbuf_uuid}");
        };
        *vxbuf.vertex_mut(vxbuf_offset) = next;

        // Update the vertex buffer; use the FA position to lock ourselves to integers
        let vert = &mut shape.get_vertices_mut(shape_id)[self.vb_offset];
        vert.set_fa_position(&next, load_scale);

        Ok(vert.position())
    }
}

// A set of Co-Located vertices from the vertex buffer (get_vertices).
// This collects all the faces that associate these vertices, all locations in
// the vertex buffer and all vxbuf locations. Typically one vxbuf location will
// map to many faces, so the expectation is there will be many references to
// the same vxbuf address. That's not a requirement, however: e.g. if there is
// a different vertex normal for adjacent faces.
#[derive(Clone, Debug)]
pub struct CoVertex {
    position: Pt3<Meters>,
    vertices: Vec<VertexRef>,
}

impl CoVertex {
    pub fn from_position(
        pos: &Pt3<Meters>,
        shape_id: ShapeId,
        shape: &Shape,
    ) -> Result<Option<Self>> {
        let mut vert_refs = vec![];
        let verts = shape.get_vertices(shape_id);
        let sh_code = shape.get_code(shape_id);
        for (face_uuid, face_info) in shape.get_faces(shape_id) {
            let InstrDetail::Face(face_detail) = &sh_code.instruction(face_uuid)?.detail() else {
                bail!("expected face_uuid to be a face!");
            };
            for (loop_offset, index) in face_info.face_loop().iter().enumerate() {
                if verts[*index].position() == *pos {
                    vert_refs.push(VertexRef {
                        vb_offset: *index,
                        face: *face_uuid,
                        vxbuf: (
                            *face_info.vxbuf_uuid(),
                            face_info.vxbuf_offset(loop_offset, face_detail),
                        ),
                    });
                }
            }
        }
        if vert_refs.is_empty() {
            return Ok(None);
        }
        Ok(Some(Self {
            position: *pos,
            vertices: vert_refs,
        }))
    }

    pub fn vert_refs(&self) -> impl Iterator<Item = &VertexRef> {
        self.vertices.iter()
    }

    pub fn vert_ref(&self, offset: usize) -> &VertexRef {
        &self.vertices[offset]
    }

    pub fn position(&self) -> &Pt3<Meters> {
        &self.position
    }

    // Check if this co-vertex contains a vertex with the given vertex buffer index.
    pub fn contains_vb_index(&self, vb_index: usize) -> bool {
        for vert_ref in &self.vertices {
            if vert_ref.vb_offset == vb_index {
                return true;
            }
        }
        false
    }

    pub fn get_fa_vertex_position(&self, shape_id: ShapeId, shape: &Shape) -> Result<[i16; 3]> {
        let (vxbuf_uuid, vxbuf_offset) = self.vertices[0].vxbuf;
        let InstrDetail::VertexBuffer(vxbuf) =
            shape.get_code(shape_id).instruction(&vxbuf_uuid)?.detail()
        else {
            bail!("expected a vertex buffer at {vxbuf_uuid}");
        };
        Ok(*vxbuf.vertex(vxbuf_offset))
    }

    pub fn update_position(
        &mut self,
        next: [i16; 3],
        load_scale: Scalar,
        recompute_face_center: bool,
        recompute_face_normal: bool,
        shape_id: ShapeId,
        shape: &mut Shape,
    ) -> Result<()> {
        for vxref in &self.vertices {
            vxref.update_position(next, load_scale, shape_id, shape)?;

            // Update the face center and normal. For this we need all vertices in the face,
            // including vertices that may not have been selected.
            {
                let face_loop = shape.get_faces(shape_id)[&vxref.face].face_loop();
                assert!(face_loop.len() >= 3);
                let verts = shape.get_vertices(shape_id);
                let norm = compute_normal(
                    &verts[face_loop[0]].position(),
                    &verts[face_loop[1]].position(),
                    &verts[face_loop[2]].position(),
                );
                let centroid = vxref.compute_face_centroid(shape_id, shape);

                let InstrDetail::Face(face) = shape
                    .get_code_mut(shape_id)
                    .instruction_mut(&vxref.face)?
                    .detail_mut()
                else {
                    bail!("expected a face at {}", vxref.face);
                };
                if recompute_face_normal {
                    face.set_normal(vec_to_fa_face_norm(&norm.as_vec3()));
                }
                if recompute_face_center {
                    face.set_center(centroid, load_scale);
                }
            }
        }

        // Write the updated position back to our cache, getting it from the position we
        // updated via the set_fa_position call.
        let updated = shape.get_vertices(shape_id)[self.vertices[0].vb_offset].position();
        self.position = updated;

        Ok(())
    }
}
