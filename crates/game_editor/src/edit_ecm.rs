// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    widget::{FieldRef, Form, FormField, Widget},
    DocumentEditProvider, DocumentImportProvider, EditState, EditableDocument,
};
use anyhow::Result;
use catalog::{AssetCollection, FileInfo, FileSystem, Search};
use ecm::{EcmType, EcmTypeIo};
use egui::Ui;
use installations::GameInstallation;
use nitrous::HeapMut;
use smallvec::{smallvec, SmallVec};
use xt::TypeManager;

#[derive(Clone, Debug, Default)]
pub struct EcmEdit;

impl EditableDocument for EcmEdit {
    fn make_importer(&self) -> Box<dyn DocumentImportProvider> {
        Box::<EcmImporter>::default() as Box<dyn DocumentImportProvider>
    }
    fn object_menu(&self, ui: &mut Ui) -> Option<EditState> {
        self.waiting_menu(ui)
    }
    fn graphics_menu(&self, _ui: &mut Ui) -> Option<EditState> {
        None
    }
    fn waiting_menu(&self, ui: &mut Ui) -> Option<EditState> {
        if ui.button(".ECM [Counter Measures Type]").clicked() {
            return Some(EditState::ImportDocument(self.make_importer()));
        }
        None
    }
    fn document_actions(
        &self,
        ui: &mut Ui,
        info: FileInfo,
        install: &GameInstallation,
        collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<Option<EditState>> {
        if info.name().ends_with(".ECM") && ui.button("Edit").clicked() {
            return Ok(Some(EcmImporter.import(info, install, collection, heap)?));
        }
        Ok(None)
    }
}

// Note: currently leaning on the TypeManager cache
#[derive(Debug, Default)]
pub struct EcmImporter;

impl DocumentImportProvider for EcmImporter {
    fn all_documents<'a>(&self, collection: &'a AssetCollection) -> Result<Vec<FileInfo<'a>>> {
        collection.search(Search::for_extension("ECM"))
    }

    fn column_headers(&self) -> SmallVec<[&'static str; 8]> {
        smallvec!["Name", "Description", "Weight"]
    }

    fn column_values(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<SmallVec<[String; 8]>> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(smallvec![
            info.name().to_owned(),
            xt.names().long_name().to_owned(),
            xt.ecm()?.weight().to_string()
        ])
    }

    fn import(
        &mut self,
        info: FileInfo,
        _install: &GameInstallation,
        _collection: &AssetCollection,
        heap: HeapMut,
    ) -> Result<EditState> {
        let xt = heap.resource::<TypeManager>().load(info)?;
        Ok(EditState::EditDocument(
            Box::new(EcmEditState::new(xt.ecm()?.to_owned())) as Box<dyn DocumentEditProvider>,
        ))
    }
}

#[derive(Clone, Debug)]
pub(crate) struct EcmEditState {
    ecm: EcmTypeIo,
}

impl DocumentEditProvider for EcmEditState {
    fn plugin(&self) -> &'static str {
        "ECM"
    }

    fn get_edit_widgets(&mut self, _heap: HeapMut) -> Result<Vec<Widget>> {
        use FormField as FF;
        Ok(vec![
            Widget::H1(format!(
                "{}: {}",
                self.ecm.si_names().file_name(),
                self.ecm.si_names().long_name()
            )),
            Widget::Form(Form::from_table(
                "ecm_form",
                vec![
                    FF::new("ecm_name", self)?.with_label("Name"),
                    FF::new("ecm_weight", self)?
                        .with_label("Weight")
                        .with_hover_text(
                            "Weight (in lbs) of the jammer pod, if externally mountable",
                        ),
                    FF::new("ecm_externally_loadable", self)?
                        .with_label("Externally Loadable")
                        .with_hover_text("1 for externally loadable, otherwise 0"),
                    FF::new("ecm_jammer_flags", self)?
                        .with_label("Flags")
                        .with_hover_text("TODO: make this checkboxes"),
                    // Chaff
                    FF::new_h2("ecm_chaff_header")?.with_label("Chaff Info"),
                    FF::new("ecm_chaff_count", self)?
                        .with_label("Chaff Count")
                        .with_hover_text("Number of chaff charges"),
                    FF::new("ecm_chaff_chance", self)?
                        .with_label("chaff_chance")
                        .with_hover_text("Probability that a chaff burst will distract a missile"),
                    FF::new("ecm_chaff_horizontal", self)?
                        .with_label("Chaff Horizontal Angle")
                        .with_hover_text("Horizontal ejection angle of chaff bursts"),
                    FF::new("ecm_chaff_pitch", self)?
                        .with_label("chaff_pitch")
                        .with_hover_text("Vertical ejection angle of chaff bursts"),
                    // Flares
                    FF::new_h2("ecm_flares_header")?.with_label("Flares Info"),
                    FF::new("ecm_flares_loaded", self)?
                        .with_label("Flares Loaded")
                        .with_hover_text("Number of flare charges"),
                    FF::new("ecm_flares_chance", self)?
                        .with_label("flares_chance")
                        .with_hover_text("Probability that a flare will distract a missile"),
                    FF::new("ecm_flares_horizontal", self)?
                        .with_label("Flares Horizontal")
                        .with_hover_text("Horizontal ejection angle of flares"),
                    FF::new("ecm_flares_pitch", self)?
                        .with_label("Flares Pitch")
                        .with_hover_text("Vertical ejection angle of flares"),
                    // Radar Jammer
                    FF::new_h2("ecm_radar_jammer_header")?.with_label("Radar Jammer Info"),
                    FF::new("ecm_radar_jammer_chance", self)?
                        .with_label("Radar Jammer Chance")
                        .with_hover_text(
                            "Probability that a radar homing missile will be distracted by ECM",
                        ),
                    FF::new("ecm_radar_jammer_rcs_increase", self)?
                        .with_label("Radar Jammer RCS Increase")
                        .with_hover_text(
                            "Increase to the radar profile of the aircraft with the jammer on",
                        ),
                    FF::new("ecm_radar_jammer_max_detect_cone", self)?
                        .with_label("Radar Jammer Max Detect Cone")
                        .with_hover_text("If you know, please let me know!"),
                    FF::new("ecm_radar_jammer_min_detect_cone", self)?
                        .with_label("Radar Jammer Min Detect Cone")
                        .with_hover_text("If you know, please let me know!"),
                    // IR Jammer
                    FF::new_h2("ecm_ir_jammer_header")?.with_label("IR Jammer Info"),
                    FF::new("ecm_ir_jammer_chance", self)?
                        .with_label("IR Jammer Chance")
                        .with_hover_text(
                            "Probability that an IR homing missile will be distracted by ECM",
                        ),
                    FF::new("ecm_ir_jammer_ics_increase", self)?
                        .with_label("IR Jammer ICS Increase")
                        .with_hover_text(
                            "Increase to the IR profile of the aircraft with the jammer on",
                        ),
                    FF::new("ecm_ir_jammer_lock_loss_time", self)?
                        .with_label("IR Jammer Lock Loss Time")
                        .with_hover_text("If you know, please let me know!"),
                ],
            )),
        ])
    }

    fn get_field_ref(&mut self, name: &str) -> Result<FieldRef> {
        let ecm = &mut self.ecm;
        Ok(match name {
            "ecm_name" => FieldRef::DosFilename(ecm.si_names_mut().file_name_mut()),
            "ecm_weight" => FieldRef::U16(ecm.weight_mut()),
            "ecm_externally_loadable" => FieldRef::U8(ecm.externally_loadable_mut()),
            "ecm_jammer_flags" => FieldRef::U16(ecm.jammer_flags_mut()),
            // Chaff
            "ecm_chaff_count" => FieldRef::U8(ecm.chaff_count_mut()),
            "ecm_chaff_chance" => FieldRef::U8(ecm.chaff_chance_mut()),
            "ecm_chaff_horizontal" => FieldRef::U8(ecm.chaff_horizontal_mut()),
            "ecm_chaff_pitch" => FieldRef::U8(ecm.chaff_pitch_mut()),
            // Flares
            "ecm_flares_loaded" => FieldRef::U8(ecm.flares_loaded_mut()),
            "ecm_flares_chance" => FieldRef::U8(ecm.flares_chance_mut()),
            "ecm_flares_horizontal" => FieldRef::U8(ecm.flares_horizontal_mut()),
            "ecm_flares_pitch" => FieldRef::U8(ecm.flares_pitch_mut()),
            // Radar Jammer
            "ecm_radar_jammer_chance" => FieldRef::U8(ecm.radar_jammer_chance_mut()),
            "ecm_radar_jammer_rcs_increase" => FieldRef::U16(ecm.radar_jammer_rcs_increase_mut()),
            "ecm_radar_jammer_max_detect_cone" => {
                FieldRef::U8(ecm.radar_jammer_max_detect_cone_mut())
            }
            "ecm_radar_jammer_min_detect_cone" => {
                FieldRef::U8(ecm.radar_jammer_min_detect_cone_mut())
            }
            // IR Jammer
            "ecm_ir_jammer_chance" => FieldRef::U8(ecm.ir_jammer_chance_mut()),
            "ecm_ir_jammer_ics_increase" => FieldRef::U16(ecm.ir_jammer_ics_increase_mut()),
            "ecm_ir_jammer_lock_loss_time" => FieldRef::U8(ecm.ir_jammer_lock_loss_time_mut()),
            _ => panic!("Unknown name {name}"),
        })
    }

    fn serialize(&self, _heap: HeapMut) -> Result<Vec<(String, Vec<u8>)>> {
        Ok(vec![(
            self.ecm.si_names().file_name().to_owned(),
            self.ecm.to_string().as_bytes().to_owned(),
        )])
    }
}

impl EcmEditState {
    pub(crate) fn new(ecm: EcmType) -> Self {
        Self {
            ecm: EcmTypeIo::from(&ecm),
        }
    }
}
