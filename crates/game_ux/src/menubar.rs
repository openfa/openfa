// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use mantle::Window;
use runtime::ScriptHerder;

pub fn show_basic_menubar(
    ctx: &egui::Context,
    name: &'static str,
    window: &mut Window,
    herder: &mut ScriptHerder,
) -> Result<()> {
    egui::TopBottomPanel::top(name)
        .show(ctx, |ui| -> Result<()> {
            egui::menu::bar(ui, |ui| -> Result<()> {
                ui.menu_button("File", |ui| -> Result<()> {
                    add_file_menu_items(ui, herder, window)
                });
                ui.menu_button("Help", |ui| -> Result<()> {
                    add_help_menu_items(ui, herder)
                });
                Ok(())
            })
            .inner
        })
        .inner?;
    Ok(())
}

pub fn add_file_menu_items(
    ui: &mut egui::Ui,
    herder: &mut ScriptHerder,
    window: &mut Window,
) -> Result<()> {
    if ui.button("Return to Game").clicked() {
        herder.run_string("ux.return_to_top()")?;
    }
    if ui.button("Quit to Desktop").clicked() {
        window.request_quit();
    }
    Ok(())
}

pub fn add_help_menu_items(ui: &mut egui::Ui, herder: &mut ScriptHerder) -> Result<()> {
    if ui.button("Credits").clicked() {
        herder.run_string("ux.credits()")?;
    }
    Ok(())
}
