// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use paste::paste;

use anyhow::{anyhow, Result};
use catalog::{AssetCatalog, FileSystem, Search};
use clap::Args;
use codepage_437::{FromCp437, IntoCp437, CP437_CONTROL};
use ecm::{EcmType, EcmTypeIo};
use gas::{GasType, GasTypeIo};
use installations::{from_dos_string, Installations};
use jt::{ProjectileType, ProjectileTypeIo};
use nitrous::Value;
use nt::{NpcType, NpcTypeIo};
use ot::{ObjectType, ObjectTypeIo};
use pt::{PlaneType, PlaneTypeIo};
use see::{SeeType, SeeTypeIo};
use std::{fs, path::Path};

macro_rules! make_type_command {
    ($cmdname:ident, $file_ext:ident, $ty:ident) => {
        #[derive(Debug, Args)]
        #[command(about = concat!("Show the contents of ", stringify!($file_ext), " files"))]
        pub struct $cmdname {
            #[arg(help = concat!("List of ", stringify!($file_ext), " files to load"))]
            inputs: Vec<String>,

            /// Show just one field
            #[arg(short, long)]
            field: Option<String>,
        }

        impl $cmdname {
            pub fn handle(&self, _installs: &Installations, catalog: &AssetCatalog) -> Result<()> {
                for input in &self.inputs {
                    for info in catalog.search(Search::for_any_input(input))? {
                        let ty = $ty::from_text(&from_dos_string(info.data()?))?;
                        if let Some(field) = self.field.as_deref() {
                            println!("{info}> {field} = {}", ty.get_field(field)?);
                        } else {
                            println!("{info}");
                            for field in $ty::fields() {
                                println!("   {field} = {}", ty.get_field(field)?);
                            }
                        }
                    }
                }
                Ok(())
            }

            pub fn to_yaml(path: &Path) -> Result<()> {
                let outext = path
                    .extension()
                    .expect("extension")
                    .to_string_lossy()
                    .to_string()
                    + ".yaml";
                let outpath = path.with_extension(outext);
                let data = fs::read(path)?;
                let content = String::from_cp437(data, &CP437_CONTROL);
                let xt = $ty::from_text(&content)?;
                let val: Value = xt.into();
                let yaml = val.to_yaml()?;
                fs::write(outpath, yaml)?;
                Ok(())
            }

            paste! {
                pub fn from_yaml(path: &Path) -> Result<()> {
                    let outpath = path.with_extension("");
                    let yaml = fs::read_to_string(path)?;
                    let val = Value::from_yaml(&yaml)?;
                    let xt = $ty::try_from(&val)?;
                    let io = [<$ty Io>]::from(&xt);
                    let contents = io.to_string();
                    let encoded = contents
                        .into_cp437(&CP437_CONTROL)
                        .map_err(|e| anyhow!("result is not a cp437 string {e:?}"))?;
                    fs::write(outpath, encoded)?;
                    Ok(())
                }
            }
        }
    };
}

make_type_command!(EcmCommand, ECM, EcmType);
make_type_command!(GasCommand, GAS, GasType);
make_type_command!(SeeCommand, SEE, SeeType);
make_type_command!(JtCommand, JT, ProjectileType);
make_type_command!(NtCommand, NT, NpcType);
make_type_command!(OtCommand, OT, ObjectType);
make_type_command!(PtCommand, PT, PlaneType);
