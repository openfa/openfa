// This file is part of OpenFA.
//
// OpenFA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenFA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenFA.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::Feet;
use anyhow::Result;
use catalog::{AssetCatalog, FileSystem, Search};
use clap::Args;
use installations::{from_dos_string, Installations};
use lay::Layer;
use nitrous::Value;
use pal::Palette;
use std::{fs, path::Path, time::Instant};
use t2::Terrain;
use xt::TypeManager;

/// Show contents of T2 files
#[derive(Debug, Args)]
pub struct T2Command {
    /// Back of the envelop profiling.
    #[arg(long)]
    profile: bool,

    /// One or more T2 files to process
    inputs: Vec<String>,
}

const PROFILE_COUNT: usize = 10000;

fn find_and_build_palette(
    t2_path: &Path,
    catalog: &AssetCatalog,
    installs: &Installations,
    typeman: &TypeManager,
) -> Result<Palette> {
    let mm_path = t2_path.with_extension("MM");
    let mm_fp = catalog.lookup(mm_path.to_str().expect("ascii"))?;
    let mm_content = from_dos_string(mm_fp.data()?);
    let mm = mmm::MissionMap::from_str(&mm_content, typeman, catalog)?;
    let lay_index = if mm.layer_index() != 0 {
        mm.layer_index()
    } else {
        2
    };
    let lay_name = mm.layer_name().filename();

    // Load the LAY to get palette indices to export
    let layer_fp = catalog.lookup(lay_name)?;
    let layer = Layer::from_bytes(&layer_fp.data()?, installs.primary_palette())?;

    // Build a palette from the layer
    Terrain::make_palette(&layer, lay_index, installs.primary_palette())
}

pub fn t2_to_yaml(
    path: &Path,
    catalog: &AssetCatalog,
    installs: &Installations,
    typeman: &TypeManager,
) -> Result<()> {
    let outpath = path.with_extension("T2.yaml");
    let palette = find_and_build_palette(path, catalog, installs, typeman)?;

    let bytes0 = fs::read(path)?;
    let terrain = Terrain::from_bytes(&bytes0)?;
    let mut value: Value = (&terrain).into();
    terrain.serialize_layers(&palette, &mut value)?;
    let yaml = value.to_yaml()?;
    fs::write(outpath, yaml)?;
    Ok(())
}

pub fn yaml_to_t2(
    path: &Path,
    catalog: &AssetCatalog,
    installs: &Installations,
    typeman: &TypeManager,
) -> Result<()> {
    let t2_path = path.with_extension("");
    let palette = find_and_build_palette(&t2_path, catalog, installs, typeman)?;
    let yaml = fs::read_to_string(path)?;
    let value: Value = Value::from_yaml(&yaml)?;
    let terrain = Terrain::deserialize_layers(&palette, &value)?;
    let t2_data = terrain.to_bytes()?;
    fs::write(t2_path, t2_data)?;
    Ok(())
}

pub fn handle_t2_command(
    cmd: &T2Command,
    _installs: &Installations,
    catalog: &AssetCatalog,
) -> Result<()> {
    for input in &cmd.inputs {
        for info in catalog.search(Search::for_input("T2", input))? {
            let long_name = info.to_string();
            println!("{long_name}");
            println!("{}", "=".repeat(long_name.len()));
            show_t2(&info.data()?, cmd.profile)?;
        }
    }
    Ok(())
}

fn show_t2(raw: &[u8], profile: bool) -> Result<()> {
    if profile {
        let start = Instant::now();
        for _ in 0..PROFILE_COUNT {
            let _ = Terrain::from_bytes(raw)?;
        }
        println!(
            "load time: {}ms",
            (start.elapsed().as_micros() / PROFILE_COUNT as u128) as f64 / 1000.0
        );
        return Ok(());
    }
    let t2 = Terrain::from_bytes(raw)?;
    println!("map name:       {}", t2.name());
    println!("logical width:  {}", t2.width());
    println!("logical height: {}", t2.height());
    println!("physical width: {}", t2.physical_width());
    println!("physical height:{}", t2.physical_height());
    println!("extent e-w:     {}", t2.extent_east_west::<Feet>());
    println!("extent n-s:     {}", t2.extent_north_south::<Feet>());
    println!();

    Ok(())
}
