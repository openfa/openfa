# Roadmap
What are the next major features that I plan to work on?

Please keep in mind:
* This is highly aspirational.
* I'll likely be distracted along the way.
* Some of it may be impossible anyway. 

In rough order, but not with deadlines:

1) Classic FA flight model, with feel similar to FA, for most traditional aircraft.
   1) Outcomes:
      * [x] Fly around static maps and missions.
      * [x] An interesting tech demo, usable on more than just the development machine.
   2) Non-goals:
      * ~Ground interactions~
      * ~Collisions~
      * ~Vectored Thrust~
      * ~AIs or NPCs that fly~

2) Fix the terrain engine. Pay down some high interest tech debt taken to get us off the ground quickly in the first couple of years of development. Provide a sound and stable base for building OpenFA going forward.
   1) Outcomes:
      * [x] Faster CPU tiles computation
      * [x] Stream tiles from the internet
      * ~[ ] Integrate landsat 8/9 color imagery~
        * _Note1: Landsat 8 is old and busted; Sentinel 2 is the new hotness._
        * _Note2: this is going to cost about $4,000. to compute on Google Earth Engine._
      * [x] Ability to query terrain height regions on the CPU
      * [x] Use GDAL to build imagery sets; delete dump-terrain-tiles, with fire
   2) Non-goals:
      * ~Collisions with terrain~
      * ~Ground movers~
      * ~Correctly placed ground assets~

3) Landing and Takeoff. We can at least think of ourselves as a flight sim at this point.
   1) Outcomes:
      * [ ] Correctly placed runways
      * [ ] Gear vs runway collisions
      * [ ] Shape vs runway collisions
      * [ ] Shape vs shape collisions
      * [ ] Shape vs terrain collisions
      * [ ] Fly around missions that start on the ground.
      * [ ] Touch-and-go's
      * [ ] Taxi to and from the apron
      * [ ] Bonus: drive wheeled vehicles around, maybe?
   2) Non-goals:
      * ~Collisions between shapes~
      * ~Weapons or combat~

4) Implement network play. This has the potential to cause massive rework, so we need to get it done as early as possible, rather than putting it off.
   1) Outcomes:
      * [ ] Start a server, somehow
      * [ ] Connect clients to the server via the console
      * [ ] Tested at least once over the real internet
      * [ ] STUN support so we work with firewalls*
        * _Note: if we can get it "for free" with a library_
   2) Non-goals:
      * ~UX for creating and joining games~
      * ~Any sort of gameplay~

5) HUD and cockpit view. The purpose here is to validate that our flight model still feels broadly correct when flown in first person. So far we have only flown in 3rd person.
   1) Outcomes:
      * [ ] First person flight that feels like FA
        * _Note: May push changes back to flight and ground models_
      * [ ] Altitude ticker
      * [ ] Speed ticker
      * [ ] Angle tape
      * [ ] Bonus: cockpit systems displays
      * [ ] Bonus: sensor views in hud windows
   2) Non-goals:
      * ~Working anything beyond the very basic cockpit and hud tape~

6) Guns and Damage Model
   1) Outcomes:
      * [ ] Ability to fire guns / gun pods
      * [ ] Ability to damage structures and vehicles with gun fire
      * [ ] Show the damage shape or shapes upon destruction
      * [ ] Bonus; ability to fire rockets / rocket pods
   2) Non-goals:
      * ~Rendering fire or smoke~
      * ~Guided anything~
      * ~Wind effects~
      * ~Tracking and target prediction~
      * ~Exact conformance to FA behavior~
        * Note: The gunplay is great, but the bullet graphics are just sad at this point

7) Research the AI/BI formats
   1) Outcomes:
      * [ ] Understand enough of the instruction format to decode all instructions
   2) Non-goals:
      * ~Working AI~

8) AI (Easy)
   1) Outcomes:
      * [ ] Have something not static in the world so that missile implementation can be meaningful
      * [ ] Very, very basic fly/drive to waypoint steering
      * [ ] Bonus: point-this-end-at-enemy combat AI
   2) Non-goals:
      * ~Fun combat~
      * ~Working missions~

9) Radar, targets, tracking, etc
   1) Outcomes:
      * [ ] Ability to turn the radar on and off in air and ground mode
      * [ ] More or less correct targets should show up in each
      * [ ] Ability to designate a target
      * [ ] Targets highlighted (with box) in the HUD
      * [ ] Bonus: Target cameras
   2) Non-goals:
      * ~AWACS~
      * ~IR/FLIR~
      * ~IFF~
      * ~ECS~
      * ~Jamming~
      * ~Chaff/Flares~
      * ~FoF designations~

10) Missiles
    1) Outcomes:
       * [ ] Ability to lock onto a target and deploy a missile
       * [ ] Active radar homing
       * [ ] Semi-Active radar homing
       * [ ] Bonus: Ability of AI to deploy missiles
    2) Non-goals:
       * ~IR missiles~
       * ~Smoke, Fire, or any effects whatsoever outside the missile itself~
       * ~Most missile guidance modes~

11) Mission completion checking
    1) Outcomes:
       * [ ] Ability to pass missions with basic objectives
    2) Non-goals:
       * ~MC support~
       * ~Ability to win all possible missions~
       * ~Ability to fail missions~

!!! Some FA mission should now be "playable", at least in spirit... maybe !!!

12+. ... The long, long, looooooong tail of work to make campaigns completable.
But this is already like a decade of work; no point planning much further yet.
